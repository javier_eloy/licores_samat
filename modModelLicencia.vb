Imports Microsoft.VisualBasic
Imports system.Data.SqlClient
Imports System.Data
Imports System.Globalization

Public Class modModelLicencia

    'Public Shared Function getDocumentos(ByVal lngContribuyenteId As Long, _
    '                                     ByVal lngTipoLicenciaId As Long, _
    '                                     ByVal lngClasificacionLicenciaId As Long, _
    '                                     ByVal lngEstadoLicenciaAnula As Long, _
    '                                     ByRef sNumExpediente As String, _
    '                                     ByRef sNumSolicitud As String) As Boolean
    '    'PROP�SITO: Devuelve el n�mero de expediente de licores de un contribuyente recuperandolo de la BD
    '    'PAR�METROS: lngContribuyenteId: id del contribuyente
    '    'DESARROLLADOR: Sigerist Rodr�guez
    '    'FECHA DE �LTIMA MODIFICACI�N: 21/01/2011
    '    'DESARROLLADOR DE LA �LTIMA MODIFICACI�N: Javier Hernandez
    '    Dim sql As String = vbNullString
    '    Dim cnn As SqlConnection = Nothing
    '    Dim cmd As SqlCommand = Nothing
    '    Dim dr As SqlDataReader

    '    cnn = modBaseDatos.obtenerConexion
    '    cnn.Open()

    '    sql = String.Format("SELECT TOP 1 num_expediente, num_solicitud FROM licencia WHERE contribuyente_id = {0} AND tipo_licencia_id ={1} AND clasificacion_licencia_id = {2}", lngContribuyenteId, lngTipoLicenciaId, lngClasificacionLicenciaId)
    '    sql = String.Format("{0} AND num_expediente NOT IN (SELECT num_expediente FROM licencia WHERE estado_licencia_id = {1})", sql, lngEstadoLicenciaAnula)

    '    cmd = New SqlCommand(sql, cnn)
    '    dr = cmd.ExecuteReader(CommandBehavior.CloseConnection)
    '    If dr.Read Then
    '        sNumExpediente = dr("num_expediente")
    '        sNumSolicitud = dr("num_solicitud")
    '        Return True
    '    Else
    '        Return False
    '    End If
    'End Function

    Public Shared Function getRenovaciones(ByVal lngContribuyenteId As Long, _
                                    ByVal lngTipoLicenciaId As Long, _
                                    ByVal lngClasificacionLicenciaId As Long, _
                                    ByVal lngEstadoLicenciaAnula As Integer, _
                                    ByRef lNumRenovaciones As Long, _
                                    ByRef dUltimaRenovacion As Date) As Boolean
        Dim sql As String = vbNullString
        Dim cnn As SqlConnection = Nothing
        Dim cmd As SqlCommand = Nothing
        Dim dr As SqlDataReader

        cnn = modBaseDatos.obtenerConexion
        cnn.Open()
        sql = String.Format("SELECT COALESCE(SUM(CASE m.motivo_accion WHEN 1 THEN 1 ELSE 0 END),-1) As renovaciones, MAX(L.fecha_vencimiento_licencia) As ultima_vigencia FROM licencia As L INNER JOIN motivo_autorizacion As m ON m.motivo_autorizacion_id = L.motivo_licencia_id")
        sql = String.Format("{0} WHERE l.contribuyente_id = {1} AND l.tipo_licencia_id = {2} AND l.clasificacion_licencia_id = {3} ", sql, lngContribuyenteId, lngTipoLicenciaId, lngClasificacionLicenciaId)
        sql = String.Format("{0} AND l.num_expediente NOT IN (SELECT num_expediente FROM licencia WHERE estado_licencia_id = {1})", sql, lngEstadoLicenciaAnula)

        cmd = New SqlCommand(sql, cnn)
        dr = cmd.ExecuteReader(CommandBehavior.CloseConnection)

        If dr.Read Then
            lNumRenovaciones = Integer.Parse(dr("renovaciones"))
            If lNumRenovaciones < 0 Then Return False
            dUltimaRenovacion = dr("ultima_vigencia")
            Return True
        Else
            Return False
        End If

    End Function

    Public Shared Function verifLicencia(ByVal lngContribuyenteId As Long, _
                                         ByVal lngLicenciaId As Long, _
                                         ByVal lngTipoLicenciaId As Long, _
                                         ByVal lngMotivoLicenciaId As Long, _
                                         ByVal lngClasificacionLicenciaId As Long, _
                                         Optional ByVal intAno As String = "") As Integer
        'PROP�SITO: Verifica que no existan en base de datos una combinaci�n de tipo de licencia, clasificaci�n de licencia y motivo de autorizaci�n para un contribuyente. 
        'opcionalmente verifica que sea en un a�o espec�fico
        'PAR�METROS:  lngContribuyenteId: id del contribuyente a verificar
        '             lngLicenciaId: Licencia ID, si es 0 se verifica para un nueva insersion, en caso contrario se excluye
        '             lngTipoLicencia: id del tipo de licencia a verificar
        '             lngMotivoLicencia: id del motivo de autorizaci�n a verificar
        '             lngClasificacionLicenciaId: id de la clasificaci�n de licencia a verificar
        '             intAno: a�o para el cual se verificar� la existencia de la licencia
        'DESARROLLADOR: Sigerist Rodr�guez
        'FECHA DE �LTIMA MODIFICACI�N: 01/02/2011
        'DESARROLLADOR DE LA �LTIMA MODIFICACI�N: Javier Hernandez
        Dim sql As String = vbNullString
        Dim cnn As SqlConnection = Nothing
        Dim cmd As SqlCommand = Nothing
        Dim dr As SqlDataReader

        cnn = modBaseDatos.obtenerConexion
        cnn.Open()

        sql = "SELECT l.* FROM licencia l INNER JOIN tipo_licencia tp ON tp.tipo_licencia_id = l.tipo_licencia_id "
        sql = String.Concat(sql, " WHERE l.tipo_licencia_id = ", lngTipoLicenciaId.ToString)
        sql = String.Concat(sql, " AND l.motivo_licencia_id = ", lngMotivoLicenciaId.ToString)
        sql = String.Concat(sql, " AND l.clasificacion_licencia_id = ", lngClasificacionLicenciaId.ToString)
        sql = String.Concat(sql, " AND l.contribuyente_id = ", lngContribuyenteId.ToString)
        sql = String.Concat(sql, " AND tp.estemporal = 0")
        sql = String.Concat(sql, " AND l.num_expediente NOT IN (SELECT num_expediente FROM licencia WHERE estado_licencia_id = (SELECT TOP 1 estado_licencia_id_anula FROM CONFIGURACION))")
        If lngLicenciaId <> 0 Then sql = String.Concat(sql, " AND l.licencia_id <> ", lngLicenciaId.ToString)
        If Not intAno.Equals(String.Empty) Then sql = String.Concat(sql, " AND ano = ", intAno)

        cmd = New SqlCommand(sql, cnn)
        dr = cmd.ExecuteReader(CommandBehavior.CloseConnection)

        If dr.HasRows Then
            If intAno.Equals(String.Empty) Then Return 1 _
                                           Else Return 2
        End If

        Return 0
    End Function


    Public Shared Function getLastLicenciaVigente(ByVal lngContribuyenteId As Long, _
                                                 ByVal lngTipoLicenciaId As Long, _
                                                 ByVal lngClasificacionLicenciaId As Long, _
                                                 ByVal lngEstadoLicenciaAnula As Long) As SqlDataReader
        'PROP�SITO: Obtiene los datos de la �ltima licencia permanente emitida para un contribuyente para un contribuyente. 
        'PAR�METROS:  lngContribuyenteId: id del contribuyente
        'DESARROLLADOR: Sigerist Rodr�guez
        'MODIFICACION: Javier Hernandez
        'FECHA DE �LTIMA MODIFICACI�N: 15/02/2011
        'DESARROLLADOR DE LA �LTIMA MODIFICACI�N: Sigerist Rodr�guez
        Dim sql As String = vbNullString
        Dim cnn As SqlConnection = Nothing
        Dim cmd As SqlCommand = Nothing
        Dim dr As SqlDataReader

        cnn = modBaseDatos.obtenerConexion
        cnn.Open()

        sql = "SELECT TOP 1 l.num_licencia, l.ano, l.num_solicitud, l.fecha_solicitud, l.num_solicitud_seniat,"
        sql = String.Concat(sql, " l.fecha_ven_solicitud_seniat, l.num_autorizacion, l.num_control, l.motivo_autorizacion,")
        sql = String.Concat(sql, " l.tipo_licencia, l.renovacion, l.estatus, l.horario,")
        sql = String.Concat(sql, " l.estado_licencia_id, l.tipo_licencia_id, l.fecha_inicio_licencia,")
        sql = String.Concat(sql, " l.fecha_vencimiento_licencia, l.clasificacion_licencia_id, l.clasificacion_licencia,")
        sql = String.Concat(sql, " l.num_expediente, l.fecha_expendio, l.motivo_licencia_id, l.horario_licencia_id, l.con_extension ")
        sql = String.Concat(sql, " FROM licencia As l INNER JOIN motivo_autorizacion As m ON m.motivo_autorizacion_id = l.motivo_licencia_id AND m.motivo_accion = 0")
        sql = String.Format("{0} WHERE l.contribuyente_id = {1} AND l.tipo_licencia_id ={2} AND l.clasificacion_licencia_id = {3}", sql, lngContribuyenteId, lngTipoLicenciaId, lngClasificacionLicenciaId)
        sql = String.Format("{0} AND l.num_expediente NOT IN (SELECT num_expediente FROM licencia WHERE estado_licencia_id = {1})", sql, lngEstadoLicenciaAnula)

        cmd = New SqlCommand(sql, cnn)
        dr = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        Return dr
    End Function

    Public Shared Function UpdateAnulaLicencia(ByVal lngContribuyenteId As Long, _
                                               ByVal sNumExpediente As String, _
                                               ByVal cnn As SqlConnection, ByRef tran As IDbTransaction) As Integer
        'PROP�SITO: Actualiza el estado de la licencia a anulado
        'PAR�METROS:  lngContribuyenteId: id del contribuyente para el cual se generar� la planilla
        '             sNumExpediente: Numero de Expediente a anular
        '             cnn: conexi�n que se usar� para ejecutar el comando
        '             tran: transacci�n que se usar� en el comando
        'DESARROLLADOR: Javier Hernandez
        'FECHA DE �LTIMA MODIFICACI�N: 18/02/2011

        Dim cmd As SqlClient.SqlCommand
        cmd = New SqlClient.SqlCommand()
        Dim bd As New modBaseDatos
        Dim planillaID As Integer

        Try
            cmd = New SqlCommand("upd_licencia_anula", cnn)
            cmd.Transaction = tran
            cmd.CommandType = Data.CommandType.StoredProcedure

            bd.config_parametro(cmd, "@contribuyente_id", DbType.Int32, ParameterDirection.Input, lngContribuyenteId)
            bd.config_parametro(cmd, "@num_expediente", DbType.String, ParameterDirection.Input, sNumExpediente)
            bd.config_parametro(cmd, "@return_value", DbType.Int32, ParameterDirection.ReturnValue, 0)
            cmd.ExecuteNonQuery()
            planillaID = cmd.Parameters("@return_value").Value

        Catch ex As Exception
            planillaID = -1
        End Try

        Return planillaID
    End Function


    Public Shared Function guardarPlanillaLicencia(ByVal lngContribuyenteId As Long, _
                                                   ByVal lngUsuarioId As Long, _
                                                   ByVal lngMotivoLicenciaId As Long, _
                                                   ByVal lngKioskos As Long, _
                                                   ByVal cnn As SqlConnection, ByRef tran As IDbTransaction) As Long
        'PROP�SITO: Genera automat�camente una planilla de emisi�n de licencia de licores para asociarla a la licencia. 
        'PAR�METROS:  lngContribuyenteId: id del contribuyente para el cual se generar� la planilla
        '             lngUsuarioId: id del usuario que genera la licencia y por ende la planilla
        '             cnn: conexi�n que se usar� para ejecutar el comando
        '             tran: transacci�n que se usar� en el comando
        'DESARROLLADOR: Sigerist Rodr�guez
        'FECHA DE �LTIMA MODIFICACI�N: 01/02/2011
        'DESARROLLADOR DE LA �LTIMA MODIFICACI�N: Javier Hernandez
        Dim cmd As SqlClient.SqlCommand
        cmd = New SqlClient.SqlCommand()
        Dim bd As New modBaseDatos
        Dim planillaID As Integer

        Try
            cmd = New SqlCommand("ins_planilla_licencia", cnn)
            cmd.Transaction = tran
            cmd.CommandType = Data.CommandType.StoredProcedure

            bd.config_parametro(cmd, "@contribuyente_id", DbType.Int32, ParameterDirection.Input, lngContribuyenteId)
            bd.config_parametro(cmd, "@usuario_id", DbType.Int32, ParameterDirection.Input, lngUsuarioId)
            bd.config_parametro(cmd, "@motivo_licencia_id", DbType.Int32, ParameterDirection.Input, lngMotivoLicenciaId)
            bd.config_parametro(cmd, "@cantidad", DbType.Int32, ParameterDirection.Input, lngKioskos)
            bd.config_parametro(cmd, "@new_planilla_id", DbType.Int32, ParameterDirection.Output, 0)
            bd.config_parametro(cmd, "@return_value", DbType.Int32, ParameterDirection.ReturnValue, 0)
            cmd.ExecuteNonQuery()
            planillaID = cmd.Parameters("@return_value").Value

        Catch ex As Exception
            planillaID = -1
        End Try

        Return planillaID
    End Function


    Public Shared Function GuardaLicencia(ByVal lngLicenciaId As Long, ByVal lngContribuyenteId As Long, _
                                          ByVal lngUsuarioId As Long, ByVal intActivo As Integer, _
                                          ByVal strRIM As String, ByVal strRIF As String, _
                                          ByVal strRazonSocial As String, ByVal intAno As Integer, ByVal strFecha As String, _
                                          ByVal strParroquia As String, ByVal strSector As String, ByVal intNumConforme As Integer, _
                                          ByVal intNumSolicitud As Integer, ByVal strFechaSolicitud As String, ByVal intNumSolicitudSeniat As Integer, _
                                          ByVal strFechaVencSolSeniat As String, ByVal intNumAutorizacion As Integer, ByVal sNumControl As String, _
                                          ByVal strMotivoAutorizacion As String, ByVal strTipoLicencia As String, ByVal intRenovacion As Integer, _
                                          ByVal strEstatus As String, ByVal strHorario As String, ByVal lngEstadoLicenciaId As Long, _
                                          ByVal lngTipoLicenciaId As Long, ByVal strFechaInicioLicencia As String, ByVal strFechaVencimientoLicencia As String, _
                                          ByVal lngClasificacionLicenciaId As Long, ByVal strClasificacionLicencia As String, ByVal intNumExpediente As Integer, _
                                          ByVal strFechaExpendio As String, ByVal lngMotivoLicenciaId As Long, ByVal lngHorarioLicenciaId As Long, _
                                          ByVal intConExtension As Integer, ByVal intNumLicenciaSeniat As Integer, ByVal intNumLicencia As Integer, _
                                          ByVal strEvento As String, ByVal intKioskoNumero As Byte, ByVal strKioskoEvento As String, ByVal strDirEvento As String, _
                                          ByVal cnn As SqlConnection, ByRef tran As IDbTransaction) As Long
        Dim sql As String = vbNullString
        Dim cmd As SqlCommand = Nothing
        Dim result As Long = 0
        Dim planillaId As Long = 0

        Try
            If lngLicenciaId > 0 Then
                cmd = New SqlCommand("upd_licencia", cnn)
                cmd.Transaction = tran
                cmd.CommandType = Data.CommandType.StoredProcedure
                cmd.Parameters.Add("licencia_id", Data.SqlDbType.Int, 4) 'si se est� modificando una licencia existente se incluye el par�metro del id
            Else
                'Si es una licencia nueva se genera la planilla de liquidacion
                planillaId = guardarPlanillaLicencia(lngContribuyenteId, lngUsuarioId, _
                                                     lngMotivoLicenciaId, intKioskoNumero, _
                                                     cnn, tran)
                If planillaId > 0 Then
                    cmd = New SqlCommand("ins_licencia", cnn)
                    cmd.Transaction = tran
                    cmd.CommandType = Data.CommandType.StoredProcedure
                Else
                    Return -100 'si hubo un error al generar la planilla se retorna -1 para cancelar la transaccion
                End If
            End If

            cmd.Parameters.Add("contribuyente_id", Data.SqlDbType.Int, 4)
            cmd.Parameters.Add("usuario_id", Data.SqlDbType.Int, 4)
            cmd.Parameters.Add("activo", Data.SqlDbType.Int, 4)
            cmd.Parameters.Add("rim", Data.SqlDbType.NVarChar, 50)
            cmd.Parameters.Add("rif", Data.SqlDbType.NVarChar, 50)
            cmd.Parameters.Add("razon_social", Data.SqlDbType.NVarChar, 100)
            cmd.Parameters.Add("ano", Data.SqlDbType.Int, 4)
            cmd.Parameters.Add("fecha", Data.SqlDbType.DateTime, 10)
            cmd.Parameters.Add("parroquia", Data.SqlDbType.NVarChar, 200)
            cmd.Parameters.Add("sector", Data.SqlDbType.NVarChar, 200)
            cmd.Parameters.Add("num_conforme", Data.SqlDbType.Int, 4)
            cmd.Parameters.Add("num_solicitud", Data.SqlDbType.Int, 4)
            cmd.Parameters.Add("fecha_solicitud", Data.SqlDbType.DateTime, 10)
            cmd.Parameters.Add("num_solicitud_seniat", Data.SqlDbType.Int, 4)
            cmd.Parameters.Add("fecha_ven_solicitud_seniat", Data.SqlDbType.DateTime, 10)
            cmd.Parameters.Add("num_autorizacion", Data.SqlDbType.Int, 4)
            cmd.Parameters.Add("num_control", Data.SqlDbType.VarChar, 20)
            cmd.Parameters.Add("motivo_autorizacion", Data.SqlDbType.NVarChar, 100)
            cmd.Parameters.Add("tipo_licencia", Data.SqlDbType.NVarChar, 20)
            cmd.Parameters.Add("renovacion", Data.SqlDbType.Int, 4)
            cmd.Parameters.Add("estatus", Data.SqlDbType.NVarChar, 30)
            cmd.Parameters.Add("horario", Data.SqlDbType.NVarChar, 100)
            cmd.Parameters.Add("estado_licencia_id", Data.SqlDbType.Int, 4)
            cmd.Parameters.Add("tipo_licencia_id", Data.SqlDbType.Int, 4)
            cmd.Parameters.Add("fecha_inicio_licencia", Data.SqlDbType.DateTime, 10)
            cmd.Parameters.Add("fecha_vencimiento_licencia", Data.SqlDbType.DateTime, 10)
            cmd.Parameters.Add("clasificacion_licencia_id", Data.SqlDbType.Int, 4)
            cmd.Parameters.Add("clasificacion_licencia", Data.SqlDbType.NVarChar, 50)
            cmd.Parameters.Add("num_expediente", Data.SqlDbType.NVarChar, 50)
            cmd.Parameters.Add("fecha_expendio", Data.SqlDbType.DateTime, 10)
            cmd.Parameters.Add("motivo_licencia_id", Data.SqlDbType.Int, 4)
            cmd.Parameters.Add("horario_licencia_id", Data.SqlDbType.Int, 4)
            cmd.Parameters.Add("con_extension", Data.SqlDbType.Int, 4) 'XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
            cmd.Parameters.Add("num_licencia_SENIAT", Data.SqlDbType.NVarChar, 50)
            cmd.Parameters.Add("num_licencia", Data.SqlDbType.NVarChar, 50)
            If (lngLicenciaId = 0) Then cmd.Parameters.Add("planilla_id", Data.SqlDbType.Int, 4)
            cmd.Parameters.Add("evento", Data.SqlDbType.NVarChar, 500)
            cmd.Parameters.Add("kiosko_evento", Data.SqlDbType.VarChar, 100)
            cmd.Parameters.Add("kiosko_numero", Data.SqlDbType.Int, 4)
            cmd.Parameters.Add("dir_evento", Data.SqlDbType.VarChar, 500)
            cmd.Parameters.Add("licId", Data.SqlDbType.Int, 4)

            If (lngLicenciaId > 0) Then cmd.Parameters("licencia_id").Value = lngLicenciaId 'Si se esta actualizando una licencia existente se incluye el valor del par�metro del id
            cmd.Parameters("contribuyente_id").Value = lngContribuyenteId
            cmd.Parameters("usuario_id").Value = lngUsuarioId
            cmd.Parameters("activo").Value = intActivo
            cmd.Parameters("rim").Value = strRIM
            cmd.Parameters("rif").Value = strRIF
            cmd.Parameters("razon_social").Value = strRazonSocial
            cmd.Parameters("ano").Value = intAno
            cmd.Parameters("fecha").Value = Date.Parse(strFecha, CultureInfo.GetCultureInfo("es-ES"))
            cmd.Parameters("parroquia").Value = strParroquia
            cmd.Parameters("sector").Value = strSector
            cmd.Parameters("num_conforme").Value = intNumConforme
            cmd.Parameters("num_solicitud").Value = intNumSolicitud
            cmd.Parameters("fecha_solicitud").Value = Date.Parse(strFechaSolicitud, CultureInfo.GetCultureInfo("es-ES"))
            cmd.Parameters("num_solicitud_seniat").Value = intNumSolicitudSeniat
            cmd.Parameters("fecha_ven_solicitud_seniat").Value = Date.Parse(strFechaVencSolSeniat, CultureInfo.GetCultureInfo("es-ES"))
            cmd.Parameters("num_autorizacion").Value = intNumAutorizacion
            cmd.Parameters("num_control").Value = sNumControl
            cmd.Parameters("motivo_autorizacion").Value = strMotivoAutorizacion
            cmd.Parameters("tipo_licencia").Value = strTipoLicencia
            cmd.Parameters("renovacion").Value = intRenovacion
            cmd.Parameters("estatus").Value = strEstatus
            cmd.Parameters("horario").Value = strHorario
            cmd.Parameters("estado_licencia_id").Value = lngEstadoLicenciaId
            cmd.Parameters("tipo_licencia_id").Value = lngTipoLicenciaId
            cmd.Parameters("fecha_inicio_licencia").Value = Date.Parse(strFechaInicioLicencia, CultureInfo.GetCultureInfo("es-ES"))
            cmd.Parameters("fecha_vencimiento_licencia").Value = Date.Parse(strFechaVencimientoLicencia, CultureInfo.GetCultureInfo("es-ES"))
            cmd.Parameters("clasificacion_licencia_id").Value = lngClasificacionLicenciaId
            cmd.Parameters("clasificacion_licencia").Value = strClasificacionLicencia
            cmd.Parameters("num_expediente").Value = intNumExpediente
            cmd.Parameters("fecha_expendio").Value = Date.Parse(strFechaExpendio, CultureInfo.GetCultureInfo("es-ES"))
            cmd.Parameters("motivo_licencia_id").Value = lngMotivoLicenciaId
            cmd.Parameters("horario_licencia_id").Value = lngHorarioLicenciaId
            cmd.Parameters("con_extension").Value = intConExtension
            cmd.Parameters("num_licencia_seniat").Value = intNumLicenciaSeniat.ToString
            cmd.Parameters("num_licencia").Value = intNumLicencia.ToString
            If (lngLicenciaId = 0) Then cmd.Parameters("planilla_id").Value = planillaId
            cmd.Parameters("evento").Value = strEvento
            cmd.Parameters("kiosko_evento").Value = strKioskoEvento
            cmd.Parameters("kiosko_numero").Value = intKioskoNumero
            cmd.Parameters("dir_evento").Value = strDirEvento
            cmd.Parameters("licId").Direction = ParameterDirection.Output

            cmd.ExecuteNonQuery()

            result = cmd.Parameters("licId").Value
            Return result

        Catch ex As Exception
            Return -1
        End Try

    End Function

    Public Shared Function GuardaLicenciaVehiculo(ByVal lngLicenciaId As Long, ByVal lngContribuyenteId As Long, _
                                                  ByVal lngUsuarioId As Long, ByVal intActivo As Integer, ByVal strRIM As String, _
                                                  ByVal strRIF As String, ByVal strRazonSocial As String, ByVal intAno As Integer, _
                                                  ByVal strFecha As String, ByVal strParroquia As String, ByVal strSector As String, _
                                                  ByVal intNumConforme As Integer, ByVal intNumSolicitud As Integer, ByVal strFechaSolicitud As String, _
                                                  ByVal intNumSolicitudSeniat As Integer, ByVal strFechaVencSolSeniat As String, ByVal intNumAutorizacion As Integer, _
                                                  ByVal sNumControl As String, ByVal strMotivoAutorizacion As String, ByVal strTipoLicencia As String, _
                                                  ByVal intRenovacion As Integer, ByVal strEstatus As String, ByVal strHorario As String, _
                                                  ByVal lngEstadoLicenciaId As Long, ByVal lngTipoLicenciaId As Long, ByVal strFechaInicioLicencia As String, _
                                                  ByVal strFechaVencimientoLicencia As String, ByVal lngClasificacionLicenciaId As Long, ByVal strClasificacionLicencia As String, _
                                                  ByVal intNumExpediente As Integer, ByVal strFechaExpendio As String, ByVal lngMotivoLicenciaId As Long, _
                                                  ByVal lngHorarioLicenciaId As Long, ByVal intConExtension As Integer, ByVal intNumLicenciaSeniat As Integer, _
                                                  ByVal intNumLicencia As Integer, _
                                                  ByVal strEvento As String, ByVal intKioskoNumero As Integer, ByVal strKioskoEvento As String, ByVal strDirEvento As String, _
                                                  ByRef cnn As SqlConnection, ByRef tran As IDbTransaction, ByVal arrVehiculo As ArrayList) As Long
        'Dim lngLicenciaId As Long
        Dim sql As String = vbNullString
        Dim cmd As SqlCommand = Nothing
        Dim result As Long = 0
        Dim x As Integer = 0

        Try

            lngLicenciaId = GuardaLicencia(lngLicenciaId, lngContribuyenteId, lngUsuarioId, _
                                           intActivo, strRIM, strRIF, strRazonSocial, _
                                           intAno, strFecha, strParroquia, strSector, _
                                           intNumConforme, intNumSolicitud, strFechaSolicitud, _
                                           intNumSolicitudSeniat, strFechaVencSolSeniat, _
                                           intNumAutorizacion, sNumControl, strMotivoAutorizacion, _
                                           strTipoLicencia, intRenovacion, strEstatus, strHorario, _
                                           lngEstadoLicenciaId, lngTipoLicenciaId, strFechaInicioLicencia, _
                                           strFechaVencimientoLicencia, lngClasificacionLicenciaId, _
                                           strClasificacionLicencia, intNumExpediente, strFechaExpendio, _
                                           lngMotivoLicenciaId, lngHorarioLicenciaId, intConExtension, _
                                           intNumLicenciaSeniat, intNumLicencia, _
                                           strEvento, intKioskoNumero, strKioskoEvento, strDirEvento, _
                                           cnn, tran)

            If lngLicenciaId > 0 Then

                cmd = New SqlCommand("ins_vehiculoxlicencia", cnn)
                cmd.Transaction = tran
                cmd.CommandType = Data.CommandType.StoredProcedure
                cmd.Parameters.Add("vehiculo_id", Data.SqlDbType.Int, 4)
                cmd.Parameters.Add("licencia_id", Data.SqlDbType.Int, 4)
                cmd.Parameters.Add("usuario_id", Data.SqlDbType.Int, 4)
                cmd.Parameters.Add("activo", Data.SqlDbType.Int, 4)
                cmd.Parameters.Add("newId", SqlDbType.Int, 4)
                cmd.Parameters("newId").Direction = ParameterDirection.Output

                While (x < arrVehiculo.Count)
                    cmd.Parameters("vehiculo_id").Value = arrVehiculo.Item(x)
                    cmd.Parameters("licencia_id").Value = lngLicenciaId
                    cmd.Parameters("usuario_id").Value = lngUsuarioId
                    cmd.Parameters("activo").Value = intActivo
                    cmd.ExecuteNonQuery()
                    If cmd.Parameters("newId").Value < 0 Then
                        Return cmd.Parameters("newId").Value
                    End If
                    x = x + 1
                End While
            End If

            Return lngLicenciaId

        Catch ex As Exception
            Return -1
        End Try
    End Function


    Public Shared Function getDatosLicencia(ByVal lngLicenciaId) As System.Data.IDataReader
        Dim sql As String = vbNullString
        Dim cnn As SqlConnection = Nothing
        Dim cmd As SqlCommand = Nothing
        Dim dr As SqlDataReader

        cnn = modBaseDatos.obtenerConexion
        cnn.Open()
        '                  0         1            2               3                 4
        '                      5                         6                7              8  
        '                     9            10        11        12             
        '                   13                      14               15                  
        '                      16                               17                      18                          
        '                   19                20             21                     22
        '                    23              24
        '              25        26             27         28
        sql = "SELECT num_licencia, ano, num_solicitud, fecha_solicitud, num_solicitud_seniat, "
        sql = String.Concat(sql, " fecha_ven_solicitud_seniat, num_autorizacion, num_control, motivo_autorizacion,")
        sql = String.Concat(sql, "  tipo_licencia, renovacion, estatus, horario, ")
        sql = String.Concat(sql, "  estado_licencia_id, tipo_licencia_id, fecha_inicio_licencia,")
        sql = String.Concat(sql, "  fecha_vencimiento_licencia, clasificacion_licencia_id, clasificacion_licencia, ")
        sql = String.Concat(sql, " num_expediente, fecha_expendio, motivo_licencia_id, horario_licencia_id, ")
        sql = String.Concat(sql, " con_extension, num_licencia_seniat, ")
        sql = String.Concat(sql, " evento, kiosko_evento, dir_evento, kiosko_numero ")
        sql = String.Concat(sql, "  FROM licencia WHERE licencia_id =", lngLicenciaId.ToString)

        cmd = New SqlCommand(sql, cnn)
        dr = cmd.ExecuteReader()
        Return dr
    End Function

    Public Shared Function getDatosLicenciaFromExpediente(ByVal snum_expediente As String) As System.Data.IDataReader
        Dim sql As String = vbNullString
        Dim cnn As SqlConnection = Nothing
        Dim cmd As SqlCommand = Nothing
        Dim dr As SqlDataReader

        cnn = modBaseDatos.obtenerConexion
        cnn.Open()
        sql = "SELECT TOP 1 num_licencia, ano, num_solicitud, fecha_solicitud, num_solicitud_seniat, "
        sql = String.Concat(sql, " fecha_ven_solicitud_seniat, num_autorizacion, num_control, motivo_autorizacion,")
        sql = String.Concat(sql, " tipo_licencia, renovacion, estatus, horario, ")
        sql = String.Concat(sql, " estado_licencia_id, tipo_licencia_id, fecha_inicio_licencia,")
        sql = String.Concat(sql, " fecha_vencimiento_licencia, clasificacion_licencia_id, clasificacion_licencia, ")
        sql = String.Concat(sql, " num_expediente, fecha_expendio, motivo_licencia_id, horario_licencia_id, ")
        sql = String.Concat(sql, " con_extension, num_licencia_seniat, ")
        sql = String.Concat(sql, " evento, kiosko_evento, dir_evento, kiosko_numero ")
        sql = String.Concat(sql, " FROM licencia WHERE num_expediente = '", snum_expediente.Trim, "'")
        cmd = New SqlCommand(sql, cnn)
        dr = cmd.ExecuteReader()
        Return dr
    End Function
End Class
