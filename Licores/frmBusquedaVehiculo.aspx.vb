﻿Imports System.Data.SqlClient
Imports System.Data

Partial Class pagina_operacion_Licores_emrBusquedaVehiculo
    Inherits System.Web.UI.Page

    Private lngContribuyenteId As Long
    Private strOrigen As String = String.Empty

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            lngContribuyenteId = CType(Request("ID"), Integer)
            Session("contribuyente_id") = lngContribuyenteId
        Catch
            lngContribuyenteId = 0
        End Try
        ActualizarGrid("", 0, "")
    End Sub

    Private Function arrSearch(ByVal arr As ArrayList, ByVal strValor As String) As Integer
        Dim x As Integer = 0

        For x = 0 To arr.Count - 1
            If arr.Item(x) = strValor Then
                Return x
            End If
        Next
        Return -1

    End Function

    Private Sub ActualizarGrid(ByVal strCampo As String, ByVal strCriterio As String, ByVal strValor As String)
        Dim aSQL As String

        aSQL = "SELECT v.vehiculo_id, V.año, v.numero_placa, v.numero_calcomania, v.activo, "
        aSQL += " v.descripcion_estado_vehiculo, v.descripcion_tipo_vehiculo, v.descripcion_modelo, "
        aSQL += " v.nombre_color, v.descripcion_marca, v.contribuyente_id, DATEDIFF(day,GETDATE(),vh.ultima_fecha) As DiasSolvente"
        aSQL += " FROM vis_vehiculo_contribuyente v "
        aSQL += " LEFT JOIN vis_vehiculo_ult_planilla vh ON v.contribuyente_id = vh.contribuyente_id "
        aSQL += " WHERE v.contribuyente_id = " + lngContribuyenteId.ToString

        If strCampo <> "" Then
            Select Case strCampo
                Case "MARCA"
                    aSQL += " AND descripcion_marca "
                Case "MODELO"
                    aSQL += " AND descripcion_modelo "
                Case "AÑO"
                    aSQL += " AND año "
                Case "PLACA"
                    aSQL += " AND numero_placa "
            End Select

            Select Case strCriterio
                Case "IGUAL A"
                    aSQL += " = '" & strValor & "'"
                Case "CONTENGA"
                    aSQL += " LIKE '%" & strValor & "%'"
            End Select

        End If

        aSQL += " ORDER BY v.descripcion_marca, v.descripcion_modelo"
        SqlDSVehiculos.SelectCommand = aSQL
        SqlDSVehiculos.Select(New System.Web.UI.DataSourceSelectArguments)
    End Sub

    Protected Sub cmdBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdBuscar.Click
        ActualizarGrid(Me.ddlCampo.SelectedItem.Text, Me.ddlCriterio.SelectedItem.Text, Me.txtValor.Text)
    End Sub

    'Protected Sub grdVehiculo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdVehiculo.PageIndexChanging
    'grdVehiculo.PageIndex = e.NewPageIndex
    'Me.ActualizarGrid("", "", "")
    'End Sub

    Protected Sub cmdSeleccionar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdSeleccionar.Click
        Dim strIdsVehiculo As String = ""
        Dim x As Integer = 0
        guardarArreglo()
        Dim arrIds As New ArrayList
        If Not IsNothing(Session("arrIds")) Then
            arrIds = CType(Session("arrIds"), ArrayList)
        End If

        For x = 0 To arrIds.Count - 1
            strIdsVehiculo &= arrIds.Item(x) & ","
        Next

        If strIdsVehiculo.Length > 0 Then
            strIdsVehiculo = strIdsVehiculo.Substring(0, strIdsVehiculo.Length - 1)
        End If

        Session.Remove("arrIds")
        If Request("origen") = "asoc" Then
            Response.Redirect(ResolveUrl("~/pagina/operacion/Licores/frmAsocVehiculos.aspx?ID=" & Request("ID") & "&tipoLicenciaId=" & Request("tipoLicenciaId") & "&motivoAutorizacionId=" & Request("motivoAutorizacionId") & "&estadoLicenciaId=" & Request("estadoLicenciaId") & "&horarioId=" & Request("horarioId") & "&licenciaId=" & Request("licenciaId") & "&numeroControl=" & Request("numeroControl") & "&numeroAutorizacion=" & Request("numeroAutorizacion") & "&numeroSolicitud=" & Request("numeroSolicitud") & "&fechaSolicitud=" & Request("fechaSolicitud") & "&fechaEmision=" & Request("fechaEmision") & "&fechaInicio=" & Request("fechaInicio") & "&fechaVencimiento=" & Request("fechaVencimiento") & "&anno=" & Request("anno") & "&numeroSeniat=" & Request("numeroSeniat") & "&numeroExpediente=" & Request("numeroExpediente") & "&numeroLicencia=" & Request("numeroLicencia") & "&clasificacionLicenciaId=" & Request("clasificacionLicenciaId") & "&IdsVehiculo=" & strIdsVehiculo))
            'Response.Redirect(ResolveUrl("~/pagina/operacion/Licores/frmAsocVehiculos.aspx?ID=" & Request("ID") & "&IdsVehiculo=" & strIdsVehiculo))
        ElseIf Request("origen") = "edit" Then
            Response.Redirect(ResolveUrl("~/pagina/operacion/Licores/frmEditAsocVehiculos.aspx?ID=" & Request("ID") & "&tipoLicenciaId=" & Request("tipoLicenciaId") & "&motivoAutorizacionId=" & Request("motivoAutorizacionId") & "&estadoLicenciaId=" & Request("estadoLicenciaId") & "&horarioId=" & Request("horarioId") & "&licenciaId=" & Request("licenciaId") & "&numeroControl=" & Request("numeroControl") & "&numeroAutorizacion=" & Request("numeroAutorizacion") & "&numeroSolicitud=" & Request("numeroSolicitud") & "&fechaSolicitud=" & Request("fechaSolicitud") & "&fechaEmision=" & Request("fechaEmision") & "&fechaInicio=" & Request("fechaInicio") & "&fechaVencimiento=" & Request("fechaVencimiento") & "&anno=" & Request("anno") & "&numeroSeniat=" & Request("numeroSeniat") & "&numeroExpediente=" & Request("numeroExpediente") & "&numeroLicencia=" & Request("numeroLicencia") & "&clasificacionLicenciaId=" & Request("clasificacionLicenciaId") & "&IdsVehiculo=" & strIdsVehiculo))
        End If

    End Sub

    Protected Sub grvVehiculo_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grvVehiculo.PageIndexChanging
        guardarArreglo()
    End Sub

    Protected Sub grvVehiculo_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grvVehiculo.RowCreated
        If e.Row.Cells.Count > 3 Then
            e.Row.Cells(7).Visible = False
            'If Request("tipoLicenciaId") = 17 Then
            '    e.Row.Cells(8).Visible = True
            'Else
            '    e.Row.Cells(8).Visible = False
            'End If
        End If
    End Sub

    Protected Sub grvVehiculo_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grvVehiculo.RowDataBound
        Dim arrIds As New ArrayList
        If Not IsNothing(Session("arrIds")) Then
            arrIds = CType(Session("arrIds"), ArrayList)
        End If
        If grvVehiculo.Rows.Count > 0 Then
            'If arrIds.BinarySearch(grvVehiculo.Rows(grvVehiculo.Rows.Count - 1).Cells(7).Text.Trim) > -1 Then
            If arrIds.IndexOf(grvVehiculo.Rows(grvVehiculo.Rows.Count - 1).Cells(7).Text.Trim) > -1 Then
                'If arrSearch(arrIds, grvVehiculo.Rows(grvVehiculo.Rows.Count - 1).Cells(7).Text.Trim) > -1 Then
                CType(grvVehiculo.Rows(grvVehiculo.Rows.Count - 1).Cells(0).FindControl("cbxAsociar"), CheckBox).Checked = True
            End If
        End If

        If e.Row.RowType = DataControlRowType.DataRow Then
            Dim drView As DataRowView = CType(e.Row.DataItem, DataRowView)
            Dim lblSolv As Label = CType(e.Row.FindControl("lblSolvente"), Label)

            lblSolv.Text = "No"
            If Not IsDBNull(drView("DiasSolvente")) AndAlso drView("DiasSolvente") > 0 Then _
               lblSolv.Text = "Si"

        End If
    End Sub

    Protected Sub cmdCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles cmdCancelar.Click
        Session.Remove("arrIds")
        If Request("origen") = "asoc" Then
            Response.Redirect(ResolveUrl("~/pagina/operacion/Licores/frmAsocVehiculos.aspx?ID=" & Request("ID") & "&tipoLicenciaId=" & Request("tipoLicenciaId") & "&motivoAutorizacionId=" & Request("motivoAutorizacionId") & "&estadoLicenciaId=" & Request("estadoLicenciaId") & "&horarioId=" & Request("horarioId") & "&licenciaId=" & Request("licenciaId") & "&numeroControl=" & Request("numeroControl") & "&numeroAutorizacion=" & Request("numeroAutorizacion") & "&numeroSolicitud=" & Request("numeroSolicitud") & "&fechaSolicitud=" & Request("fechaSolicitud") & "&fechaEmision=" & Request("fechaEmision") & "&fechaInicio=" & Request("fechaInicio") & "&fechaVencimiento=" & Request("fechaVencimiento") & "&anno=" & Request("anno") & "&numeroSeniat=" & Request("numeroSeniat") & "&numeroExpediente=" & Request("numeroExpediente") & "&numeroLicencia=" & Request("numeroLicencia") & "&clasificacionLicenciaId=" & Request("clasificacionLicenciaId")))
            'Response.Redirect(ResolveUrl("~/pagina/operacion/Licores/frmAsocVehiculos.aspx?ID=" & Request("ID") & "&IdsVehiculo=" & strIdsVehiculo))
        ElseIf Request("origen") = "edit" Then
            Response.Redirect(ResolveUrl("~/pagina/operacion/Licores/frmEditAsocVehiculos.aspx?ID=" & Request("ID") & "&tipoLicenciaId=" & Request("tipoLicenciaId") & "&motivoAutorizacionId=" & Request("motivoAutorizacionId") & "&estadoLicenciaId=" & Request("estadoLicenciaId") & "&horarioId=" & Request("horarioId") & "&licenciaId=" & Request("licenciaId") & "&numeroControl=" & Request("numeroControl") & "&numeroAutorizacion=" & Request("numeroAutorizacion") & "&numeroSolicitud=" & Request("numeroSolicitud") & "&fechaSolicitud=" & Request("fechaSolicitud") & "&fechaEmision=" & Request("fechaEmision") & "&fechaInicio=" & Request("fechaInicio") & "&fechaVencimiento=" & Request("fechaVencimiento") & "&anno=" & Request("anno") & "&numeroSeniat=" & Request("numeroSeniat") & "&numeroExpediente=" & Request("numeroExpediente") & "&numeroLicencia=" & Request("numeroLicencia") & "&clasificacionLicenciaId=" & Request("clasificacionLicenciaId")))
        End If

    End Sub

    Protected Sub guardarArreglo()
        Dim arrIds As New ArrayList
        If Not IsNothing(Session("arrIds")) Then
            arrIds = CType(Session("arrIds"), ArrayList)
        End If
        For Each fila As GridViewRow In Me.grvVehiculo.Rows
            arrIds.Remove(fila.Cells(7).Text.Trim)
            If CType(fila.FindControl("cbxAsociar"), CheckBox).Checked = True Then
                arrIds.Add(fila.Cells(7).Text.Trim)
            End If
        Next
        Session("arrIds") = arrIds
    End Sub

End Class
