﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wucDatosContribuyente.ascx.vb" Inherits="pagina_operacion_Licores_frmDatosContribuyente" %>
 <table class="cssTabla" style="width: 998px; height: 472px">
            <tr>
                <td align="center" colspan="4">
                    <asp:Label ID="Label1" runat="server" CssClass="cssTitulo" Text="Licencias de Licores"></asp:Label></td>
            </tr>
            <tr>
                <td align="center" colspan="4" >
                    <asp:Label ID="Label3" runat="server" CssClass="subtitulo" Text="DATOS GENERALES DEL CONTRIBUYENTE"></asp:Label></td>
            </tr>
            <tr>
                <td align="left" >
                    <asp:Label ID="Label2" runat="server" CssClass="subtitulo" Text="Nombre / Razón Social:"></asp:Label></td>
                <td align="left" >
                    <asp:Label ID="lblNombreContribuyente" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
                <td align="left" >
                    <asp:Label ID="Label5" runat="server" CssClass="subtitulo" Text="Estado:"></asp:Label></td>
                <td align="left" >
                    <asp:Label ID="lblEstadoContribuyente" runat="server" CssClass="normal" Text=" " Width="104px"></asp:Label>
                    <asp:Label ID="lblEstClasificacionContribuyente" runat="server" CssClass="normal" Text=" " Width="104px"></asp:Label></td>
            </tr>
            <tr>
                <td align="left" style="white-space:nowrap" >
                    <asp:Label ID="Label8" runat="server" CssClass="subtitulo" Text="Lema Comercial:"></asp:Label></td>
                <td align="left" colspan="3">
                    <asp:Label ID="lblLemaContribuyente" runat="server" CssClass="normal" Width="244px"></asp:Label>&nbsp;</td>
            </tr>
            <tr>
                <td align="left" >
                    <asp:Label ID="Label13" runat="server" CssClass="subtitulo" Text="C.I.:"></asp:Label></td>
                <td align="left" >
                    <asp:Label ID="lblCedulaContribuyente" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
                <td align="left" >
                    <asp:Label ID="Label12" runat="server" CssClass="subtitulo" Text="RIF:"></asp:Label></td>
                <td align="left" >
                    <asp:Label ID="lblRIFContribuyente" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
            </tr>
            <tr>
                <td align="left" >
                    <asp:Label ID="Label14" runat="server" CssClass="subtitulo" Text="RIM:"></asp:Label></td>
                <td align="left" >
                    <asp:Label ID="lblRIMContribuyente" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
                <td align="left" style="white-space:nowrap">
                   <asp:Label ID="Label18" runat="server" CssClass="subtitulo" Text="Nº de Uso Conforme:"></asp:Label></td>
                <td align="left" >
                   <asp:Label ID="lblUsoContribuyente" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
            </tr>
            <tr>
                <td align="left" style="white-space:nowrap" >
                    <asp:Label ID="Label30" runat="server" CssClass="subtitulo" Text="Parroquia Fiscal:"></asp:Label></td>
                <td align="left" >
                    <asp:Label ID="lblParroquiaContribuyente" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
                <td align="left" style="white-space:nowrap">
                    <asp:Label ID="Label32" runat="server" CssClass="subtitulo" Text="Sector Fiscal:"></asp:Label></td>
                <td align="left" >
                    <asp:Label ID="lblSectorContribuyente" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
            </tr>
            <tr>
                <td align="left" style="white-space:nowrap">
                    <asp:Label ID="Label34" runat="server" CssClass="subtitulo" Text="Dirección Fiscal:"></asp:Label></td>
                <td align="left" colspan="3" >
                    <asp:Label ID="lblDireccionContribuyente" runat="server" CssClass="normal" Width="737px"></asp:Label></td>
            </tr>
            <tr>
                <td align="left" style="white-space:nowrap">
                    <asp:Label ID="Label10" runat="server" CssClass="subtitulo" Text="Teléfono Principal:"></asp:Label></td>
                <td align="left" >
                    <asp:Label ID="lblTelefonoContribuyente" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
                <td align="left" style="white-space:nowrap" >
                    <asp:Label ID="Label19" runat="server" CssClass="subtitulo" Text="Teléfono Adicional:"></asp:Label></td>
                <td align="left" >
                    <asp:Label ID="lblTlfAdicionalContribuyente" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
            </tr>
            <tr>
                <td align="left" style="white-space:nowrap" >
                    <asp:Label ID="Label29" runat="server" CssClass="subtitulo" Text="Teléfono Celular:"></asp:Label></td>
                <td align="left" >
                    <asp:Label ID="lblCelularContribuyente" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
                <td align="left" style="white-space:nowrap">
                    <asp:Label ID="Label24" runat="server" CssClass="subtitulo" Text="Teléfono Celular Adicional:"></asp:Label></td>
                <td align="left" >
                    <asp:Label ID="lblCelAdicionalContribuyente" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
            </tr>
            <tr>
                <td align="left" style="white-space:nowrap" >
                    <asp:Label ID="Label25" runat="server" CssClass="subtitulo" Text="E-mail:"></asp:Label></td>
                <td align="left" >
                    <asp:Label ID="lblEmailContribuyente" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
                <td align="left" style="white-space:nowrap" >
                    <asp:Label ID="Label27" runat="server" CssClass="subtitulo" Text="Página Web:"></asp:Label></td>
                <td align="left" >
                    <asp:Label ID="lblPaginaContribuyente" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
            </tr>
            <tr>
                <td align="left" style="white-space:nowrap" >
                    <asp:Label ID="Label23" runat="server" CssClass="subtitulo" Text="Parroquia del Establecimiento:"></asp:Label></td>
                <td align="left" >
                    <asp:Label ID="lblParroquiaEstablecimiento" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
                <td align="left" style="white-space:nowrap" >
                    <asp:Label ID="Label41" runat="server" CssClass="subtitulo" Text="Sector del Establecimiento:"></asp:Label></td>
                <td align="left" >
                    <asp:Label ID="lblSectorEstablecimiento" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
            </tr>
            <tr >
                <td align="left" style="white-space:nowrap">
                    <asp:Label ID="Label43" runat="server" CssClass="subtitulo" Text="Dirección del Establecimiento:"></asp:Label></td>
                <td align="left" colspan="3" >
                    <asp:Label ID="lblDireccionEstablecimiento" runat="server" CssClass="normal" Width="739px"></asp:Label></td>
            </tr>
            <tr >
                <td align="left" style="white-space:nowrap">
                    <asp:Label ID="Label45" runat="server" CssClass="subtitulo" Text="Numero de Licencia:"></asp:Label></td>
                <td align="left" >
                    <asp:Label ID="lblNumLicencia" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
                <td align="left" >
                    <asp:Label ID="Label47" runat="server" CssClass="subtitulo" Text="Número de Licencia SENIAT:"></asp:Label></td>
                <td align="left" >
                    <asp:Label ID="lblSeniat" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
            </tr>
        </table>