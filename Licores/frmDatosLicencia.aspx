﻿<%@ Page Language="VB" MasterPageFile="~/pagina/plantilla/plantillaSegura.master"
    AutoEventWireup="false" CodeFile="frmDatosLicencia.aspx.vb" Inherits="pagina_operacion_Licores_frmDatosLicencia"
    Title="Untitled Page" %>
<%@ Register Src="wucDatosLicencias.ascx" TagName="frmDatosLicencias" TagPrefix="uc2" %>
<%@ Register Src="wucDatosContribuyente.ascx" TagName="frmDatosContribuyente" TagPrefix="uc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../../javascript/calendario/jscripts/scripts.js"></script>

    <script type="text/javascript" src="../../../javascript/calendario/jscripts/calendar/calendar.js"></script>

    <script type="text/javascript" src="../../../javascript/calendario/jscripts/calendar/calendar-es.js"></script>

    <script type="text/javascript" src="../../../javascript/calendario/jscripts/calendar/calendar-setup.js"></script>

    <script language="javascript" type="text/javascript">
// <!CDATA[

function TABLE1_onclick() {

}

// ]]>
    </script>

    <form id="frm1" runat="server">
        <uc1:frmDatosContribuyente ID="ucDatosContribuyentes" runat="server" />       
        <br />
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:SqlDataSource ID="SqlDSHorario" runat="server" SelectCommand="SELECT horario_licencia_id, descripcion_horario_licencia FROM horario_licencia"
                    ConnectionString="<%$ ConnectionStrings:TRIBUTOConnectionString %>" ProviderName="<%$ ConnectionStrings:TRIBUTOConnectionString.ProviderName %>">
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDSEstadoLicencia" runat="server" SelectCommand="SELECT estado_licencia_id, descripcion_estado_licencia FROM ESTADO_LICENCIA"
                    ConnectionString="<%$ ConnectionStrings:TRIBUTOConnectionString %>" ProviderName="<%$ ConnectionStrings:TRIBUTOConnectionString.ProviderName %>">
                </asp:SqlDataSource>
                <div style="left: 458px; width: 100px; position: absolute; top: 778px; height: 18px"
                    id="DivCalendar">
                    &nbsp;</div>
                <table style="width: 1000px" id="Table2" class="cssTabla" onclick="return TABLE1_onclick()">
                    <tbody>
                        <tr>
                          <td>
                              <uc2:frmDatosLicencias ID="ucDatosLicencia" runat="server" />
                          </td>
                        </tr>
                        <tr>
                            <td align="left" colspan="5">
                                <asp:Button ID="BtnInsertar" OnClick="BtnInsertar_Click" runat="server" Text="Insertar"
                                    CssClass="cssBoton" Width="68px"></asp:Button>&nbsp;
                                <asp:Button ID="BtnCancelar" runat="server" Text="Cancelar" CssClass="cssBoton" Width="64px">
                                </asp:Button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</asp:Content>
