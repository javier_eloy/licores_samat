﻿<%@ Page Language="VB" MasterPageFile="~/pagina/plantilla/plantillaSegura.master"
    AutoEventWireup="false" CodeFile="frmEditAsocVehiculos.aspx.vb" Inherits="pagina_operacion_Licores_frmEditAsocVehiculos"
    Title="Untitled Page" %>

<%@ Register Src="wucDatosLicencias.ascx" TagName="frmDatosLicencias" TagPrefix="uc2" %>
<%@ Register Src="wucDatosContribuyente.ascx" TagName="frmDatosContribuyente" TagPrefix="uc1" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript" src="../../../javascript/calendario/jscripts/scripts.js"></script>
    <script type="text/javascript" src="../../../javascript/calendario/jscripts/calendar/calendar.js"></script>
    <script type="text/javascript" src="../../../javascript/calendario/jscripts/calendar/calendar-es.js"></script>
    <script type="text/javascript" src="../../../javascript/calendario/jscripts/calendar/calendar-setup.js"></script>
    <form id="frm1" runat="server">
        <uc1:frmDatosContribuyente ID="ucDatosContribuyentes" runat="server" />
        <br />
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:SqlDataSource ID="SqlDSHorario" runat="server" SelectCommand="SELECT horario_licencia_id, descripcion_horario_licencia FROM horario_licencia"
                    ConnectionString="<%$ ConnectionStrings:TRIBUTOConnectionString %>" ProviderName="<%$ ConnectionStrings:TRIBUTOConnectionString.ProviderName %>">
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="SqlDSEstadoLicencia" runat="server" SelectCommand="SELECT estado_licencia_id, descripcion_estado_licencia FROM ESTADO_LICENCIA"
                    ConnectionString="<%$ ConnectionStrings:TRIBUTOConnectionString %>" ProviderName="<%$ ConnectionStrings:TRIBUTOConnectionString.ProviderName %>">
                </asp:SqlDataSource>
                <div style="left: 458px; width: 100px; position: absolute; top: 778px; height: 18px"
                    id="DivCalendar">
                </div>
                <table style="width: 998px" id="Table2" class="cssTabla" >
                    <tbody>
                        <tr> 
                         <td>
                             <uc2:frmDatosLicencias ID="ucDatosLicencia" runat="server" />                        
                         </td>
                        </tr>
                        <tr>
                            <td style="height: 30px" align="left" colspan="5">
                                <asp:Button ID="Button1" OnClick="BtnInsertar_Click" runat="server" Text="Guardar"
                                    CssClass="cssBoton" Width="68px" Visible="False"></asp:Button>&nbsp;
                                <asp:Button ID="BtnCancelar" runat="server" Text="Cancelar" CssClass="cssBoton" Width="64px"
                                    Visible="False"></asp:Button></td>
                        </tr>
                    </tbody>
                </table>
                <table style="width: 998px" id="Table3" class="cssTabla" onclick="return TABLE1_onclick()">
                    <tbody>
                        <tr>
                            <td style="height: 30px" align="center" colspan="5" rowspan="2">
                                <asp:Label ID="Label54" runat="server" Text="ASOCIACIÓN DE VEHÍCULOS" CssClass="subtitulo"></asp:Label></td>
                        </tr>
                        <tr>
                        </tr>
                        <tr>
                            <td style="height: 25px" align="left" colspan="5">
                                <asp:Button ID="BtnBuscar" OnClick="BtnBuscar_Click" runat="server" Text="Buscar en el Registro"
                                    CssClass="cssBoton" Width="136px"></asp:Button>
                            </td>
                        </tr>
                        <tr>
                            <td style="height: 37px" align="center" colspan="5" rowspan="7">
                                <asp:SqlDataSource ID="SqlDSVehiculos" runat="server" SelectCommand="SELECT vehiculo_id, año, numero_placa, numero_calcomania, fecha_registro_sistema, fecha_compra, publicidad_rotulada, activo, descripcion_estado_vehiculo, descripcion_tipo_vehiculo, descripcion_modelo, nombre_color, descripcion_marca, contribuyente_id FROM vis_vehiculo_contribuyente WHERE vehiculo_id IN (-1)"
                                    ConnectionString="<%$ ConnectionStrings:tributo %>" ProviderName="<%$ ConnectionStrings:TRIBUTOConnectionString.ProviderName %>">
                                </asp:SqlDataSource>
                                <asp:GridView ID="grvVehiculo" runat="server" CssClass="grv" Width="900px"
                                    DataSourceID="SqlDSVehiculos" PageSize="20" HeaderStyle-CssClass="btnFrm" AutoGenerateColumns="False"
                                    AlternatingRowStyle-CssClass="grvFila">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="btnEliminar" runat="server" CausesValidation="false" CommandName="pdf"
                                                    CssClass="cssBotonEliminar" Height="16px" OnClick="btnEliminar_Click" ToolTip="Eliminar"
                                                    Width="16px"></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="vehiculo_id" HeaderText="vehiculo_id" SortExpression="vehiculo_id"
                                            Visible="False" />
                                        <asp:BoundField DataField="descripcion_marca" HeaderText="Marca" />
                                        <asp:BoundField DataField="descripcion_modelo" HeaderText="Modelo" />
                                        <asp:BoundField DataField="a&#241;o" HeaderText="A&#241;o" />
                                        <asp:BoundField DataField="nombre_color" HeaderText="Color" />
                                        <asp:BoundField DataField="numero_placa" HeaderText="Placa" SortExpression="numero_placa">
                                            <ItemStyle Width="120px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="vehiculo_id" HeaderText="vehiculo_id" ReadOnly="True"
                                            SortExpression="vehiculo_id" />
                                        <asp:BoundField DataField="contribuyente_id" HeaderText="contribuyente_id" SortExpression="contribuyente_id"
                                            Visible="False">
                                            <ItemStyle Width="120px" />
                                        </asp:BoundField>
                                    </Columns>
                                    <EmptyDataTemplate>
                                        <div class="grvDivVacio">
                                            <table>
                                                <tr>
                                                    <td valign="middle">
                                                        No hay vehículos asociados</td>
                                                </tr>
                                            </table>
                                        </div>
                                    </EmptyDataTemplate>
                                    <HeaderStyle CssClass="cssCabecera" />
                                    <AlternatingRowStyle CssClass="grvFila" />
                                </asp:GridView>
                                &nbsp; &nbsp; &nbsp;&nbsp;
                            </td>
                        </tr>
                        <tr>
                        </tr>
                        <tr>
                        </tr>
                        <tr>
                        </tr>
                        <tr>
                        </tr>
                        <tr>
                        </tr>
                        <tr>
                        </tr>
                        <tr>
                            <td style="height: 30px" align="left" colspan="5">
                                <asp:Button ID="BtnInsertar" OnClick="BtnInsertar_Click" runat="server" Text="Guardar"
                                    CssClass="cssBoton" Width="63px"></asp:Button>&nbsp;
                                <asp:Button ID="BtnCancelar1" OnClick="BtnCancelar1_Click" runat="server" Text="Cancelar"
                                    CssClass="cssBoton" Width="64px"></asp:Button></td>
                        </tr>
                    </tbody>
                </table>
            </ContentTemplate>
        </asp:UpdatePanel>
        <br />
    </form>
</asp:Content>
