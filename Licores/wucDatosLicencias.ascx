﻿<%@ Control Language="VB" AutoEventWireup="false" CodeFile="wucDatosLicencias.ascx.vb"
    Inherits="pagina_operacion_Licores_frmDatosLicencias" %>
<table style="width: 990px" id="Table2" class="cssTabla" onclick="return TABLE1_onclick()">
    <tbody>
        <tr>
            <td align="center" colspan="5" rowspan="2">
                <asp:Label ID="Label50" runat="server" Text="DATOS DE LA LICENCIA" CssClass="subtitulo"></asp:Label>
            </td>
        </tr>
        <tr>
        </tr>
        <tr>
            <td align="left" style="height: 5px">
                <asp:Label ID="Label51" runat="server" Text="Tipo de Licencia:" CssClass="subtitulo"></asp:Label></td>
            <td align="left" colspan="2" style="height: 5px">
                <asp:DropDownList ID="ddlTipoLicencia" runat="server" Width="236px" DataSourceID="SqlDSTipoLicencia"
                    DataTextField="descripcion_tipo_licencia" DataValueField="tipo_licencia_id" AutoPostBack="True" AppendDataBoundItems="True">
                    <asp:ListItem Value="0">SELECCIONE</asp:ListItem>
                </asp:DropDownList>
                </td>
            <td align="left" style="height: 5px">
                <asp:Label ID="Label53" runat="server" Text="Motivo de la Autorización:" CssClass="subtitulo"
                    Width="192px"></asp:Label></td>
            <td align="left" style="height: 5px">
                <asp:DropDownList ID="ddlMotivoAutorizacion" runat="server" Width="236px" DataSourceID="SqlDSMotivoAutorizacion"
                    DataTextField="descripcion_motivo_autorizacion" DataValueField="motivo_autorizacion_id"
                    AutoPostBack="True" AppendDataBoundItems="True" OnSelectedIndexChanged="ddlMotivoAutorizacion_SelectedIndexChanged">
                    <asp:ListItem>SELECCIONE</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="Label58" runat="server" Text="Clasificación de la Licencia:" CssClass="subtitulo"
                    Width="200px"></asp:Label></td>
            <td align="left" colspan="2">
                <asp:DropDownList ID="ddlClasificacionLicencia" runat="server" Width="236px" DataSourceID="SqlDSClasificacionLicencia"
                    DataTextField="descripcion_clasificacion_licencia" DataValueField="clasificacion_licencia_id"
                    AutoPostBack="True" AppendDataBoundItems="True" OnSelectedIndexChanged="ddlClasificacionLicencia_SelectedIndexChanged">
                    <asp:ListItem Value="0">SELECCIONE</asp:ListItem>
                </asp:DropDownList>
                </td>
            <td align="left">
                <asp:Label ID="Label59" runat="server" Text="Estado de la Licencia:" CssClass="subtitulo"></asp:Label></td>
            <td align="left">
                <asp:DropDownList ID="ddlEstadoLicencia" runat="server" Width="236px" DataSourceID="SqlDSEstadoLicencia"
                    DataTextField="descripcion_estado_licencia" DataValueField="estado_licencia_id"
                    AutoPostBack="True" AppendDataBoundItems="True">
                    <asp:ListItem>SELECCIONE</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="Label61" runat="server" Text="Horario:" CssClass="subtitulo"></asp:Label></td>
            <td align="left" colspan="4">
                <asp:DropDownList ID="ddlHorario" runat="server" Width="737px" DataSourceID="SqlDSHorario"
                    DataTextField="descripcion_horario_licencia" DataValueField="horario_licencia_id"
                    AutoPostBack="True" EnableViewState="False" AppendDataBoundItems="True">
                    <asp:ListItem>SELECCIONE</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="lblNumeroControl" runat="server" Text="Número de Control:" CssClass="subtitulo"></asp:Label></td>
            <td align="left" colspan="2">
                <asp:TextBox ID="txtNumeroControl" runat="server" Width="200px" ToolTip="Numero de Control para la licencia a la Fecha"></asp:TextBox></td>
            <td align="left">
                &nbsp;</td>
            <td align="left">
                </td>
        </tr>
        <tr>
            <td align="left" colspan="3">
                <asp:Label ID="lblNumeroControl1" runat="server" CssClass="subtitulo" ForeColor="Maroon"></asp:Label></td>
            <td align="left">
                <asp:TextBox ID="txtNumeroControl1" runat="server" ToolTip="Numero de Control para la Primera Renovacion"
                    Width="200px"></asp:TextBox></td>
            <td align="left">
            </td>
        </tr>
        <tr>
            <td align="left" colspan="3">
                <asp:Label ID="lblNumeroControl2" runat="server" CssClass="subtitulo" ForeColor="Maroon"></asp:Label></td>
            <td align="left">
                <asp:TextBox ID="txtNumeroControl2" runat="server" ToolTip="Numero de Control para la Segunda Renovacion"
                    Width="200px"></asp:TextBox></td>
            <td align="left">
            </td>
        </tr>
        <tr>
            <td align="left" style="height: 25px">
                <asp:Label ID="label68" runat="server" Text="Número de Renovación:" CssClass="subtitulo"></asp:Label></td>
            <td align="left" colspan="2">
                <asp:Label ID="lblRenovacion" runat="server" Text="0" CssClass="normal" Width="8px"></asp:Label></td>
            <td align="left">
            </td>
            <td align="left">
            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="Label52" runat="server" Text="Número de Solicitud:" CssClass="subtitulo"></asp:Label></td>
            <td align="left" colspan="2" style="height: 26px">
                <asp:TextBox ID="txtNumeroSolicitud" runat="server" Width="200px"></asp:TextBox></td>
            <td align="left" style="height: 26px">
                <asp:Label ID="Label80" runat="server" Text="Número de Licencia:" CssClass="subtitulo"></asp:Label></td>
            <td align="left" style="height: 26px">
                <asp:TextBox ID="txtNumeroLicencia" runat="server" Width="168px" Enabled="False" BorderStyle="Solid" BorderWidth="1px" ForeColor="Silver">AUTOGENERADO</asp:TextBox></td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="Label71" runat="server" Text="Fecha de Solicitud:" CssClass="subtitulo"></asp:Label></td>
            <td align="left" nowrap="noWrap" colspan="2">
                <asp:TextBox ID="txtFechaSolicitud" runat="server" Width="168px"></asp:TextBox>
                <asp:Image ID="imgFechaSolicitud" runat="server" ImageUrl="~/imagen/icono/icono-calendar.gif"
                    name="imgFechaSolicitud"></asp:Image>
          
          <!--- <script type="text/javascript">
                          var fecha = document.getElementById('<%=txtFechaSolicitud.ClientID%>');                     
                          iniciarCalendario(fecha,'imgFechaSolicitud','%d/%m/%Y',false,{x:520,y:820}); 
                </script> --->
            </td>
            <td align="left">
                <asp:Label ID="Label74" runat="server" Text="Fecha de Emisión:" CssClass="subtitulo"></asp:Label></td>
            <td align="left" nowrap="noWrap">
                <asp:TextBox ID="txtFechaEmision" runat="server" Width="168px"></asp:TextBox>
                <asp:Image ID="imgFechaEmision" runat="server" ImageUrl="~/imagen/icono/icono-calendar.gif"
                    name="imgFechaEmision"></asp:Image>

          <!--- <script type="text/javascript">
                           var fecha = document.getElementById('<%=txtFechaEmision.ClientID%>');                     
                           iniciarCalendario(fecha,'imgFechaEmision','%d/%m/%Y',false,{x:1040,y:820});
                </script> --->

            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="Label77" runat="server" Text="Fecha de Inicio:" CssClass="subtitulo"></asp:Label></td>
            <td align="left" colspan="2">
                <asp:TextBox ID="txtFechaInicio" runat="server" Width="168px"></asp:TextBox>
                <asp:Image ID="imgFechaInicio" runat="server" ImageUrl="~/imagen/icono/icono-calendar.gif"
                    name="imgFechaInicio"></asp:Image>

                <!--- <script type="text/javascript">
                            var fecha = document.getElementById('<%=txtFechaInicio.ClientID%>');                     
                            iniciarCalendario(fecha,'imgFechaInicio','%d/%m/%Y',false,{x:520,y:870});
                </script> --->

            </td>
            <td align="left">
                <asp:Label ID="lblFechaVencimiento" runat="server" Text="Fecha de Vencimiento:" CssClass="subtitulo"></asp:Label></td>
            <td align="left">
                <asp:TextBox ID="txtFechaVencimiento" runat="server" Width="168px"></asp:TextBox>
                <asp:Image ID="imgFechaVencimiento" runat="server" ImageUrl="~/imagen/icono/icono-calendar.gif"
                    name="imgFechaVencimiento"></asp:Image>

              <!---  <script type="text/javascript">
                             var fecha = document.getElementById('<%=txtFechaVencimiento.ClientID%>');                     
                             iniciarCalendario(fecha,'imgFechaVencimiento','%d/%m/%Y', false,{x:1040,y:870});
                </script> --->

            </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="Label83" runat="server" Text="Año:" CssClass="subtitulo"></asp:Label></td>
            <td align="left" colspan="2">
                <asp:TextBox ID="txtAnn" runat="server" Width="72px"></asp:TextBox></td>
            <td align="left">
                <asp:Label ID="lblLicenciaSENIAT" runat="server" Text="Número de Licencia SENIAT:"
                    CssClass="subtitulo" Visible="False"></asp:Label></td>
            <td align="left">
                <asp:TextBox ID="txtLicenciaSENIAT" runat="server" Width="164px" Visible="False"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="Label6" runat="server" Text="Número de Expediente:" CssClass="subtitulo"></asp:Label></td>
            <td align="left" colspan="2">
                <asp:TextBox ID="txtNumeroExpediente" runat="server" Width="163px"></asp:TextBox></td>
            <td align="left">
                </td>
            <td align="left">
                </td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="lblEvento" runat="server" Text="Nombre del Evento:" CssClass="subtitulo"></asp:Label></td>
            <td colspan="4" align="left" >
                <asp:TextBox ID="txtEvento" runat="server" Width="500px" MaxLength="300"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="lblKiosko" runat="server" Text="Cantidad de Puntos o Kiosko:" CssClass="subtitulo"></asp:Label></td>
            <td colspan="4" align="left">
                &nbsp;<asp:DropDownList ID="ddpNumeroKiosko" runat="server" AutoPostBack="True">
                </asp:DropDownList>
                <asp:TextBox ID="txtKiosko" runat="server" Width="416px" MaxLength="100" ReadOnly="True"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="left">
                <asp:Label ID="lblUbicacionEv" runat="server" Text="Ubicacion del Evento:" CssClass="subtitulo"></asp:Label></td>
            <td colspan="4" align="left">
                <asp:TextBox ID="txtUbicacionEv" runat="server" Width="730px" MaxLength="500"></asp:TextBox></td>
        </tr>
        <tr>
            <td align="left" colspan="5">
                &nbsp;<asp:Label ID="lblMensaje" runat="server" CssClass="subtitulo" ForeColor="DarkRed"
                    Text="Mensaje  "></asp:Label>
                <asp:Button ID="BtnExpirarLicencia" runat="server" CssClass="cssBoton" Text="Anular" Width="68px" OnClientClick='return confirm("¿ Esta seguro de anular la licencia ?");' />
                </td>
        </tr>
    </tbody>
</table>
<asp:SqlDataSource ID="SqlDSTipoLicencia" runat="server" ConnectionString="<%$ ConnectionStrings:TRIBUTOConnectionString %>"
    ProviderName="<%$ ConnectionStrings:TRIBUTOConnectionString.ProviderName %>"
    SelectCommand="SELECT tipo_licencia_id, descripcion_tipo_licencia, estemporal FROM TIPO_LICENCIA">
</asp:SqlDataSource>
<asp:SqlDataSource ID="SqlDSClasificacionLicencia" runat="server" ConnectionString="<%$ ConnectionStrings:TRIBUTOConnectionString %>"
    ProviderName="<%$ ConnectionStrings:TRIBUTOConnectionString.ProviderName %>"
    SelectCommand="SELECT clasificacion_licencia_id, descripcion_clasificacion_licencia,esdistribuidor FROM vis_clasificacion_licencia">
</asp:SqlDataSource>
<asp:SqlDataSource ID="SqlDSMotivoAutorizacion" runat="server" ConnectionString="<%$ ConnectionStrings:TRIBUTOConnectionString %>"
    ProviderName="<%$ ConnectionStrings:TRIBUTOConnectionString.ProviderName %>"
    SelectCommand="SELECT motivo_autorizacion_id, descripcion_motivo_autorizacion,motivo_accion FROM MOTIVO_AUTORIZACION WHERE (tipo_licencia_id = @tipo_licencia_id)">
    <SelectParameters>
        <asp:Parameter Name="tipo_licencia_id" />
    </SelectParameters>
</asp:SqlDataSource>
