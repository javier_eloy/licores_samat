﻿Imports System.Data.SqlClient
Imports System.Data
Imports modModelContribuyente
Imports modModelLicencia
Imports modGeneralTributo
Imports modPermiso

Partial Class pagina_operacion_Licores_frmEditAsocVehiculos
    Inherits System.Web.UI.Page

    Private Enum ColumnaVehiculo As Integer
        ccSeleccionar
        ccVehiculoId
    End Enum

    Dim lngContribuyenteId As Long
    Dim lngLicenciaId As Long
    Dim arrIdsVehiculos As New ArrayList

    '---- Definicion de objetos del contro datos del contribuyente para permitir el acceso ---
    Protected WithEvents lblNombreContribuyente As Label
    Protected WithEvents lblEstadoContribuyente As Label
    Protected WithEvents lblEstClasificacionContribuyente As New Label
    Protected WithEvents lblLemaContribuyente As New Label
    Protected WithEvents lblCedulaContribuyente As New Label
    Protected WithEvents lblRIFContribuyente As New Label
    Protected WithEvents lblRIMContribuyente As New Label
    Protected WithEvents lblUsoContribuyente As New Label
    Protected WithEvents lblParroquiaContribuyente As New Label
    Protected WithEvents lblSectorContribuyente As New Label
    Protected WithEvents lblDireccionContribuyente As New Label
    Protected WithEvents lblTelefonoContribuyente As New Label
    Protected WithEvents lblTlfAdicionalContribuyente As New Label
    Protected WithEvents lblCelularContribuyente As New Label
    Protected WithEvents lblCelAdicionalContribuyente As New Label
    Protected WithEvents lblEmailContribuyente As New Label
    Protected WithEvents lblPaginaContribuyente As New Label
    Protected WithEvents lblParroquiaEstablecimiento As New Label
    Protected WithEvents lblSectorEstablecimiento As New Label
    Protected WithEvents lblDireccionEstablecimiento As New Label
    Protected WithEvents lblNumLicencia As New Label
    Protected WithEvents lblSeniat As New Label
    '--- fin Definicion -------

    '--- Definicion de objetos del control de Datos de las licencias ---
    Protected WithEvents ddlMotivoAutorizacion As DropDownList
    Protected WithEvents ddlEstadoLicencia As DropDownList
    Protected WithEvents ddlTipoLicencia As DropDownList
    Protected WithEvents ddlHorario As DropDownList
    Protected WithEvents ddlClasificacionLicencia As DropDownList

    Protected txtNumeroControl As TextBox
    Protected txtNumeroSolicitud As TextBox
    Protected txtFechaSolicitud As TextBox
    Protected txtFechaEmision As TextBox
    Protected txtFechaInicio As TextBox
    Protected txtFechaVencimiento As TextBox
    Protected txtAnn As TextBox
    Protected txtLicenciaSENIAT As TextBox
    Protected txtNumeroExpediente As TextBox
    Protected txtNumeroLicencia As TextBox

    Protected imgFechaSolicitud As Image
    Protected imgFechaEmision As Image
    Protected imgFechaInicio As Image
    Protected imgFechaVencimiento As Image
    Protected lblRenovacion As Label
    '--- fin Definicion ---

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        lblNombreContribuyente = ucDatosContribuyentes.FindControl("lblNombreContribuyente")
        lblEstadoContribuyente = ucDatosContribuyentes.FindControl("lblEstadoContribuyente")
        lblEstClasificacionContribuyente = ucDatosContribuyentes.FindControl("lblEstClasificacionContribuyente")
        lblLemaContribuyente = ucDatosContribuyentes.FindControl("lblLemaContribuyente")
        lblCedulaContribuyente = ucDatosContribuyentes.FindControl("lblCedulaContribuyente")
        lblRIFContribuyente = ucDatosContribuyentes.FindControl("lblRIFContribuyente")
        lblRIMContribuyente = ucDatosContribuyentes.FindControl("lblRIMContribuyente")
        lblUsoContribuyente = ucDatosContribuyentes.FindControl("lblUsoContribuyente")
        lblParroquiaContribuyente = ucDatosContribuyentes.FindControl("lblParroquiaContribuyente")
        lblSectorContribuyente = ucDatosContribuyentes.FindControl("lblSectorContribuyente")
        lblDireccionContribuyente = ucDatosContribuyentes.FindControl("lblDireccionContribuyente")
        lblTelefonoContribuyente = ucDatosContribuyentes.FindControl("lblTelefonoContribuyente")
        lblTlfAdicionalContribuyente = ucDatosContribuyentes.FindControl("lblTlfAdicionalContribuyente")
        lblCelularContribuyente = ucDatosContribuyentes.FindControl("lblCelularContribuyente")
        lblCelAdicionalContribuyente = ucDatosContribuyentes.FindControl("lblCelAdicionalContribuyente")
        lblEmailContribuyente = ucDatosContribuyentes.FindControl("lblEmailContribuyente")
        lblPaginaContribuyente = ucDatosContribuyentes.FindControl("lblPaginaContribuyente")
        lblParroquiaEstablecimiento = ucDatosContribuyentes.FindControl("lblParroquiaEstablecimiento")
        lblSectorEstablecimiento = ucDatosContribuyentes.FindControl("lblSectorEstablecimiento")
        lblDireccionEstablecimiento = ucDatosContribuyentes.FindControl("lblDireccionEstablecimiento")
        lblNumLicencia = ucDatosContribuyentes.FindControl("lblNumLicencia")
        lblSeniat = ucDatosContribuyentes.FindControl("lblSeniat")

        '---- Datos de Licencia
        ddlMotivoAutorizacion = ucDatosLicencia.FindControl("ddlMotivoAutorizacion")
        ddlEstadoLicencia = ucDatosLicencia.FindControl("ddlEstadoLicencia")
        ddlTipoLicencia = ucDatosLicencia.FindControl("ddlTipoLicencia")
        ddlHorario = ucDatosLicencia.FindControl("ddlHorario")
        ddlClasificacionLicencia = ucDatosLicencia.FindControl("ddlClasificacionLicencia")
        txtNumeroControl = ucDatosLicencia.FindControl("txtNumeroControl")
        txtNumeroSolicitud = ucDatosLicencia.FindControl("txtNumeroSolicitud")
        txtFechaSolicitud = ucDatosLicencia.FindControl("txtFechaSolicitud")
        txtFechaEmision = ucDatosLicencia.FindControl("txtFechaEmision")
        txtFechaInicio = ucDatosLicencia.FindControl("txtFechaInicio")
        txtFechaVencimiento = ucDatosLicencia.FindControl("txtFechaVencimiento")
        txtAnn = ucDatosLicencia.FindControl("txtAnn")
        txtLicenciaSENIAT = ucDatosLicencia.FindControl("txtLicenciaSENIAT")
        txtNumeroExpediente = ucDatosLicencia.FindControl("txtNumeroExpediente")
        txtNumeroLicencia = ucDatosLicencia.FindControl("txtNumeroLicencia")
        imgFechaSolicitud = ucDatosLicencia.FindControl("imgFechaSolicitud")
        imgFechaEmision = ucDatosLicencia.FindControl("imgFechaEmision")
        imgFechaInicio = ucDatosLicencia.FindControl("imgFechaInicio")
        imgFechaVencimiento = ucDatosLicencia.FindControl("imgFechaVencimiento")
        lblRenovacion = ucDatosLicencia.FindControl("lblRenovacion")
    End Sub

    Protected Sub BtnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancelar.Click
        Response.Redirect(ResolveUrl("~/pagina/operacion/Licores/frmConsultaLicencia.aspx"))
    End Sub

    Protected Sub llenarArrIdsVehiculo(ByVal strIdsVehiculo As String)
        Dim x As Integer
        Dim strNum As String = ""

        For x = 0 To strIdsVehiculo.Length - 1
            If strIdsVehiculo.Chars(x) <> "," Then
                strNum &= strIdsVehiculo.Chars(x)
            Else
                arrIdsVehiculos.Add(strNum)
                strNum = ""
            End If
        Next
        arrIdsVehiculos.Add(strNum)
    End Sub

    Protected Function getIdsVehiculo() As String
        Dim x As Integer
        Dim strNum As String = ""

        For x = 0 To arrIdsVehiculos.Count - 1
            strNum &= arrIdsVehiculos.Item(x).ToString & ","
        Next
        If strNum <> "" Then
            strNum = strNum.Substring(0, strNum.Length - 1)
        Else
            strNum = "0"
        End If
        Session("strIdsVehiculo") = strNum
        Return strNum
    End Function

    Protected Sub actualizarGrid()
        SqlDSVehiculos.SelectCommand = "SELECT vehiculo_id, año, numero_placa, numero_calcomania, fecha_registro_sistema, fecha_compra, publicidad_rotulada, activo, descripcion_estado_vehiculo, descripcion_tipo_vehiculo, descripcion_modelo, nombre_color, descripcion_marca, contribuyente_id FROM vis_vehiculo_contribuyente WHERE vehiculo_id IN (" & getIdsVehiculo() & ")"
        SqlDSVehiculos.Select(New System.Web.UI.DataSourceSelectArguments)
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim transaccion As Integer
        Dim strSQL As String

        Session("Action") = "frmEditAsocVehiculos" '---- Identifica las acciones a tomar en el User Control
        Try

            If Session("strIdsVehiculo") <> Nothing AndAlso Session("strIdsVehiculo") <> "0" Then
                llenarArrIdsVehiculo(Session("strIdsVehiculo"))
            End If
         
            lngContribuyenteId = CType(Request("ID"), Integer)
            Session("contribuyente_id") = lngContribuyenteId
            lngLicenciaId = CType(Request("licenciaId"), Integer)
            Session("licencia_id") = lngLicenciaId
            transaccion = CType(Request("transac"), Integer)
        Catch
            lngContribuyenteId = 0
        End Try

        If Not Page.IsPostBack Then

            If Request("IdsVehiculo") <> Nothing AndAlso Request("IdsVehiculo") <> "0" Then
                arrIdsVehiculos.Clear()
                llenarArrIdsVehiculo(Request("IdsVehiculo"))
                Session("strIdsVehiculo") = Request("IdsVehiculo")
                actualizarGrid()
            Else
                strSQL = "select -1 as esta, v.vehiculo_id, V.año, v.numero_placa, "
                strSQL += " v.numero_calcomania, v.activo, v.descripcion_estado_vehiculo,"
                strSQL += " v.descripcion_tipo_vehiculo, v.descripcion_modelo, "
                strSQL += " v.nombre_color, v.descripcion_marca, v.contribuyente_id"
                strSQL += " FROM vis_vehiculo_contribuyente v"
                strSQL += " INNER JOIN vehiculoxlicencia vxl  ON vxl.vehiculo_id = v.vehiculo_id"
                strSQL += " WHERE vxl.licencia_id = " + lngLicenciaId.ToString
                strSQL += " AND v.contribuyente_id = " + lngContribuyenteId.ToString

                SqlDSVehiculos.SelectCommand = strSQL
                SqlDSVehiculos.Select(New System.Web.UI.DataSourceSelectArguments)

            End If

            DeshabilitarCache(Me)
            Dim Usuario As Sesion = Session("Usuario")
            'Validar(Usuario, Me) OJO TENGO QUE DESCOMENTAR ESTO

            lngContribuyenteId = ucDatosContribuyentes.CargaDatosContribuyenteLicencia(lngLicenciaId)

            If transaccion = 1 Then 'VIENE DE CONSULTA DIRECTAMENTE
                ucDatosLicencia.CargaDatosLicencia(lngLicenciaId)
            Else 'VIENE DE LA MODIFICACIÓN DE UNA LICENCIA EXISTENTE
                ucDatosLicencia.AsignaDatosLicencia(Request("motivoAutorizacionId"), _
                    Request("estadoLicenciaId"), Request("tipoLicenciaId"), _
                    Request("horarioId"), 17, _
                    Request("numeroControl"), _
                    Request("numeroSolicitud"), Request("fechaSolicitud"), _
                    Request("fechaEmision"), Request("fechaInicio"), _
                    Request("fechaVencimiento"), Request("anno"), Request("numeroSeniat"), _
                    Request("numeroExpediente"), Request("numeroLicencia"), _
                    Request("evento"), Request("ikiosko"), Request("kiosko"), Request("ubicacionev"))
                'OJO cambiar en configuración  el numero 17
            End If

        End If
    End Sub

    Protected Sub BtnCancelar1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect(ResolveUrl("~/pagina/operacion/Licores/frmConsultaLicencia.aspx?ID=" & Session("contribuyente_id")))
    End Sub

    Protected Sub BtnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim arrvehiculo As New ArrayList

        For Each fila As GridViewRow In Me.grvVehiculo.Rows
            arrvehiculo.Add(fila.Cells(7).Text)
        Next
        Session("arrIds") = arrvehiculo
        Response.Redirect(ResolveUrl("~/pagina/operacion/Licores/frmBusquedaVehiculo.aspx?ID=" & Session("contribuyente_id") & "&tipoLicenciaId=" & Me.ddlTipoLicencia.SelectedValue & "&motivoAutorizacionId=" & Me.ddlMotivoAutorizacion.SelectedValue & "&estadoLicenciaId=" & Me.ddlEstadoLicencia.SelectedValue & "&horarioId=" & Me.ddlHorario.SelectedValue & "&licenciaId=" & Session("licencia_Id") & "&numeroControl=" & Me.txtNumeroControl.Text & "&numeroSolicitud=" & Me.txtNumeroSolicitud.Text & "&fechaSolicitud=" & Me.txtFechaSolicitud.Text & "&fechaEmision=" & Me.txtFechaEmision.Text & "&fechaInicio=" & Me.txtFechaInicio.Text & "&fechaVencimiento=" & Me.txtFechaVencimiento.Text & "&anno=" & Me.txtAnn.Text & "&numeroSeniat=" & Me.txtLicenciaSENIAT.Text & "&numeroExpediente=" & Me.txtNumeroExpediente.Text & "&numeroLicencia=" & Me.txtNumeroLicencia.Text & "&clasificacionLicenciaId=" & Me.ddlClasificacionLicencia.SelectedValue & "&origen=edit"))
    End Sub

    Protected Sub BtnInsertar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim arrVehiculo As New ArrayList

        For Each fila As GridViewRow In Me.grvVehiculo.Rows
            arrVehiculo.Add(fila.Cells(7).Text)
        Next

        If ucDatosLicencia.InsertaLicenciaVehiculo(lngContribuyenteId, lngLicenciaId, lblUsoContribuyente.Text, _
                                             lblSeniat.Text, lblRIMContribuyente.Text, _
                                             lblRIFContribuyente.Text, lblNombreContribuyente.Text, _
                                             lblParroquiaContribuyente.Text, lblSectorContribuyente.Text, _
                                             False, arrVehiculo, _
                                            UpdatePanel1) Then
            Response.Redirect(ResolveUrl("~/pagina/operacion/Licores/frmConsultaLicencia.aspx?ID=" & lngContribuyenteId.ToString))
        End If
        
    End Sub

    Protected Sub grvVehiculo_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles grvVehiculo.RowCreated
        If e.Row.Cells.Count > 3 Then
            e.Row.Cells(7).Visible = False
            e.Row.Cells(8).Visible = False
        End If
    End Sub

    Protected Sub btnEliminar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim fila As GridViewRow = CType(sender, LinkButton).NamingContainer

        If arrIdsVehiculos.Count = 0 Then
            For Each filaV As GridViewRow In Me.grvVehiculo.Rows
                arrIdsVehiculos.Add(filaV.Cells(7).Text)
            Next
        End If

        arrIdsVehiculos.Remove(fila.Cells(7).Text)
        actualizarGrid()
        'Response.Redirect(ResolveUrl("~/pagina/operacion/Solvencias/frmSolvenciaImpresion.aspx?ID=" & Request("ID") & "&solvenciaId=" & fila.Cells(6).Text))
    End Sub

End Class
