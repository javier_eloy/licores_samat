﻿Imports System.Data.SqlClient
Partial Class pagina_operacion_Licores_frmLicenciaAprobadaDistribuidor
    Inherits System.Web.UI.Page

    Protected Sub Grid_Licencias_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

    End Sub

    Protected Sub Button5_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Call Actualiza_Grig()
    End Sub

    Private Sub Actualiza_Grig()
        Dim Query As String = ""
        Try
            Query = "SELECT [vehiculo_id],[marca_vehiculo_id],[modelo_vehiculo_id],[año],[numero_placa] FROM [TRIBUTO].[dbo].[vis_vehiculo] where contribuyente_id = " & Session("contribuyente_id").ToString()
            SqlDSVehiculos.SelectCommand = Query
            SqlDSVehiculos.Update()
        Catch

        End Try

    End Sub

    Protected Sub BtnCancelar1_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect(ResolveUrl("~/pagina/operacion/Licores/frmConsultaLicencia.aspx"))
    End Sub

    Protected Sub BtnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect(ResolveUrl("~/pagina/operacion/Licores/frmConsultaLicencia.aspx"))
    End Sub

    Protected Sub BtnInsertar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Call GuardaLicencia()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub GuardaLicencia()
        Dim sql As String = vbNullString
        Dim cnn As SqlConnection = Nothing
        Dim cmd As SqlCommand = Nothing
        Dim dr As System.Data.IDataReader
        Dim id As Integer

        cnn = modBaseDatos.obtenerConexion

        Try
            cnn.Open()

            sql = "Select Max(licencia_id) + 1 from LICENCIA"
            cmd = New SqlCommand(sql, cnn)
            dr = cmd.ExecuteReader()
            If dr.Read() = True Then
                id = CInt(dr.GetValue(0).ToString())
            End If
            dr.Close()


            sql = "Insert into LICENCIA "
            sql += "(licencia_id, "
            sql += "contribuyente_id, "
            sql += "usuario_id, "
            sql += "tipo_licencia_id, "
            sql += "motivo_autorizacion_id, "
            sql += "clasificacion_licencia_id, "
            sql += "estado_licencia_id, "
            sql += "horario_licencia_id, "
            sql += "num_control, "
            sql += "num_autorizacion, "
            sql += "num_solicitud, "
            sql += "fecha_solicitud, "
            sql += "fecha_inicio_licencia, "
            sql += "fecha_vencimiento_licencia, "
            sql += "año) values "
            sql += "(" & id & ", "
            sql += "" & Session("contribuyente_id") & ", "
            sql += "" & CType(Session("Usuario"), Sesion).UsuarioID.ToString & ", "
            sql += "" & ddlTipoLicencia.SelectedValue & ", "
            sql += "" & ddlMotivoAutorizacion.SelectedValue & ", "
            sql += "" & ddlClasificacionLicencia.SelectedValue & ", "
            sql += "" & ddlEstadoLicencia.SelectedValue & ", "
            sql += "" & ddlHorario.SelectedValue & ", "
            sql += "'" & txtNumeroControl.Text & "', "
            sql += "'" & txtNumeroAutorizacion.Text & "', "
            sql += "'" & txtNumeroSolicitud.Text & "', "
            sql += "'" & Format(CDate(txtFechaSolicitud.Text), "yyyyMMdd") & "', "
            sql += "'" & Format(CDate(txtFechaEmision.Text), "yyyyMMdd") & "', "
            sql += "'" & Format(CDate(txtFechaVencimiento.Text), "yyyyMMdd") & "', "
            sql += "'" & txtAnn.Text & "')"
            cmd = New SqlCommand(sql, cnn)
            cmd.ExecuteNonQuery()
            System.Windows.Forms.MessageBox.Show("Datos registrados con exito.")
            Call Nuevo()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub CargaDatosContribuyente()
        lblUsoContribuyente.Text = Session("UsoContribuyente")
        lblNombreContribuyente.Text = Session("NombreContribuyente")
        lblLemaContribuyente.Text = Session("LemaContribuyente")
        lblRIFContribuyente.Text = Session("RIFContribuyente")
        lblCedulaContribuyente.Text = Session("CedulaContribuyente")
        lblRIMContribuyente.Text = Session("RIMContribuyente")
        lblParroquiaContribuyente.Text = Session("ParroquiaContribuyente")
        lblDireccionContribuyente.Text = Session("DireccionContribuyente")
        lblTelefonoContribuyente.Text = Session("TelefonoContribuyente")
        lblCelularContribuyente.Text = Session("CelularContribuyente")
        lblEmailContribuyente.Text = Session("EmailContribuyente")
        lblEstadoContribuyente.Text = Session("EstadoContribuyente")
        lblEstClasificacionContribuyente.Text = Session("EstClasificacionContribuyente")
        lblSectorContribuyente.Text = Session("SectorContribuyente")
        lblTlfAdicionalContribuyente.Text = Session("TlfAdicionalContribuyente")
        lblCelAdicionalContribuyente.Text = Session("CelAdicionalContribuyente")
        lblPaginaContribuyente.Text = Session("PaginaContribuyente")
        lblParroquiaEstablecimiento.Text = Session("ParroquiaEstablecimiento")
        lblSectorEstablecimiento.Text = Session("SectorEstablecimiento")
        lblDireccionEstablecimiento.Text = Session("DireccionEstablecimiento")
        'lblNumLicencia.Text = Session("NumLicencia")
        lblSeniat.Text = Session("Seniat")
    End Sub

    Private Sub Nuevo()
        ddlTipoLicencia.ClearSelection()
        ddlMotivoAutorizacion.ClearSelection()
        ddlClasificacionLicencia.ClearSelection()
        ddlEstadoLicencia.ClearSelection()
        ddlHorario.ClearSelection()
        txtNumeroControl.Text = ""
        txtNumeroAutorizacion.Text = ""
        txtNumeroSolicitud.Text = ""
        txtFechaSolicitud.Text = ""
        txtFechaEmision.Text = ""
        txtFechaVencimiento.Text = ""
        txtAnn.Text = ""
    End Sub

    Protected Sub Grid_Vehiculos_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid_Vehiculos.RowCommand
        Dim row As GridViewRow = Grid_Vehiculos.Rows(Convert.ToInt32(e.CommandArgument))
        Call EliminaVehiculo(CInt(row.Cells(1).Text))
    End Sub

    Protected Sub Grid_Vehiculos_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grid_Vehiculos.RowCreated
        If e.Row.Cells.Count > 2 Then
            e.Row.Cells(ColumnaVehiculo.ccVehiculoId).Visible = False
        End If
    End Sub

    Private Enum ColumnaVehiculo As Integer
        ccSeleccionar
        ccVehiculoId
    End Enum

    Protected Sub btnFechaSolicitud_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        calendario.Visible = True
        txtindice.Text = "1"
    End Sub

    Protected Sub FechaVencimiento_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        calendario.Visible = True
        txtindice.Text = "2"
    End Sub

    Protected Sub btnFechaEmision_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        calendario.Visible = True
        txtindice.Text = "3"
    End Sub

    Protected Sub calendario_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Select Case txtindice.Text
            Case "1"
                InsertaFecha(txtFechaSolicitud)
            Case "2"
                InsertaFecha(txtFechaVencimiento)
            Case "3"
                InsertaFecha(txtFechaEmision)
        End Select
    End Sub

    Private Sub InsertaFecha(ByVal txt As TextBox)
        Dim ann As Integer
        txt.Text = calendario.SelectedDate
        If txtindice.Text = "3" Then
            ann = CInt(calendario.SelectedDate.Year)
            ann = ann + 1
            txtFechaVencimiento.Text = calendario.SelectedDate.Day & "/" & Format(calendario.SelectedDate.Month, "00") & "/" & ann.ToString
        End If
        calendario.Visible = False
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call CargaDatosContribuyente()
    End Sub

    Private Sub EliminaVehiculo(ByVal IdVehiculo As Integer)
        Dim sql As String = vbNullString
        Dim cnn As SqlConnection = Nothing
        Dim cmd As SqlCommand = Nothing

        cnn = modBaseDatos.obtenerConexion

        Try
            cnn.Open()
            sql = "Update VEHICULO set activo = 2 WHERE vehiculo_id = " & IdVehiculo
            cmd = New SqlCommand(sql, cnn)
            cmd.ExecuteNonQuery()
            System.Windows.Forms.MessageBox.Show("Vehiculo Eliminado.")
        Catch ex As Exception

        End Try

    End Sub
End Class
