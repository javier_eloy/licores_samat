﻿Imports System.Data.SqlClient
Imports modPermiso
Imports modBaseDatos
Imports modValidacion
Imports CrystalDecisions.CrystalReports.Engine
Imports CrystalDecisions.Shared
Imports System.Data
Imports System.Data.Sql


Partial Class pagina_operacion_Licores_frmConsultaLicencia
    Inherits System.Web.UI.Page

    Private lngContribuyenteId As Long
    Private Enum ColumnaLicencia As Integer
        ccSeleccionar = 0
        ccLicenciaId = 2
        ccTipoLicenciaTexto = 6
        ccEsDistribuidor = 8
        ccEsTemporal = 9
    End Enum

    '---- Definicion de objetos del contro datos del contribuyente para permitir el acceso ---
    Protected WithEvents lblNombreContribuyente As Label
    Protected WithEvents lblEstadoContribuyente As Label
    Protected WithEvents lblEstClasificacionContribuyente As New Label
    Protected WithEvents lblLemaContribuyente As New Label
    Protected WithEvents lblCedulaContribuyente As New Label
    Protected WithEvents lblRIFContribuyente As New Label
    Protected WithEvents lblRIMContribuyente As New Label
    Protected WithEvents lblUsoContribuyente As New Label
    Protected WithEvents lblParroquiaContribuyente As New Label
    Protected WithEvents lblSectorContribuyente As New Label
    Protected WithEvents lblDireccionContribuyente As New Label
    Protected WithEvents lblTelefonoContribuyente As New Label
    Protected WithEvents lblTlfAdicionalContribuyente As New Label
    Protected WithEvents lblCelularContribuyente As New Label
    Protected WithEvents lblCelAdicionalContribuyente As New Label
    Protected WithEvents lblEmailContribuyente As New Label
    Protected WithEvents lblPaginaContribuyente As New Label
    Protected WithEvents lblParroquiaEstablecimiento As New Label
    Protected WithEvents lblSectorEstablecimiento As New Label
    Protected WithEvents lblDireccionEstablecimiento As New Label
    Protected WithEvents lblNumLicencia As New Label
    Protected WithEvents lblSeniat As New Label
    '--- fin Definicion -------

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        lblNombreContribuyente = ucDatosContribuyentes.FindControl("lblNombreContribuyente")
        lblEstadoContribuyente = ucDatosContribuyentes.FindControl("lblEstadoContribuyente")
        lblEstClasificacionContribuyente = ucDatosContribuyentes.FindControl("lblEstClasificacionContribuyente")
        lblLemaContribuyente = ucDatosContribuyentes.FindControl("lblLemaContribuyente")
        lblCedulaContribuyente = ucDatosContribuyentes.FindControl("lblCedulaContribuyente")
        lblRIFContribuyente = ucDatosContribuyentes.FindControl("lblRIFContribuyente")
        lblRIMContribuyente = ucDatosContribuyentes.FindControl("lblRIMContribuyente")
        lblUsoContribuyente = ucDatosContribuyentes.FindControl("lblUsoContribuyente")
        lblParroquiaContribuyente = ucDatosContribuyentes.FindControl("lblParroquiaContribuyente")
        lblSectorContribuyente = ucDatosContribuyentes.FindControl("lblSectorContribuyente")
        lblDireccionContribuyente = ucDatosContribuyentes.FindControl("lblDireccionContribuyente")
        lblTelefonoContribuyente = ucDatosContribuyentes.FindControl("lblTelefonoContribuyente")
        lblTlfAdicionalContribuyente = ucDatosContribuyentes.FindControl("lblTlfAdicionalContribuyente")
        lblCelularContribuyente = ucDatosContribuyentes.FindControl("lblCelularContribuyente")
        lblCelAdicionalContribuyente = ucDatosContribuyentes.FindControl("lblCelAdicionalContribuyente")
        lblEmailContribuyente = ucDatosContribuyentes.FindControl("lblEmailContribuyente")
        lblPaginaContribuyente = ucDatosContribuyentes.FindControl("lblPaginaContribuyente")
        lblParroquiaEstablecimiento = ucDatosContribuyentes.FindControl("lblParroquiaEstablecimiento")
        lblSectorEstablecimiento = ucDatosContribuyentes.FindControl("lblSectorEstablecimiento")
        lblDireccionEstablecimiento = ucDatosContribuyentes.FindControl("lblDireccionEstablecimiento")
        lblNumLicencia = ucDatosContribuyentes.FindControl("lblNumLicencia")
        lblSeniat = ucDatosContribuyentes.FindControl("lblSeniat")
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Try
            lngContribuyenteId = CType(Request("ID"), Integer)
            Session("contribuyente_id") = lngContribuyenteId
        Catch
            lngContribuyenteId = 0
        End Try

        If Not Page.IsPostBack Then
            DeshabilitarCache(Me)
            Dim Usuario As Sesion = Session("Usuario")
            'Validar(Usuario, Me) OJO TENGO QUE DESCOMENTAR ESTO
            ucDatosContribuyentes.CargarContribuyente(lngContribuyenteId)
            ActualizarGrid()
        End If
    End Sub

    Private Sub MostrarLicencia(ByVal licencia_id As Long, ByVal EsTemporal As Boolean)
        Dim rpt As New ReportDocument
        Dim strSQL As String = String.Empty
        Dim strIntendenteNombre As String = "<sin informacion>"
        Dim drRead As SqlDataReader
        Dim dtTable As DataTable = New DS_ExpLicencia.ExpLicenciaDataTable
        Dim fileName As String = String.Format("ExpLic{0}.pdf", DateTime.Now.ToString("yyMMddHHmmss"))
        Dim fileNamePath As String = String.Format("{0}{1}", Server.MapPath(ResolveUrl("~/Descarga/")), fileName)

        For i As Integer = 0 To dtTable.Columns.Count - 1
            If Not strSQL.Equals(String.Empty) Then strSQL = String.Concat(strSQL, ",")
            strSQL = String.Concat(strSQL, dtTable.Columns.Item(i).ColumnName)
        Next
        Try
            strSQL = String.Format("SELECT {0} FROM vis_rpt_ExpLicencia WHERE licencia_id ={1}", strSQL, licencia_id)
            drRead = ExecuteReaderSQL(strSQL)
            dtTable.Load(drRead)

            drRead = ExecuteReaderSQL("SELECT TOP 1 int_mun_trib_nombre FROM dbo.CONFIGURACION")
            If drRead.Read() Then strIntendenteNombre = drRead("int_mun_trib_nombre")

            If EsTemporal Then
                rpt.Load(Server.MapPath(ResolveUrl("~/pagina/reportes_crystal/rptExpLicenciaTemporal.rpt")))
            Else
                rpt.Load(Server.MapPath(ResolveUrl("~/pagina/reportes_crystal/rptExpLicencia.rpt")))
            End If

            rpt.SetDataSource(dtTable)
            rpt.SetParameterValue("Intendente", strIntendenteNombre)
            rpt.ExportToDisk(ExportFormatType.PortableDocFormat, fileNamePath)
            rpt.Close()
            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ExpLicencia", String.Format("window.open('{0}?file={1}','ExpLicencia','status=0,toolbar=0,location=0,menubar=0,resizable=1');", ResolveUrl("~/pagina/reporte/frmShowPDF.aspx"), fileName), True)

        Catch ex As Exception
            ScriptManager.RegisterClientScriptBlock(Me, GetType(Page), "ExpLicencia", "alert('Error mostrando la licencia, revise los parametros de configuracion');", True)
        End Try
    End Sub

    Protected Sub BtnVolver_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect(ResolveUrl("~/pagina/operacion/frmPersonasConsulta.aspx?CargarFiltros=1"))
    End Sub

    Protected Sub BtnNuevo_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect(ResolveUrl("~/pagina/operacion/Licores/frmDatosLicencia.aspx?ID=" & lngContribuyenteId.ToString))
    End Sub

    Protected Sub BtnBuscar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        ActualizarGrid()
    End Sub

    Private Sub ActualizarGrid()
        Dim Query As String = String.Empty

        Query = "SELECT LICENCIA.licencia_id, LICENCIA.clasificacion_licencia_id, LICENCIA.tipo_licencia_id, " & _
                " LICENCIA.num_licencia, LICENCIA.num_control, LICENCIA.fecha_inicio_licencia, CLASIFICACION_LICENCIA.esdistribuidor, TIPO_LICENCIA.estemporal, " & _
                " TIPO_LICENCIA.descripcion_tipo_licencia, CLASIFICACION_LICENCIA.descripcion_clasificacion_licencia " & _
                " FROM LICENCIA INNER JOIN TIPO_LICENCIA ON LICENCIA.tipo_licencia_id = TIPO_LICENCIA.tipo_licencia_id " & _
                " INNER JOIN CLASIFICACION_LICENCIA ON LICENCIA.clasificacion_licencia_id = CLASIFICACION_LICENCIA.clasificacion_licencia_id " & _
                " WHERE(contribuyente_id = " & lngContribuyenteId.ToString() & ")"

        If ddlNumLicencia.SelectedIndex > 0 Then
            Query += " AND LICENCIA.num_licencia = " & ddlNumLicencia.SelectedItem.ToString()
        End If

        If ddlTipoLicencia.SelectedIndex > 0 Then
            Query += " AND LICENCIA.tipo_licencia_id = " & ddlTipoLicencia.SelectedValue
        End If

        If ddlClasificacionLicencia.SelectedIndex > 0 Then
            Query += " AND CLASIFICACION_LICENCIA.clasificacion_licencia_id = " & ddlClasificacionLicencia.SelectedValue
        End If

        SqlDSLicencias.SelectCommand = Query & " ORDER BY num_licencia DESC "
        SqlDSLicencias.Select(New System.Web.UI.DataSourceSelectArguments)
        'SqlDSLicencias.Update()
    End Sub

    Protected Sub Grid_Licencias_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles Grid_Licencias.RowCommand
        Dim row As GridViewRow = Grid_Licencias.Rows(Convert.ToInt32(e.CommandArgument))

        Select Case e.CommandName
            Case "Edit"
                If CBool(row.Cells(ColumnaLicencia.ccEsDistribuidor).Text) Then
                    Response.Redirect(ResolveUrl("~/pagina/operacion/Licores/frmEditAsocVehiculos.aspx?ID=" & Session("contribuyente_id") & "&licenciaId=" & row.Cells(ColumnaLicencia.ccLicenciaId).Text) & "&transac=1")
                Else
                    Response.Redirect(ResolveUrl("~/pagina/operacion/Licores/frmEditDatosLicencia.aspx?ID=" & Session("contribuyente_id") & "&licenciaId=" & row.Cells(ColumnaLicencia.ccLicenciaId).Text) & "&transac=1")
                End If
            Case "Print"
                MostrarLicencia(row.Cells(ColumnaLicencia.ccLicenciaId).Text, row.Cells(ColumnaLicencia.ccEsTemporal).Text)
        End Select
    End Sub

    Protected Sub Grid_Licencias_RowCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles Grid_Licencias.RowCreated
        If e.Row.Cells.Count > 2 Then
            e.Row.Cells(ColumnaLicencia.ccLicenciaId).Visible = False
            e.Row.Cells(ColumnaLicencia.ccEsDistribuidor).Visible = False '<--- Modificado de 7 a 8 porque se agrego una columna nueva (Javier Hernandez)
            e.Row.Cells(ColumnaLicencia.ccEsTemporal).Visible = False
        End If
    End Sub


End Class
