﻿'----------------------------------------------------------------------------------------------
'-- ORIGINAL: (DESCONOCIDO)
'-- MODIFICADO: JAVIER HERNANDEZ 
'-- OBJETIVO DE LA MODIFICACION: Agrupar las acciones de los diferentes modulos en controles 
'--                              de usuarios para las licencias de licores.
'-- FECHA MODIFICACION: FEBRERO 2011
'---------------------------------------------------------------------------------------------
Imports modModelContribuyente
Imports modModelLicencia
Imports System.Data.SqlClient
Imports System.Data
Imports system.Globalization

Partial Class pagina_operacion_Licores_frmDatosLicencias
    Inherits System.Web.UI.UserControl

#Region "--------------- Constantes de Mensajes ---------------"
    Const sEtiqueta As String = "Número de Control para la Renovacion del periodo."
    Const sMensajeIngresaControl As String = "Por favor, ingrese el número de control para todas las renovaciones."
    Const sMensajeLicenciaLimite As String = "La licencia alcanzo el maximo de {0} ({1}) renovaciones. Presione el boton de [Anular] para expirar la licencia."
    Const sMensajeNoRenovacion As String = "No se puede seleccionar motivos de renovaciones sin previas licencias permanentes."
#End Region

#Region "-------------- Declaracion de Variables --------------"

    Private lngTipoLicenciaPermanente As Long = 0
    Private lngMotivoLicenciaInstalacion As Long = 0
    Private lngEstadoLicenciaElaboracion As Long = 0
    Private lngEstadoLicenciaAnula As Long = 0
    Private Const lngMotivoLicenciaSeniat As Long = 2 '--- Colocar en Configuracion
    Private Const lngMotivoLicenciaSamat As Long = 3 '--- Colocar en Configuracion

    '---- Uso para las renovaciones -------------------
    Private Const lngMesesVigencia As Integer = 12  '--- Colocar en Configuracion
    Private Const lngMaxRenovacion As Integer = 3 '--- Colocar en Configuracion, si excede los 3 debe cambiar el diseño y agregar mas controles con el mismo nombre seguido de numero
    '--------------------------------------------------

    Private dtTipoLicenciaTemporal As DataTable
    Private dtClasificacionLicenciaDistrib As DataTable
    Private dtMotivoAutorizacion As DataTable
    Private dr As SqlDataReader
    Private Structure Periodo
        ReadOnly dFechaInicio As Date
        ReadOnly dFechaFin As Date
        ReadOnly sNumeroControl As String
        ReadOnly iRenovacion As Integer

        Public Sub New(ByVal FechaInicio As Date, ByVal FechaFin As Date, ByVal NumeroControl As String, ByVal Renovacion As Integer)
            dFechaInicio = FechaInicio
            dFechaFin = FechaFin
            sNumeroControl = NumeroControl
            iRenovacion = Renovacion
        End Sub
    End Structure

#End Region

    ''' <summary>
    '''  Registra una alerta a la pagina
    ''' </summary>
    ''' <param name="msg">Mensaje a enviar</param>
    Private Sub RegisterMessage(ByVal msg As String)
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "msg", String.Format("alert('{0}');", msg), True)
    End Sub

    ''' <summary>
    ''' Registra los Eventos de las fechas
    ''' </summary>
    Private Sub RegisterEvents()
        ScriptManager.RegisterStartupScript(Me, Me.GetType(), "IBScriptEventFecha", ViewState("EventFecha"), True)
    End Sub

    Private Sub CargarTipoLicenciaTemporal()
        Dim dv As DataView
        dv = SqlDSTipoLicencia.Select(DataSourceSelectArguments.Empty)
        dtTipoLicenciaTemporal = dv.ToTable("TipoLicenciaTemporal")
        Session("TipoLicenciaTemporal") = dtTipoLicenciaTemporal
    End Sub

    Private Sub CargarClasificacionLicenciaDistribuidor()
        Dim dv As DataView
        dv = SqlDSClasificacionLicencia.Select(DataSourceSelectArguments.Empty)
        dtClasificacionLicenciaDistrib = dv.ToTable("ClasificacionLicenciaDistrib")
        Session("ClasificacionLicenciaDistrib") = dtClasificacionLicenciaDistrib
    End Sub

    Private Sub CargarListaKioskos()
        ddpNumeroKiosko.Items.Clear()
        For i As Integer = 0 To 99
            ddpNumeroKiosko.Items.Add(i.ToString)
        Next
    End Sub
    ''' <summary>
    ''' Identifica si el tipo de licencia es temporal
    ''' </summary>
    ''' <param name="tipo_licencia_id"></param>
    Private Function EsTipoLicenciaTemporal(ByVal tipo_licencia_id As Long) As Boolean
        Dim foundRow() As DataRow
        dtTipoLicenciaTemporal = Session("TipoLicenciaTemporal")
        foundRow = dtTipoLicenciaTemporal.Select(String.Format("tipo_licencia_id = {0}", tipo_licencia_id.ToString()))

        If foundRow.Length > 0 Then _
            Return Boolean.Parse(foundRow(0)("estemporal"))

        Return False
    End Function
    ''' <summary>
    ''' Identifica si la clasificacion de la licencia es distribuidor
    ''' </summary>
    ''' <param name="clasificacion_licencia_id"></param>
    Private Function EsClasificacionLicenciaDistrib(ByVal clasificacion_licencia_id As Long) As Boolean
        Dim foundRow() As DataRow
        dtTipoLicenciaTemporal = Session("ClasificacionLicenciaDistrib")
        foundRow = dtTipoLicenciaTemporal.Select(String.Format("clasificacion_licencia_id = {0}", clasificacion_licencia_id.ToString()))

        If foundRow.Length > 0 Then _
            Return Boolean.Parse(foundRow(0)("esdistribuidor"))

        Return False
    End Function
    ''' <summary>
    ''' Identifica si el motivo permite renovacion
    ''' </summary>
    ''' <param name="motivo_autorizacion_id"></param>
    Private Function EsMotivoRenovacion(ByVal motivo_autorizacion_id As Long) As Boolean
        Dim foundRow() As DataRow

        dtMotivoAutorizacion = Session("MotivoAutorizacionRenovacion")

        If Not IsNothing(dtMotivoAutorizacion) Then
            foundRow = dtMotivoAutorizacion.Select(String.Format("motivo_autorizacion_id = {0}", motivo_autorizacion_id.ToString))
            If foundRow.Length > 0 Then _
                Return Boolean.Parse(foundRow(0)("motivo_accion") = 1)
        End If

        Return False
    End Function

    ''' <summary>
    ''' Carga los datos de configuracion para las licencias
    ''' </summary>
    ''' <remarks>De uso local</remarks>
    Private Sub CargarConfiguracionLicencia()
        Dim sql As String = vbNullString
        Dim cnn As SqlConnection = Nothing
        Dim cmd As SqlCommand = Nothing

        cnn = modBaseDatos.obtenerConexion
        cnn.Open()
        sql = "SELECT TOP 1 tipo_licencia_id_permanente, motivo_licencia_id_instalacion, estado_licencia_id_elaboracion, estado_licencia_id_anula FROM configuracion"
        cmd = New SqlCommand(sql, cnn)
        dr = cmd.ExecuteReader(CommandBehavior.CloseConnection)
        If dr.Read() Then
            lngTipoLicenciaPermanente = CInt(dr("tipo_licencia_id_permanente").ToString)
            lngMotivoLicenciaInstalacion = CInt(dr("motivo_licencia_id_instalacion").ToString) '<--- Uso erroneo, existen motivos por tipos de licencia
            lngEstadoLicenciaElaboracion = CInt(dr("estado_licencia_id_elaboracion").ToString)
            lngEstadoLicenciaAnula = CInt(dr("estado_licencia_id_anula").ToString)
        End If

    End Sub
    ''' <summary>
    ''' Actualiza la lista de Motivos segun el tipo de Licencia
    ''' </summary>
    ''' <param name="tipo_licencia_id">Licencia Id</param>
    ''' <remarks></remarks>
    Private Sub UpdateMotivoAutorizacion(ByVal tipo_licencia_id As Long)
        Dim dv As DataView

        With ddlMotivoAutorizacion
            .Items.Clear()
            .Items.Add(New ListItem("SELECCIONE", 0))
            SqlDSMotivoAutorizacion.SelectParameters("tipo_licencia_id").DefaultValue = tipo_licencia_id.ToString()

            '--- Carga en memoria ----
            dv = SqlDSMotivoAutorizacion.Select(DataSourceSelectArguments.Empty)
            dtMotivoAutorizacion = dv.ToTable("MotivoAutorizacionRenovacion")
            Session("MotivoAutorizacionRenovacion") = dtMotivoAutorizacion
            '--- fin carga -----------
        End With
    End Sub

    ''' <summary>
    ''' Activa o desactiva los textos segun el tipo de licencia
    ''' </summary>
    ''' <remarks>De uso local</remarks>
    Private Sub CheckControlesLicenciaTemporal()

        Dim scriptEventFecha As String = "iniciarCalendario(document.getElementById('" & txtFechaSolicitud.ClientID & "') ,'" & imgFechaSolicitud.ID & "','%d/%m/%Y',false,{x:520,y:820});"
        scriptEventFecha += "iniciarCalendario(document.getElementById('" & txtFechaInicio.ClientID & "') ,'" & imgFechaInicio.ID & "','%d/%m/%Y',false,{x:520,y:870});"

        imgFechaInicio.Visible = True
        imgFechaEmision.Visible = True
        imgFechaVencimiento.Visible = True

        If EsTipoLicenciaTemporal(ddlTipoLicencia.SelectedValue) Then
            '---- Activa los controles personalizados de licencia Temporal
            txtEvento.Visible = True
            lblEvento.Visible = True
            txtUbicacionEv.Visible = True
            lblUbicacionEv.Visible = True
            txtKiosko.Visible = True
            lblKiosko.Visible = True
            ddpNumeroKiosko.Visible = True

            scriptEventFecha += "iniciarCalendario(document.getElementById('" & txtFechaEmision.ClientID & "') ,'" & imgFechaEmision.ID & "','%d/%m/%Y',false,{x:1040,y:820});"
            scriptEventFecha += "iniciarCalendario(document.getElementById('" & txtFechaVencimiento.ClientID & "') ,'" & imgFechaVencimiento.ID & "','%d/%m/%Y',false,{x:1040,y:870});"

            '---- Aplica el formato a los controles de fecha
            txtFechaInicio.Attributes.Remove("onchange")
            txtFechaInicio.AutoPostBack = False
            txtFechaVencimiento.Text = String.Empty
            txtFechaEmision.Text = String.Empty
            txtFechaVencimiento.ForeColor = Nothing 'System.Drawing.ColorTranslator.FromHtml(txtFechaVencimiento.Style("forecolor"))
            txtFechaEmision.ForeColor = txtFechaVencimiento.ForeColor
            txtFechaInicio.ForeColor = txtFechaVencimiento.ForeColor

        Else
            '---- Inactiva los controles personalizados de licencia Temporal
            txtEvento.Visible = False
            lblEvento.Visible = False
            txtUbicacionEv.Visible = False
            lblUbicacionEv.Visible = False
            txtKiosko.Visible = False
            lblKiosko.Visible = False
            ddpNumeroKiosko.Visible = False

            '--- Oculta los botones de seleccion de fecha
            imgFechaEmision.Visible = False
            imgFechaVencimiento.Visible = False

            '---- Agrega el script que permite realizar el postback para calcular el a~o
            txtFechaInicio.Attributes.Add("onchange", String.Format("{0}.focus();", txtFechaVencimiento.ClientID))
            txtFechaInicio.AutoPostBack = True
            txtFechaInicio_TextChanged(Nothing, Nothing)
            txtFechaInicio.ForeColor = Nothing
            txtFechaVencimiento.ForeColor = Drawing.Color.Gray
            txtFechaEmision.ForeColor = Drawing.Color.Gray
        End If

        ViewState("EventFecha") = scriptEventFecha
    End Sub

    Private Sub HideRenovacion()
        lblMensaje.Text = String.Empty
        BtnExpirarLicencia.Visible = False
        lblNumeroControl1.Visible = False
        txtNumeroControl1.Visible = False
        lblNumeroControl2.Visible = False
        txtNumeroControl2.Visible = False
    End Sub

    Private Sub CheckRenovacion()
        Dim lngContribuyenteId As Long = Session("contribuyente_id")
        Dim dr As SqlDataReader
        Dim intRenovaciones As Integer
        Dim intRestoRenovaciones As Integer
        Dim iRenovacion As Integer
        Dim dblCantRenovaciones As Double
        Dim dRenovaciones As Date
        Dim dFechaPivot As Date

        HideRenovacion()

        '---- Vacia las cantidades de Renovaciones ---
        Session("CantRenovaciones") = -1
        If ddlTipoLicencia.SelectedValue > 0 AndAlso ddlClasificacionLicencia.SelectedValue > 0 Then

            If EsMotivoRenovacion(ddlMotivoAutorizacion.SelectedValue) Then
                If getRenovaciones(lngContribuyenteId, _
                                   ddlTipoLicencia.SelectedValue, ddlClasificacionLicencia.SelectedValue, _
                                   lngEstadoLicenciaAnula, intRenovaciones, dRenovaciones) Then

                    '--- Calcula las renovaciones restantes
                    intRestoRenovaciones = lngMaxRenovacion - intRenovaciones

                    If dRenovaciones > Today Then '---- Licencia Vigente
                        RegisterMessage("La licencia del Tipo y Clasificacion esta VIGENTE, no se puede continuar el proceso de renovacion.")
                        ddlMotivoAutorizacion.SelectedValue = 0

                    ElseIf dRenovaciones.AddMonths(intRestoRenovaciones * lngMesesVigencia) > Today Then '--- Solicita Numero de Control
                        dblCantRenovaciones = DateDiff(DateInterval.Month, dRenovaciones, Today) / lngMesesVigencia
                        intRestoRenovaciones = Int(dblCantRenovaciones)
                        If dblCantRenovaciones - intRestoRenovaciones > 0.0 Then intRestoRenovaciones = intRestoRenovaciones + 1

                        '--- Muestra los controles con los periodos
                        dFechaPivot = dRenovaciones
                        For iRenovacion = 1 To (intRestoRenovaciones - 1)
                            Dim lRenovacion As Label = Me.FindControl("lblNumeroControl" & iRenovacion)
                            Dim tRenovacion As TextBox = Me.FindControl("txtNumeroControl" & iRenovacion)

                            lRenovacion.Text = String.Concat(sEtiqueta, dFechaPivot.ToString("dd/MM/yyyy"), " al ", dFechaPivot.AddMonths(lngMesesVigencia).ToString("dd/MM/yyyy"))
                            lRenovacion.Visible = True
                            tRenovacion.Visible = True
                            dFechaPivot = dFechaPivot.AddMonths(lngMesesVigencia)
                        Next
                        If iRenovacion > 1 Then lblMensaje.Text = sMensajeIngresaControl

                        txtFechaInicio.Text = dFechaPivot.ToString("dd/MM/yyyy")
                        txtFechaInicio.ForeColor = Drawing.Color.Gray
                        imgFechaInicio.Visible = False
                        txtAnn.Text = dFechaPivot.Year
                        txtAnn.Enabled = False
                        Call txtFechaInicio_TextChanged(Nothing, Nothing)

                        '----- Carga los datos de la licencia principal -----
                        dr = getLastLicenciaVigente(lngContribuyenteId, _
                                                    ddlTipoLicencia.SelectedValue, _
                                                    ddlClasificacionLicencia.SelectedValue, _
                                                    lngEstadoLicenciaAnula)
                        If dr.Read() Then
                            ddlTipoLicencia.SelectedValue = dr("tipo_licencia_id")
                            ddlClasificacionLicencia.SelectedValue = dr("clasificacion_licencia_id")
                            ddlEstadoLicencia.SelectedValue = lngEstadoLicenciaElaboracion
                            ddlHorario.SelectedValue = dr("horario_licencia_id")
                            ddlHorario.Enabled = False
                            lblRenovacion.Text = dr("renovacion").ToString
                            txtNumeroSolicitud.Text = dr("num_solicitud").ToString
                            txtNumeroSolicitud.Enabled = False
                            txtFechaSolicitud.Text = Date.Parse(dr("fecha_solicitud")).ToString("dd/MM/yyyy")
                            txtFechaSolicitud.ForeColor = Drawing.Color.Gray
                            imgFechaSolicitud.Visible = False
                            txtNumeroLicencia.Text = dr("num_licencia").ToString
                            txtNumeroLicencia.Enabled = False
                            txtNumeroExpediente.Text = dr("num_expediente").ToString
                            txtNumeroExpediente.Enabled = False
                        End If
                        '---- fin ---- Carga 

                        ViewState("EventFecha") = String.Empty
                        '---- Almacena datos para la renovacion -----
                        Session("FechaRenovacion") = dRenovaciones
                        Session("CantRenovaciones") = intRestoRenovaciones
                        Session("Renovaciones") = intRenovaciones

                    Else '--- Las renovaciones exceden el limite
                        lblMensaje.Text = String.Format(sMensajeLicenciaLimite, Num2Text(lngMaxRenovacion), lngMaxRenovacion)
                        BtnExpirarLicencia.Visible = True
                        '----- Carga los datos de la licencia principal -----
                        dr = getLastLicenciaVigente(lngContribuyenteId, _
                                                    ddlTipoLicencia.SelectedValue, _
                                                    ddlClasificacionLicencia.SelectedValue, _
                                                    lngEstadoLicenciaAnula)
                        If dr.Read() Then
                            ddlTipoLicencia.SelectedValue = dr("tipo_licencia_id")
                            ddlClasificacionLicencia.SelectedValue = dr("clasificacion_licencia_id")
                            ddlHorario.SelectedValue = dr("horario_licencia_id")
                            ddlHorario.Enabled = False
                            lblRenovacion.Text = dr("renovacion").ToString
                            txtNumeroSolicitud.Text = dr("num_solicitud").ToString
                            txtNumeroSolicitud.Enabled = False
                            txtNumeroLicencia.Text = dr("num_licencia").ToString
                            txtNumeroLicencia.Enabled = False
                            txtNumeroExpediente.Text = dr("num_expediente").ToString
                            txtNumeroExpediente.Enabled = False
                        End If
                        '---- fin ---- Carga 
                    End If
                Else '--- El motivo es renovacion pero no existen licencias ----
                    RegisterMessage(sMensajeNoRenovacion)
                    ddlMotivoAutorizacion.SelectedValue = 0
                End If
            Else
                txtAnn.Text = String.Empty
                txtAnn.Enabled = True
                txtFechaSolicitud.Text = String.Empty
                txtFechaSolicitud.ForeColor = Nothing
                imgFechaSolicitud.Visible = True
                txtNumeroSolicitud.Text = String.Empty
                txtNumeroSolicitud.Enabled = True
                txtNumeroExpediente.Text = String.Empty
                txtNumeroExpediente.Enabled = True
                txtNumeroLicencia.Text = "(AUTOGENERADO)"
                ddlHorario.Enabled = True
            End If

        End If
    End Sub


    Public Function CheckCampos() As Boolean
        Dim strMsg As String = ""
        Dim EsTemporal As Boolean

        EsTemporal = EsTipoLicenciaTemporal(ddlTipoLicencia.SelectedValue)

        If ddlTipoLicencia.SelectedIndex = 0 Then
            strMsg = " Debe Seleccionar un Tipo de Licencia"
        ElseIf ddlMotivoAutorizacion.SelectedIndex = 0 Then
            strMsg = " Debe Seleccionar un Motivo de Autorización"
        ElseIf ddlClasificacionLicencia.SelectedIndex = 0 Then
            strMsg = " Debe Seleccionar una Clasificación para la Licencia"
        ElseIf ddlEstadoLicencia.SelectedIndex = 0 Then
            strMsg = " Debe Seleccionar un Estado para la Licencia"
        ElseIf ddlHorario.SelectedIndex = 0 Then
            strMsg = " Debe Seleccionar un Horario para la Licencia"
        ElseIf txtNumeroControl.Text.Trim = "" And Not EsTemporal Then
            strMsg = " Debe especificar un Número de Control"
        ElseIf txtNumeroSolicitud.Text.Trim = "" Then
            strMsg = " Debe especificar un Número de Solicitud"
        ElseIf txtFechaSolicitud.Text.Trim = "" Then
            strMsg = " Debe especificar una Fecha de Solicitud"
        ElseIf txtFechaEmision.Text.Trim = "" Then
            strMsg = " Debe especificar una Fecha de Emisión"
        ElseIf txtFechaInicio.Text.Trim = "" Then
            strMsg = " Debe especificar una Fecha de Inicio de la Licencia"
        ElseIf txtFechaVencimiento.Text.Trim = "" Then
            strMsg = " Debe especificar una Fecha de Vencimiento de la Licencia"
        ElseIf txtAnn.Text.Trim = "" Then
            strMsg = " Debe especificar un Año de Tributo"
        ElseIf txtNumeroExpediente.Text.Trim = "" And Not EsTemporal Then
            strMsg = " Debe especificar un Número de Expediente"
        ElseIf Date.Parse(txtFechaSolicitud.Text, CultureInfo.GetCultureInfo("es-ES")) > Date.Parse(txtFechaEmision.Text, CultureInfo.GetCultureInfo("es-ES")) Then
            strMsg = " La Fecha de Solicitud no puede ser mayor a la Fecha de Emisión"
        ElseIf Date.Parse(txtFechaEmision.Text, CultureInfo.GetCultureInfo("es-ES")) > Date.Parse(txtFechaInicio.Text, CultureInfo.GetCultureInfo("es-ES")) Then
            strMsg = " La Fecha de Emisión no puede ser mayor a la Fecha de Inicio"
        ElseIf Date.Parse(txtFechaInicio.Text, CultureInfo.GetCultureInfo("es-ES")) > Date.Parse(Me.txtFechaVencimiento.Text, CultureInfo.GetCultureInfo("es-ES")) Then
            strMsg = " La Fecha de Inicio no puede ser mayor a la Fecha de Vencimiento"
        ElseIf txtNumeroControl1.Visible AndAlso txtNumeroControl1.Text.Trim = "" Then
            strMsg = String.Concat(" Debe especificar un Número de Control para la renovacion [", lblNumeroControl1.Text, "]")
        ElseIf txtNumeroControl2.Visible AndAlso txtNumeroControl2.Text.Trim = "" Then
            strMsg = String.Concat(" Debe especificar un Número de Control para la renovacion [", lblNumeroControl2.Text, "]")
        End If

        '--- Validacion para licencia tipo temporal ---
        If EsTemporal Then
            If txtEvento.Text.Equals(String.Empty) Or txtUbicacionEv.Text.Equals(String.Empty) Or txtKiosko.Text.Equals(String.Empty) Then
                strMsg = " Debe colocar el Nombre del Evento, Cantidad de Puntos y Ubicacion del Evento para el tipo de licencia seleccionado"
            End If
        End If

        If strMsg.Equals(String.Empty) Then
            Return True
        Else
            RegisterMessage(strMsg)
            Return False
        End If

    End Function

    Public Function Num2Text(ByVal value As Double) As String
        Select Case value
            Case 0 : Num2Text = "CERO"
            Case 1 : Num2Text = "UN"
            Case 2 : Num2Text = "DOS"
            Case 3 : Num2Text = "TRES"
            Case 4 : Num2Text = "CUATRO"
            Case 5 : Num2Text = "CINCO"
            Case 6 : Num2Text = "SEIS"
            Case 7 : Num2Text = "SIETE"
            Case 8 : Num2Text = "OCHO"
            Case 9 : Num2Text = "NUEVE"
            Case 10 : Num2Text = "DIEZ"
            Case 11 : Num2Text = "ONCE"
            Case 12 : Num2Text = "DOCE"
            Case 13 : Num2Text = "TRECE"
            Case 14 : Num2Text = "CATORCE"
            Case 15 : Num2Text = "QUINCE"
            Case Is < 20 : Num2Text = "DIECI" & Num2Text(value - 10)
            Case 20 : Num2Text = "VEINTE"
            Case Is < 30 : Num2Text = "VEINTI" & Num2Text(value - 20)
            Case 30 : Num2Text = "TREINTA"
            Case 40 : Num2Text = "CUARENTA"
            Case 50 : Num2Text = "CINCUENTA"
            Case 60 : Num2Text = "SESENTA"
            Case 70 : Num2Text = "SETENTA"
            Case 80 : Num2Text = "OCHENTA"
            Case 90 : Num2Text = "NOVENTA"
            Case Is < 100 : Num2Text = Num2Text(Int(value \ 10) * 10) & " Y " & Num2Text(value Mod 10)
            Case 100 : Num2Text = "CIEN"
            Case Is < 200 : Num2Text = "CIENTO " & Num2Text(value - 100)
            Case 200, 300, 400, 600, 800 : Num2Text = Num2Text(Int(value \ 100)) & "CIENTOS"
            Case 500 : Num2Text = "QUINIENTOS"
            Case 700 : Num2Text = "SETECIENTOS"
            Case 900 : Num2Text = "NOVECIENTOS"
            Case Is < 1000 : Num2Text = Num2Text(Int(value \ 100) * 100) & " " & Num2Text(value Mod 100)
            Case 1000 : Num2Text = "MIL"
            Case Is < 2000 : Num2Text = "MIL " & Num2Text(value Mod 1000)
            Case Is < 1000000 : Num2Text = Num2Text(Int(value \ 1000)) & " MIL"
                If value Mod 1000 Then Num2Text = Num2Text & " " & Num2Text(value Mod 1000)
            Case 1000000 : Num2Text = "UN MILLON"
            Case Is < 2000000 : Num2Text = "UN MILLON " & Num2Text(value Mod 1000000)
            Case Is < 1000000000000.0# : Num2Text = Num2Text(Int(value / 1000000)) & " MILLONES "
                If (value - Int(value / 1000000) * 1000000) Then Num2Text = Num2Text & " " & Num2Text(value - Int(value / 1000000) * 1000000)
            Case 1000000000000.0# : Num2Text = "UN BILLON"
            Case Is < 2000000000000.0# : Num2Text = "UN BILLON " & Num2Text(value - Int(value / 1000000000000.0#) * 1000000000000.0#)
            Case Else : Num2Text = Num2Text(Int(value / 1000000000000.0#)) & " BILLONES"
                If (value - Int(value / 1000000000000.0#) * 1000000000000.0#) Then Num2Text = Num2Text & " " & Num2Text(value - Int(value / 1000000000000.0#) * 1000000000000.0#)
        End Select
    End Function

    Public Sub CargaDatosLicencia(ByVal lngLicenciaId As Long)
        Dim pLicenciaID As String = vbNull

        pLicenciaID = lngLicenciaId.ToString()
        If IsDBNull(pLicenciaID) = False Then

            dr = getDatosLicencia(pLicenciaID)
            If dr.Read = True Then
                Session("clasificacionLicenciaIdOriginal") = dr("clasificacion_licencia_id")
                ddlTipoLicencia.SelectedValue = dr("tipo_licencia_id")
                UpdateMotivoAutorizacion(dr("tipo_licencia_id")) '---- Actualiza el query de la autorizacion
                ddlMotivoAutorizacion.SelectedValue = dr("motivo_licencia_id")
                ddlMotivoAutorizacion.DataBind()

                If EsMotivoRenovacion(dr("motivo_licencia_id")) AndAlso ddlMotivoAutorizacion.SelectedItem.Text.IndexOf("SENIAT") > 0 Then
                    lblLicenciaSENIAT.Visible = True
                    txtLicenciaSENIAT.Visible = True
                    txtLicenciaSENIAT.Enabled = False
                    lblFechaVencimiento.Text = "Fecha de Vencimiento SENIAT"
                Else
                    lblLicenciaSENIAT.Visible = False
                    txtLicenciaSENIAT.Visible = False
                    txtLicenciaSENIAT.Enabled = True
                    lblFechaVencimiento.Text = "Fecha de Vencimiento"
                End If
                ddlClasificacionLicencia.SelectedValue = dr("clasificacion_licencia_id")
                ddlEstadoLicencia.SelectedValue = dr("estado_licencia_id")
                ddlHorario.SelectedValue = dr("horario_licencia_id")

                txtNumeroControl.Text = dr("num_control")
                lblRenovacion.Text = dr("renovacion").ToString

                txtNumeroSolicitud.Text = dr("num_solicitud").ToString
                txtNumeroSolicitud.Enabled = String.IsNullOrEmpty(txtNumeroSolicitud.Text)

                txtFechaSolicitud.Text = Date.Parse(dr("fecha_solicitud")).ToString("dd/MM/yyyy")
                txtFechaEmision.Text = Date.Parse(dr("fecha_expendio")).ToString("dd/MM/yyyy")
                txtFechaInicio.Text = Date.Parse(dr("fecha_inicio_licencia")).ToString("dd/MM/yyyy")
                txtFechaVencimiento.Text = Date.Parse(dr("fecha_vencimiento_licencia")).ToString("dd/MM/yyyy")
                txtAnn.Text = dr("ano").ToString
                txtNumeroLicencia.Text = dr("num_licencia").ToString

                txtNumeroExpediente.Text = dr.GetValue(19).ToString
                txtNumeroExpediente.Enabled = String.IsNullOrEmpty(txtNumeroExpediente.Text)

                txtLicenciaSENIAT.Text = dr("num_licencia_SENIAT").ToString
                txtEvento.Text = dr("evento").ToString
                If Not IsDBNull(dr("kiosko_numero")) Then ddpNumeroKiosko.SelectedValue = dr("kiosko_numero")
                txtKiosko.Text = dr("kiosko_evento").ToString
                txtUbicacionEv.Text = dr("dir_evento").ToString
            End If
        End If
        DataBind()

        CheckControlesLicenciaTemporal()
    End Sub

    Public Sub AsignaDatosLicencia(ByVal MotivoAutorizacion As String, _
        ByVal EstadoLicencia As String, ByVal TipoLicencia As String, _
        ByVal Horario As String, ByVal ClasificacionLicencia As String, _
        ByVal NumeroControl As String, _
        ByVal NumeroSolicitud As String, ByVal FechaSolicitud As String, _
        ByVal FechaEmision As String, ByVal FechaInicio As String, _
        ByVal FechaVencimiento As String, ByVal Ann As String, _
        ByVal LicenciaSENIAT As String, ByVal NumeroExpediente As String, _
        ByVal sEvento As String, ByVal iKiosko As Integer, ByVal sKiosko As String, ByVal sUbicacionEv As String, _
        ByVal NumeroLicencia As String)

        UpdateMotivoAutorizacion(TipoLicencia) '---- Actualiza el query de la autorizacion

        ddlMotivoAutorizacion.SelectedValue = MotivoAutorizacion
        ddlEstadoLicencia.SelectedValue = EstadoLicencia
        ddlTipoLicencia.SelectedValue = TipoLicencia
        ddlHorario.SelectedValue = Horario
        ddlClasificacionLicencia.SelectedValue = ClasificacionLicencia

        txtNumeroControl.Text = NumeroControl
        txtNumeroSolicitud.Text = NumeroSolicitud
        txtNumeroSolicitud.Enabled = String.IsNullOrEmpty(NumeroSolicitud)
        txtFechaSolicitud.Text = FechaSolicitud
        txtFechaEmision.Text = FechaEmision
        txtFechaInicio.Text = FechaInicio
        txtFechaVencimiento.Text = FechaVencimiento
        txtAnn.Text = Ann
        txtLicenciaSENIAT.Text = LicenciaSENIAT
        txtNumeroExpediente.Text = NumeroExpediente
        txtNumeroExpediente.Enabled = String.IsNullOrEmpty(NumeroExpediente)
        txtNumeroLicencia.Text = IIf(String.IsNullOrEmpty(NumeroLicencia), "AUTOGENERADO", NumeroLicencia)
        txtEvento.Text = sEvento
        If Not IsNothing(ddpNumeroKiosko.Items.FindByValue(iKiosko)) Then ddpNumeroKiosko.SelectedValue = iKiosko
        txtKiosko.Text = sKiosko
        txtUbicacionEv.Text = sUbicacionEv
        DataBind()

        CheckControlesLicenciaTemporal()
    End Sub

    Public Sub LimpiarControles()
        ddlTipoLicencia.ClearSelection()
        ddlMotivoAutorizacion.ClearSelection()
        ddlClasificacionLicencia.ClearSelection()
        ddlEstadoLicencia.ClearSelection()
        ddlHorario.ClearSelection()
        txtNumeroControl.Text = ""
        txtNumeroSolicitud.Text = ""
        txtFechaSolicitud.Text = ""
        txtFechaEmision.Text = ""
        txtFechaVencimiento.Text = ""
        txtAnn.Text = ""
        txtFechaInicio.Text = ""
        txtNumeroLicencia.Text = "(AUTOGENERADO)"
        txtNumeroExpediente.Text = ""

        CheckControlesLicenciaTemporal()
        HideRenovacion()
    End Sub

    Protected Sub ddlMotivoAutorizacion_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMotivoAutorizacion.SelectedIndexChanged
        Dim lngContribuyenteId As Long = Session("contribuyente_id")

        ddlEstadoLicencia.Enabled = True
        Select Case Session("Action")
            Case "frmEditDatosLicencia", "frmEditAsocVehiculos"

                If ddlMotivoAutorizacion.SelectedIndex > 0 AndAlso ddlTipoLicencia.SelectedIndex > 0 Then
                    If ddlTipoLicencia.SelectedValue = lngTipoLicenciaPermanente And _
                       ddlMotivoAutorizacion.SelectedValue = lngMotivoLicenciaInstalacion Then
                        ddlEstadoLicencia.SelectedValue = lngEstadoLicenciaElaboracion
                        ddlEstadoLicencia.Enabled = False
                    End If
                End If

                txtLicenciaSENIAT.Enabled = False '<---- Desactiva la modificacion de la licencia SENIAT

            Case "frmDatosLicencia", "frmAsocVehiculos"

                If ddlMotivoAutorizacion.SelectedIndex > 0 AndAlso ddlTipoLicencia.SelectedIndex > 0 Then
                    If ddlTipoLicencia.SelectedValue = lngTipoLicenciaPermanente And _
                       ddlMotivoAutorizacion.SelectedValue = lngMotivoLicenciaInstalacion Then
                        ddlEstadoLicencia.SelectedValue = lngEstadoLicenciaElaboracion
                        ddlEstadoLicencia.Enabled = False
                    End If
                End If

                txtLicenciaSENIAT.Enabled = True '<---- Activa la modificacion de la licencia SENIAT
        End Select

        '===================  Acciones Comunes ===================================
        If ddlMotivoAutorizacion.SelectedIndex > 0 Then
            If ddlMotivoAutorizacion.SelectedItem.Text.IndexOf("SENIAT") > 0 Then
                lblLicenciaSENIAT.Visible = True
                txtLicenciaSENIAT.Visible = True
                lblFechaVencimiento.Text = "Fecha de Vencimiento SENIAT"
            Else
                lblLicenciaSENIAT.Visible = False
                txtLicenciaSENIAT.Visible = False
                lblFechaVencimiento.Text = "Fecha de Vencimiento"
            End If

        End If

        '=================== Valida los controles - IMPORTANTE EL ORDEN DE EJECUCION
        CheckControlesLicenciaTemporal()
        CheckRenovacion()
    End Sub

    Protected Sub ddlClasificacionLicencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlClasificacionLicencia.SelectedIndexChanged
        Select Case Session("Action")
            Case "frmEditDatosLicencia"
                If EsClasificacionLicenciaDistrib(ddlClasificacionLicencia.SelectedValue) Then
                    Response.Redirect(ResolveUrl("~/pagina/operacion/Licores/frmEditAsocVehiculos.aspx?ID=" & Session("contribuyente_id") & _
                                                 "&tipoLicenciaId=" & ddlTipoLicencia.SelectedValue & _
                                                 "&clasificacionLicenciaId=" & ddlClasificacionLicencia.SelectedValue & _
                                                 "&motivoAutorizacionId=" & ddlMotivoAutorizacion.SelectedValue & _
                                                 "&estadoLicenciaId=" & ddlEstadoLicencia.SelectedValue & _
                                                 "&horarioId=" & ddlHorario.SelectedValue & _
                                                 "&licenciaId=" & Session("licencia_Id") & _
                                                 "&numeroControl=" & txtNumeroControl.Text & _
                                                 "&numeroSolicitud=" & txtNumeroSolicitud.Text & _
                                                 "&fechaSolicitud=" & txtFechaSolicitud.Text & _
                                                 "&fechaEmision=" & txtFechaEmision.Text & _
                                                 "&fechaInicio=" & txtFechaInicio.Text & _
                                                 "&fechaVencimiento=" & txtFechaVencimiento.Text & _
                                                 "&anno=" & txtAnn.Text & _
                                                 "&numeroSeniat=" & txtLicenciaSENIAT.Text & _
                                                 "&numeroExpediente=" & txtNumeroExpediente.Text & _
                                                 "&numeroLicencia=" & txtNumeroLicencia.Text & _
                                                 "&evento=" & txtEvento.Text & _
                                                 "&ikiosko=" & ddpNumeroKiosko.SelectedValue & _
                                                 "&kiosko=" & txtKiosko.Text & _
                                                 "&ubicacionev=" & txtUbicacionEv.Text))
                End If
            Case "frmEditAsocVehiculos"
                If Not EsClasificacionLicenciaDistrib(ddlClasificacionLicencia.SelectedValue) Then
                    Response.Redirect(ResolveUrl("~/pagina/operacion/Licores/frmEditDatosLicencia.aspx?ID=" & Session("contribuyente_id") & _
                                                 "&tipoLicenciaId=" & ddlTipoLicencia.SelectedValue & _
                                                 "&clasificacionLicenciaId=" & ddlClasificacionLicencia.SelectedValue & _
                                                 "&motivoAutorizacionId=" & ddlMotivoAutorizacion.SelectedValue & _
                                                 "&estadoLicenciaId=" & ddlEstadoLicencia.SelectedValue & _
                                                 "&horarioId=" & ddlHorario.SelectedValue & _
                                                 "&licenciaId=" & Session("licencia_Id") & _
                                                 "&numeroControl=" & txtNumeroControl.Text & _
                                                 "&numeroSolicitud=" & txtNumeroSolicitud.Text & _
                                                 "&fechaSolicitud=" & txtFechaSolicitud.Text & _
                                                 "&fechaEmision=" & txtFechaEmision.Text & _
                                                 "&fechaInicio=" & txtFechaInicio.Text & _
                                                 "&fechaVencimiento=" & txtFechaVencimiento.Text & _
                                                 "&anno=" & txtAnn.Text & _
                                                 "&numeroSeniat=" & txtLicenciaSENIAT.Text & _
                                                 "&numeroExpediente=" & txtNumeroExpediente.Text & _
                                                 "&numeroLicencia=" & txtNumeroLicencia.Text & _
                                                 "&evento=" & txtEvento.Text & _
                                                 "&ikiosko=" & ddpNumeroKiosko.SelectedValue & _
                                                 "&kiosko=" & txtKiosko.Text & _
                                                 "&ubicacionev=" & txtUbicacionEv.Text))
                    ' Session("clasificacionLicenciaId")
                End If
            Case "frmDatosLicencia"
                If EsClasificacionLicenciaDistrib(ddlClasificacionLicencia.SelectedValue) Then
                    'Response.Redirect(ResolveUrl("~/pagina/operacion/Licores/frmAsocVehiculos.aspx?ID=" & lngContribuyenteId.ToString & "&tipoLicenciaId=" & Me.ddlTipoLicencia.SelectedValue & "&motivoAutorizacionId=" & Me.ddlMotivoAutorizacion.SelectedValue & "&estadoLicenciaId=" & Me.ddlEstadoLicencia.SelectedValue & "&horarioId=" & Me.ddlHorario.SelectedValue))
                    Response.Redirect(ResolveUrl("~/pagina/operacion/Licores/frmAsocVehiculos.aspx?ID=" & Session("contribuyente_id") & _
                                                 "&tipoLicenciaId=" & ddlTipoLicencia.SelectedValue & _
                                                 "&clasificacionLicenciaId=" & ddlClasificacionLicencia.SelectedValue & _
                                                 "&motivoAutorizacionId=" & ddlMotivoAutorizacion.SelectedValue & _
                                                 "&estadoLicenciaId=" & ddlEstadoLicencia.SelectedValue & _
                                                 "&horarioId=" & ddlHorario.SelectedValue & _
                                                 "&numeroControl=" & txtNumeroControl.Text & _
                                                 "&numeroSolicitud=" & txtNumeroSolicitud.Text & _
                                                 "&fechaSolicitud=" & txtFechaSolicitud.Text & _
                                                 "&fechaEmision=" & txtFechaEmision.Text & _
                                                 "&fechaInicio=" & txtFechaInicio.Text & _
                                                 "&fechaVencimiento=" & txtFechaVencimiento.Text & _
                                                 "&anno=" & txtAnn.Text & _
                                                 "&numeroSeniat=" & txtLicenciaSENIAT.Text & _
                                                 "&numeroExpediente=" & txtNumeroExpediente.Text & _
                                                 "&numeroLicencia=" & txtNumeroLicencia.Text & _
                                                 "&evento=" & txtEvento.Text & _
                                                 "&ikiosko=" & ddpNumeroKiosko.SelectedValue & _
                                                 "&kiosko=" & txtKiosko.Text & _
                                                 "&ubicacionev=" & txtUbicacionEv.Text))
                End If
            Case "frmAsocVehiculos"
                If Not EsClasificacionLicenciaDistrib(ddlClasificacionLicencia.SelectedValue) Then
                    Session.Remove("strIdsVehiculo")
                    Response.Redirect(ResolveUrl("~/pagina/operacion/Licores/frmDatosLicencia.aspx?ID=" & Session("contribuyente_id") & _
                                                 "&tipoLicenciaId=" & ddlTipoLicencia.SelectedValue & _
                                                 "&motivoAutorizacionId=" & ddlMotivoAutorizacion.SelectedValue & _
                                                 "&estadoLicenciaId=" & ddlEstadoLicencia.SelectedValue & _
                                                 "&horarioId=" & ddlHorario.SelectedValue & _
                                                 "&clasificacionLicenciaId=" & ddlClasificacionLicencia.SelectedValue))
                End If
        End Select

        '--- Valida los controles - IMPORTANTE EL ORDEN DE EJECUCION
        CheckControlesLicenciaTemporal()
        CheckRenovacion()
    End Sub

    Protected Sub ddlTipoLicencia_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTipoLicencia.SelectedIndexChanged
        Select Case Session("Action")
            Case "frmEditDatosLicencia"
                '--- Nothing ----
            Case "frmEditAsocVehiculos", "frmDatosLicencia", "frmAsocVehiculos"
                If ddlTipoLicencia.SelectedIndex <> 0 And _
                   ddlMotivoAutorizacion.SelectedIndex <> 0 Then
                    If ddlTipoLicencia.SelectedValue = lngTipoLicenciaPermanente And _
                       ddlMotivoAutorizacion.SelectedValue = lngMotivoLicenciaInstalacion Then
                        ddlEstadoLicencia.SelectedValue = lngEstadoLicenciaElaboracion
                        ddlEstadoLicencia.Enabled = False
                    End If
                End If
        End Select

        '--- Actualiza la lista de motivos de autorizacion solo cuando se seleccione el tipo de licencia 
        UpdateMotivoAutorizacion(ddlTipoLicencia.SelectedValue)

        '--- Valida los controles - IMPORTANTE EL ORDEN DE EJECUCION
        CheckControlesLicenciaTemporal()
        CheckRenovacion()
    End Sub

    ''' <summary>
    ''' Almacena la licencia en la Base de Datos
    ''' </summary>
    ''' <param name="lngContribuyenteId"></param>
    ''' <param name="sUsoContribuyente"></param>
    ''' <param name="sSeniat"></param>
    ''' <param name="sRimContribuyente"></param>
    ''' <param name="sRifContribuyente"></param>
    ''' <param name="sNombreContribuyente"></param>
    ''' <param name="sParroquiaContribuyente"></param>
    ''' <param name="sSectorContribuyente"></param>
    ''' <param name="bRevisarAno"></param>
    ''' <param name="UpdPanel"></param>
    ''' <returns>Verdadero o Falso dependiendo del resultado</returns>
    Public Function InsertaLicencia(ByVal lngLicenciaId As Long, _
                                    ByVal lngContribuyenteId As Long, _
                                    ByVal sUsoContribuyente As String, ByVal sSeniat As String, _
                                    ByVal sRimContribuyente As String, ByVal sRifContribuyente As String, _
                                    ByVal sNombreContribuyente As String, ByVal sParroquiaContribuyente As String, _
                                    ByVal sSectorContribuyente As String, _
                                    ByVal bRevisarAno As Boolean, _
                                    ByRef UpdPanel As UpdatePanel) As Boolean
        Dim tran As SqlTransaction
        Dim cnn As SqlConnection = Nothing
        Dim numSolSeniat As Integer = 0
        Dim numConforme As Integer = 0
        Dim licSeniat As Integer = 0
        Dim lngLicenciaIdRes As Long = 0
        Dim intNumLicencia As Integer = 0
        Dim lngKioskoNumero As Integer = 0
        Dim iRenovacion As Integer = 0
        Dim intCantRenovaciones As Integer = IIf(IsNumeric(Session("CantRenovaciones")), Session("CantRenovaciones"), 0)
        Dim intRenovaciones As Integer = IIf(IsNumeric(Session("Renovaciones")), Session("Renovaciones"), 0)
        Dim dFechaRenovacion As Date = IIf(IsDate(Session("FechaRenovacion")), Session("FechaRenovacion"), Today)
        Dim stListaPeriodo As New ArrayList()

        If Not CheckCampos() Then _
            Return False
        If BtnExpirarLicencia.Visible Then
            RegisterMessage("No es posible realizar la operación debido a que la licencia se encuentra expirada")
            Return False
        End If

        If lngLicenciaId > 0 AndAlso ddlEstadoLicencia.SelectedValue = lngEstadoLicenciaAnula Then
            RegisterMessage("No es posible modificar las licencias que han sido anuladas")
            Return False
        End If

        cnn = modBaseDatos.obtenerConexion
        cnn.Open()
        tran = cnn.BeginTransaction

        Try
            If IsNumeric(txtNumeroLicencia.Text) AndAlso CInt(txtNumeroLicencia.Text.Trim) <> 0 Then intNumLicencia = CInt(txtNumeroLicencia.Text.Trim)
            If IsNumeric(sUsoContribuyente.Trim) Then numConforme = CInt(sUsoContribuyente.Trim)
            If IsNumeric(sSeniat.Trim) Then numSolSeniat = CInt(sSeniat.Trim)
            If IsNumeric(txtLicenciaSENIAT.Text.Trim()) Then licSeniat = CInt(txtLicenciaSENIAT.Text.Trim)


            If bRevisarAno AndAlso verifLicencia(lngContribuyenteId, _
                                                lngLicenciaId, _
                                                ddlTipoLicencia.SelectedValue, _
                                                ddlMotivoAutorizacion.SelectedValue, _
                                                ddlClasificacionLicencia.SelectedValue, _
                                                txtAnn.Text) <> 0 Then
                Throw New Exception("No es posible crear la misma licencia para el mismo año")
            End If

            If (ddlTipoLicencia.SelectedValue = lngTipoLicenciaPermanente) And (ddlMotivoAutorizacion.SelectedValue = 1) Then
                If verifLicencia(lngContribuyenteId, _
                                 lngLicenciaId, _
                                 ddlTipoLicencia.SelectedValue, _
                                 ddlMotivoAutorizacion.SelectedValue, _
                                 ddlClasificacionLicencia.SelectedValue) <> 0 Then
                    Throw New Exception("No es posible crear dos Licencias Permanentes de Instalación para la misma Clasificación")
                End If
            End If

            '---- Asigna la cantidad a multiplicar para la licencia temporal
            If EsTipoLicenciaTemporal(ddlTipoLicencia.SelectedValue) Then lngKioskoNumero = ddpNumeroKiosko.SelectedValue

            '---- Agrega las renovaciones en caso de existir
            If intCantRenovaciones > 0 Then
                For iRenovacion = 1 To (intCantRenovaciones - 1)
                    stListaPeriodo.Add(New Periodo(dFechaRenovacion.ToString("dd/MM/yyyy"), _
                                                   dFechaRenovacion.AddMonths(lngMesesVigencia).ToString("dd/MM/yyyy"), _
                                                   CType(Me.FindControl("txtNumeroControl" & iRenovacion), TextBox).Text, _
                                                   intRenovaciones + iRenovacion))
                    dFechaRenovacion = dFechaRenovacion.AddMonths(lngMesesVigencia)
                Next
                iRenovacion = intRenovaciones + iRenovacion
            End If

            '---- Anexo el periodo en cuestion
            stListaPeriodo.Add(New Periodo(Date.Parse(txtFechaInicio.Text, CultureInfo.GetCultureInfo("es-ES")), _
                                           Date.Parse(txtFechaVencimiento.Text, CultureInfo.GetCultureInfo("es-ES")), _
                                           txtNumeroControl.Text, _
                                           iRenovacion))


            For Each LicPeriodo As Periodo In stListaPeriodo
                '---- Ejecuta la accion en la base de datos
                lngLicenciaIdRes = GuardaLicencia(lngLicenciaId, lngContribuyenteId, _
                                               CType(Session("Usuario"), Sesion).UsuarioID, 1, _
                                               sRimContribuyente.Trim(), sRifContribuyente.Trim(), _
                                               sNombreContribuyente.Trim(), CInt(txtAnn.Text), _
                                               Today.ToString("dd/MM/yyyy"), _
                                               sParroquiaContribuyente.Trim(), _
                                               sSectorContribuyente.Trim, _
                                               numConforme, _
                                               CInt(txtNumeroSolicitud.Text.Trim), _
                                               txtFechaSolicitud.Text.Trim(), _
                                               numSolSeniat, txtFechaVencimiento.Text.Trim(), _
                                               0, _
                                               LicPeriodo.sNumeroControl.Trim, _
                                               ddlMotivoAutorizacion.SelectedItem.Text.Trim, _
                                               ddlTipoLicencia.SelectedItem.Text.Trim, _
                                               LicPeriodo.iRenovacion, _
                                               ddlEstadoLicencia.SelectedItem.Text.Trim(), _
                                               ddlHorario.SelectedItem.Text.Trim, _
                                               ddlEstadoLicencia.SelectedValue, _
                                               ddlTipoLicencia.SelectedValue, _
                                               LicPeriodo.dFechaInicio.ToString("dd/MM/yyyy"), _
                                               LicPeriodo.dFechaFin.ToString("dd/MM/yyyy"), _
                                               CInt(ddlClasificacionLicencia.SelectedValue), _
                                               ddlClasificacionLicencia.SelectedItem.Text.Trim, _
                                               txtNumeroExpediente.Text.Trim, _
                                               txtFechaEmision.Text.Trim, _
                                               CInt(ddlMotivoAutorizacion.SelectedValue), _
                                               CInt(ddlHorario.SelectedValue.ToString), 0, _
                                               licSeniat, intNumLicencia, _
                                               txtEvento.Text, lngKioskoNumero, txtKiosko.Text, txtUbicacionEv.Text, _
                                               cnn, tran)

                Select Case lngLicenciaIdRes
                    Case -2 : Throw New Exception("El campo [Numero de Control] se encuentra asignado, ingrese nuevamente los datos")
                    Case -3 : Throw New Exception("El campo [Numero de Solicitud] se encuentra asignado, ingrese nuevamente los datos")
                    Case -100 : Throw New Exception("No se puede generar la planilla debido a que no tiene asignado un ramo al motivo de autorizacion. Operacion Cancelada.")
                    Case Is < 0 : Throw New Exception("No se pudo registrar la licencia")
                End Select
            Next

            tran.Commit()
            LimpiarControles()
            Return True

        Catch ex As Exception
            tran.Rollback()

            If IsNothing(ex.InnerException) Then RegisterMessage(ex.Message) _
                                            Else RegisterMessage("Ocurrio un error desconocido")
            Return False
        Finally
            If cnn.State = ConnectionState.Open Then cnn.Close()
        End Try

    End Function


    Public Function InsertaLicenciaVehiculo(ByVal lngContribuyenteId As Long, ByVal lngLicenciaSessionId As Long, _
                                   ByVal sUsoContribuyente As String, ByVal sSeniat As String, _
                                   ByVal sRimContribuyente As String, ByVal sRifContribuyente As String, _
                                   ByVal sNombreContribuyente As String, ByVal sParroquiaContribuyente As String, _
                                   ByVal sSectorContribuyente As String, _
                                   ByVal bRevisarAno As Boolean, _
                                   ByRef arrVehiculo As ArrayList, _
                                   ByRef UpdPanel As UpdatePanel) As Boolean
        Dim tran As SqlTransaction
        Dim cnn As SqlConnection = Nothing
        Dim numSolSeniat As Integer = 0
        Dim numConforme As Integer = 0
        Dim licSeniat As Integer = 0
        Dim lngLicenciaIdRes As Long = 0
        Dim intNumLicencia As Integer = 0
        Dim lngKioskoNumero As Integer = 0
        Dim iRenovacion As Integer = 0
        Dim intCantRenovaciones As Integer = IIf(IsNumeric(Session("CantRenovaciones")), Session("CantRenovaciones"), 0)
        Dim intRenovaciones As Integer = IIf(IsNumeric(Session("Renovaciones")), Session("Renovaciones"), 0)
        Dim dFechaRenovacion As Date = IIf(IsDate(Session("FechaRenovacion")), Session("FechaRenovacion"), Today)
        Dim stListaPeriodo As New ArrayList()

        If Not CheckCampos() Then _
            Return False

        If BtnExpirarLicencia.Visible Then
            RegisterMessage("No es posible realizar la operación debido a que la licencia se encuentra expirada")
            Return False
        End If

        If lngLicenciaSessionId > 0 AndAlso ddlEstadoLicencia.SelectedValue = lngEstadoLicenciaAnula Then
            RegisterMessage("No es posible modificar las licencias que han sido anuladas")
            Return False
        End If

        cnn = modBaseDatos.obtenerConexion
        cnn.Open()
        tran = cnn.BeginTransaction
        Try
            If IsNumeric(txtNumeroLicencia.Text) AndAlso CInt(txtNumeroLicencia.Text.Trim) <> 0 Then intNumLicencia = CInt(txtNumeroLicencia.Text.Trim)
            If IsNumeric(sUsoContribuyente.Trim()) Then numConforme = CInt(sUsoContribuyente.Trim)
            If IsNumeric(sSeniat.Trim()) Then numSolSeniat = CInt(sSeniat.Trim)
            If IsNumeric(txtLicenciaSENIAT.Text.Trim()) Then licSeniat = CInt(txtLicenciaSENIAT.Text.Trim)

            If bRevisarAno AndAlso verifLicencia(lngContribuyenteId, _
                                                 lngLicenciaSessionId, _
                                                 ddlTipoLicencia.SelectedValue, _
                                                 ddlMotivoAutorizacion.SelectedValue, _
                                                 ddlClasificacionLicencia.SelectedValue, _
                                                 txtAnn.Text) <> 0 Then
                Throw New Exception("No es posible crear la misma licencia para el mismo año")
            End If

            If (ddlTipoLicencia.SelectedValue = lngTipoLicenciaPermanente) And (ddlMotivoAutorizacion.SelectedValue = 1) Then
                If verifLicencia(lngContribuyenteId, _
                                 lngLicenciaSessionId, _
                                 ddlTipoLicencia.SelectedValue, _
                                 ddlMotivoAutorizacion.SelectedValue, _
                                 ddlClasificacionLicencia.SelectedValue) <> 0 Then
                    Throw New Exception("No es posible crear dos Licencias Permanentes de Instalación para la misma Clasificación")
                End If
            End If

            '---- Asigna la cantidad a multiplicar para la licencia temporal
            If EsTipoLicenciaTemporal(ddlTipoLicencia.SelectedValue) Then lngKioskoNumero = ddpNumeroKiosko.SelectedValue

            '---- Agrega las renovaciones en caso de existir
            If intCantRenovaciones > 0 Then
                For iRenovacion = 1 To (intCantRenovaciones - 1)
                    stListaPeriodo.Add(New Periodo(dFechaRenovacion.ToString("dd/MM/yyyy"), _
                                                   dFechaRenovacion.AddMonths(lngMesesVigencia).ToString("dd/MM/yyyy"), _
                                                   CType(Me.FindControl("txtNumeroControl" & iRenovacion), TextBox).Text, _
                                                   intRenovaciones + iRenovacion))
                    dFechaRenovacion = dFechaRenovacion.AddMonths(lngMesesVigencia)
                Next
                iRenovacion = intRenovaciones + iRenovacion
            End If

            '---- Anexo el periodo en cuestion
            stListaPeriodo.Add(New Periodo(Date.Parse(txtFechaInicio.Text, CultureInfo.GetCultureInfo("es-ES")), _
                                           Date.Parse(txtFechaVencimiento.Text, CultureInfo.GetCultureInfo("es-ES")), _
                                           txtNumeroControl.Text, _
                                           iRenovacion))


            For Each LicPeriodo As Periodo In stListaPeriodo
                '---- Ejecuta la accion en la base de datos
                lngLicenciaIdRes = GuardaLicenciaVehiculo(lngLicenciaSessionId, lngContribuyenteId, _
                                                       CType(Session("Usuario"), Sesion).UsuarioID, _
                                                       1, _
                                                       sRimContribuyente.Trim, _
                                                       sRifContribuyente.Trim, _
                                                       sNombreContribuyente.Trim, _
                                                       CInt(txtAnn.Text), _
                                                       Today.ToString("dd/MM/yyyy"), _
                                                       sParroquiaContribuyente.Trim, _
                                                       sSectorContribuyente.Trim, _
                                                       numConforme, _
                                                       CInt(txtNumeroSolicitud.Text.Trim), _
                                                       txtFechaSolicitud.Text.Trim, _
                                                       numSolSeniat, _
                                                       txtFechaVencimiento.Text.Trim, _
                                                       0, _
                                                       LicPeriodo.sNumeroControl, _
                                                       ddlMotivoAutorizacion.SelectedItem.Text.Trim, _
                                                       ddlTipoLicencia.SelectedItem.Text.Trim, _
                                                       LicPeriodo.iRenovacion, _
                                                       ddlEstadoLicencia.SelectedItem.Text.Trim, _
                                                       ddlHorario.SelectedItem.Text.Trim, _
                                                       ddlEstadoLicencia.SelectedValue, _
                                                       ddlTipoLicencia.SelectedValue, _
                                                       LicPeriodo.dFechaInicio.ToString("dd/MM/yyyy"), _
                                                       LicPeriodo.dFechaFin.ToString("dd/MM/yyyy"), _
                                                       CInt(ddlClasificacionLicencia.SelectedValue.ToString), _
                                                       ddlClasificacionLicencia.SelectedItem.Text.Trim, _
                                                       txtNumeroExpediente.Text.Trim, _
                                                       txtFechaEmision.Text.Trim, _
                                                       CInt(ddlMotivoAutorizacion.SelectedValue.ToString), _
                                                       CInt(ddlHorario.SelectedValue.ToString), _
                                                       0, _
                                                       licSeniat, intNumLicencia, _
                                                       txtEvento.Text, lngKioskoNumero, txtKiosko.Text, txtUbicacionEv.Text, _
                                                       cnn, tran, arrVehiculo)

                Select Case lngLicenciaIdRes
                    Case -2 : Throw New Exception("El campo [Numero de control] se encuentra asignado, ingrese nuevamente los datos")
                    Case -3 : Throw New Exception("El campo [Numero de solicitud] se encuentra asignado, ingrese nuevamente los datos")
                    Case -100 : Throw New Exception("No se puede generar la planilla debido a que no tiene asignado un ramo al motivo de autorizacion. Operacion Cancelada.")
                    Case Is < 0 : Throw New Exception("No se pudo registrar la licencia")
                End Select
            Next

            tran.Commit()
            LimpiarControles()
            Return True

        Catch ex As Exception

            tran.Rollback()
            If IsNothing(ex.InnerException) Then RegisterMessage(ex.Message) _
                                            Else RegisterMessage("Ocurrio un error desconocido")
            Return False
        Finally
            If cnn.State = ConnectionState.Open Then cnn.Close()
        End Try

    End Function

    Protected Sub Page_Init(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Init
        '----- Carga las configuraciones ----
        CargarConfiguracionLicencia()
        If Not IsPostBack Then
            CargarTipoLicenciaTemporal()
            CargarClasificacionLicenciaDistribuidor()
            CargarListaKioskos()
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            Dim sReadOnlyOnSubmit = String.Format("{0}.disabled=true;{1}.disabled=true;{2}.disabled=true;", ddlTipoLicencia.ClientID, ddlClasificacionLicencia.ClientID, ddlMotivoAutorizacion.ClientID)

            txtFechaSolicitud.Attributes.Add("readOnly", "True")
            txtFechaEmision.Attributes.Add("readOnly", "True")
            txtFechaInicio.Attributes.Add("readOnly", "True")
            txtFechaVencimiento.Attributes.Add("readOnly", "True")
            txtKiosko.Attributes.Add("readOnly", "True")

            ddlTipoLicencia.Attributes.Add("onchange", sReadOnlyOnSubmit)
            ddlClasificacionLicencia.Attributes.Add("onchange", sReadOnlyOnSubmit)
            ddlMotivoAutorizacion.Attributes.Add("onchange", sReadOnlyOnSubmit)
            HideRenovacion()
        End If
    End Sub

    Protected Sub txtFechaInicio_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles txtFechaInicio.TextChanged
        Dim dDate As Date

        If Not EsTipoLicenciaTemporal(ddlTipoLicencia.SelectedValue) AndAlso Not txtFechaInicio.Text.Equals(String.Empty) Then
            dDate = Date.Parse(txtFechaInicio.Text, CultureInfo.GetCultureInfo("es-ES"))
            txtFechaVencimiento.Text = dDate.AddMonths(lngMesesVigencia).ToString("dd/MM/yyyy")
            txtFechaEmision.Text = txtFechaInicio.Text
        End If
    End Sub

    Protected Sub ddpNumeroKiosko_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddpNumeroKiosko.SelectedIndexChanged
        If ddpNumeroKiosko.SelectedValue > 0 Then
            txtKiosko.Text = String.Format("{0} ({1:00}) PUNTO DE EXPENDIO ", Num2Text(ddpNumeroKiosko.SelectedValue), ddpNumeroKiosko.SelectedValue.PadLeft(2, "0"))
        Else
            txtKiosko.Text = String.Empty
        End If
    End Sub

    Protected Sub BtnExpirarLicencia_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExpirarLicencia.Click
        '----- Expira la licencia
        Dim lngContribuyenteId As Long = Session("contribuyente_id")
        Dim cnn As SqlConnection
        Dim tran As SqlTransaction


        cnn = modBaseDatos.obtenerConexion
        cnn.Open()
        tran = cnn.BeginTransaction
        Try
            Select Case UpdateAnulaLicencia(lngContribuyenteId, txtNumeroExpediente.Text, cnn, tran)
                Case -2 : Throw New Exception("No esta configurado el código del estado que anula la licencia")
                Case -1 : Throw New Exception("Error de Base de Datos. Operación Cancelada")
                Case Is <> 0 : Throw New Exception("Error Desconocido")
            End Select
            ScriptManager.RegisterStartupScript(Me, Me.GetType, "msg", String.Format("alert('Operación Satisfactoria');location.href='{0}';", ResolveUrl("~/pagina/operacion/Licores/frmConsultaLicencia.aspx?ID=" & lngContribuyenteId.ToString)), True)
            tran.Commit()
        Catch ex As Exception
            tran.Rollback()
            RegisterMessage(ex.Message)
        Finally
            If cnn.State = ConnectionState.Open Then cnn.Close()
        End Try
    End Sub

    Protected Sub Page_PreRender(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreRender
        RegisterEvents()
    End Sub

End Class
