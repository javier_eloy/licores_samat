﻿<%@ Page Language="VB" MasterPageFile="~/pagina/plantilla/plantillaSegura.master" AutoEventWireup="false" CodeFile="frmLicenciaAprobadaExpendio.aspx.vb" Inherits="pagina_operacion_Licores_frmLicenciaAprobadaExpendio" title="Untitled Page" %>

<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <form id= "frm1" runat ="server"> 

    <table class="cssTabla" style="width: 998px; height: 472px">
             <tr>
                 <td align="center" colspan="5" style="height: 23px">
                     <asp:Label ID="Label1" runat="server" CssClass="cssTitulo" Text="Licencias de Licores"></asp:Label></td>
             </tr>
             <tr>
                 <td align="center" colspan="5" style="height: 24px">
                     <asp:Label ID="Label3" runat="server" CssClass="subtitulo" Text="DATOS GENERALES DEL CONTRIBUYENTE"></asp:Label></td>
             </tr>
             <tr>
                 <td align="left" style="width: 204px; height: 30px">
                     <asp:Label ID="Label2" runat="server" CssClass="subtitulo" Text="Nombre / Razón Social:"></asp:Label></td>
                 <td align="left" style="width: 246px; height: 30px">
                     <asp:Label ID="lblNombreContribuyente" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
                 <td style="width: 48px; height: 30px">
                 </td>
                 <td align="left" style="width: 190px; height: 30px">
                     <asp:Label ID="Label5" runat="server" CssClass="subtitulo" Text="Estado:"></asp:Label></td>
                 <td align="left" style="width: 217px; height: 30px">
                     <asp:Label ID="lblEstadoContribuyente" runat="server" CssClass="normal" Text=" "
                         Width="104px"></asp:Label>
                     <asp:Label ID="lblEstClasificacionContribuyente" runat="server" CssClass="normal"
                         Text=" " Width="104px"></asp:Label></td>
             </tr>
             <tr>
                 <td align="left" style="width: 204px; height: 30px">
                     <asp:Label ID="Label8" runat="server" CssClass="subtitulo" Text="Lema Comercial:"></asp:Label></td>
                 <td align="left" colspan="4" style="height: 30px">
                     <asp:Label ID="lblLemaContribuyente" runat="server" CssClass="normal" Width="244px"></asp:Label>&nbsp;</td>
             </tr>
             <tr>
                 <td align="left" style="width: 204px; height: 30px">
                     <asp:Label ID="Label13" runat="server" CssClass="subtitulo" Text="C.I.:"></asp:Label></td>
                 <td align="left" style="width: 246px; height: 30px">
                     <asp:Label ID="lblCedulaContribuyente" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
                 <td style="width: 48px; height: 30px">
                 </td>
                 <td align="left" style="width: 190px; height: 30px">
                     <asp:Label ID="Label12" runat="server" CssClass="subtitulo" Text="RIF:"></asp:Label></td>
                 <td align="left" style="width: 217px; height: 30px">
                     <asp:Label ID="lblRIFContribuyente" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
             </tr>
             <tr>
                 <td align="left" style="width: 204px; height: 30px">
                     <asp:Label ID="Label14" runat="server" CssClass="subtitulo" Text="RIM:"></asp:Label></td>
                 <td align="left" style="width: 246px; height: 30px">
                     <asp:Label ID="lblRIMContribuyente" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
                 <td style="width: 48px; height: 30px">
                 </td>
                 <td align="left" style="width: 190px; height: 30px">
                     &nbsp;<asp:Label ID="Label18" runat="server" CssClass="subtitulo" Text="Nº de Uso Conforme:"></asp:Label></td>
                 <td align="left" style="width: 217px; height: 30px">
                     <asp:Label ID="lblUsoContribuyente" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
             </tr>
             <tr>
                 <td align="left" style="width: 204px; height: 30px">
                     <asp:Label ID="Label30" runat="server" CssClass="subtitulo" Text="Parroquia Fiscal:"></asp:Label></td>
                 <td align="left" style="width: 246px; height: 30px">
                     <asp:Label ID="lblParroquiaContribuyente" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
                 <td style="width: 48px; height: 30px">
                 </td>
                 <td align="left" style="width: 190px; height: 30px">
                     <asp:Label ID="Label32" runat="server" CssClass="subtitulo" Text="Sector Fiscal:"></asp:Label></td>
                 <td align="left" style="width: 217px; height: 30px">
                     <asp:Label ID="lblSectorContribuyente" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
             </tr>
             <tr>
                 <td align="left" style="width: 204px; height: 30px">
                     <asp:Label ID="Label34" runat="server" CssClass="subtitulo" Text="Dirección Fiscal:"></asp:Label></td>
                 <td align="left" colspan="4" style="height: 30px">
                     <asp:Label ID="lblDireccionContribuyente" runat="server" CssClass="normal" Width="737px"></asp:Label></td>
             </tr>
             <tr>
                 <td align="left" style="width: 204px; height: 30px">
                     <asp:Label ID="Label10" runat="server" CssClass="subtitulo" Text="Teléfono Principal:"></asp:Label></td>
                 <td align="left" style="width: 246px; height: 30px">
                     <asp:Label ID="lblTelefonoContribuyente" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
                 <td style="width: 48px; height: 30px">
                 </td>
                 <td align="left" style="width: 190px; height: 30px">
                     <asp:Label ID="Label19" runat="server" CssClass="subtitulo" Text="Teléfono Adicional:"></asp:Label></td>
                 <td align="left" style="width: 217px; height: 30px">
                     <asp:Label ID="lblTlfAdicionalContribuyente" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
             </tr>
             <tr>
                 <td align="left" style="width: 204px; height: 30px">
                     <asp:Label ID="Label29" runat="server" CssClass="subtitulo" Text="Teléfono Celular:"></asp:Label></td>
                 <td align="left" style="width: 246px; height: 30px">
                     <asp:Label ID="lblCelularContribuyente" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
                 <td style="width: 48px; height: 30px">
                 </td>
                 <td align="left" style="width: 190px; height: 30px">
                     <asp:Label ID="Label24" runat="server" CssClass="subtitulo" Text="Teléfono Celular Adicional:"></asp:Label></td>
                 <td align="left" style="width: 217px; height: 30px">
                     <asp:Label ID="lblCelAdicionalContribuyente" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
             </tr>
             <tr>
                 <td align="left" style="width: 204px; height: 30px">
                     <asp:Label ID="Label25" runat="server" CssClass="subtitulo" Text="E-mail:"></asp:Label></td>
                 <td align="left" style="width: 246px; height: 30px">
                     <asp:Label ID="lblEmailContribuyente" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
                 <td style="width: 48px; height: 30px">
                 </td>
                 <td align="left" style="width: 190px; height: 30px">
                     <asp:Label ID="Label27" runat="server" CssClass="subtitulo" Text="Página Web:"></asp:Label></td>
                 <td align="left" style="width: 217px; height: 30px">
                     <asp:Label ID="lblPaginaContribuyente" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
             </tr>
             <tr>
                 <td align="left" style="width: 204px; height: 30px">
                     <asp:Label ID="Label23" runat="server" CssClass="subtitulo" Text="Parroquia del Establecimiento:"></asp:Label></td>
                 <td align="left" style="width: 246px; height: 30px">
                     <asp:Label ID="lblParroquiaEstablecimiento" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
                 <td style="width: 48px; height: 30px">
                 </td>
                 <td align="left" style="width: 190px; height: 30px">
                     <asp:Label ID="Label41" runat="server" CssClass="subtitulo" Text="Sector del Establecimiento:"></asp:Label></td>
                 <td align="left" style="width: 217px; height: 30px">
                     <asp:Label ID="lblSectorEstablecimiento" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
             </tr>
             <tr>
                 <td align="left" style="width: 204px; height: 30px">
                     <asp:Label ID="Label43" runat="server" CssClass="subtitulo" Text="Dirección del Establecimiento:"></asp:Label></td>
                 <td align="left" colspan="4" style="height: 30px">
                     <asp:Label ID="lblDireccionEstablecimiento" runat="server" CssClass="normal" Width="739px"></asp:Label></td>
             </tr>
             <tr>
                 <td align="left" style="width: 204px; height: 30px">
                     <asp:Label ID="Label45" runat="server" CssClass="subtitulo" Text="Numero de Licencia:"></asp:Label></td>
                 <td align="left" style="width: 246px; height: 30px">
                     <asp:Label ID="lblNumLicencia" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
                 <td style="width: 48px; height: 30px">
                 </td>
                 <td align="left" style="width: 190px; height: 30px">
                     <asp:Label ID="Label47" runat="server" CssClass="subtitulo" Text="Número de Licencia SENIAT:"></asp:Label></td>
                 <td align="left" style="width: 217px; height: 30px">
                     <asp:Label ID="lblSeniat" runat="server" CssClass="normal" Width="244px"></asp:Label></td>
             </tr>
         </table>
         
     &nbsp;<asp:ScriptManager id="ScriptManager1" runat="server">
     </asp:ScriptManager>
     <asp:UpdatePanel id="UpdatePanel1" runat="server">
         <contenttemplate>
<asp:SqlDataSource id="SqlDSHorario" runat="server" SelectCommand="SELECT horario_licencia_id, descripcion_horario_licencia FROM horario_licencia" ConnectionString="<%$ ConnectionStrings:TRIBUTOConnectionString %>" ProviderName="<%$ ConnectionStrings:TRIBUTOConnectionString.ProviderName %>"></asp:SqlDataSource> <asp:SqlDataSource id="SqlDSEstadoLicencia" runat="server" SelectCommand="SELECT estado_licencia_id, descripcion_estado_licencia FROM ESTADO_LICENCIA" ConnectionString="<%$ ConnectionStrings:TRIBUTOConnectionString %>" ProviderName="<%$ ConnectionStrings:TRIBUTOConnectionString.ProviderName %>"></asp:SqlDataSource><asp:SqlDataSource id="SqlDSMotivoAutorizacion" runat="server" SelectCommand="SELECT motivo_autorizacion_id, descripcion_motivo_autorizacion FROM MOTIVO_AUTORIZACION" ConnectionString="<%$ ConnectionStrings:TRIBUTOConnectionString %>" ProviderName="<%$ ConnectionStrings:TRIBUTOConnectionString.ProviderName %>"></asp:SqlDataSource> <asp:TextBox id="txtindice" runat="server" Width="76px" Visible="False"></asp:TextBox> <asp:TextBox id="txtActualizar" runat="server" Visible="False"></asp:TextBox> <asp:SqlDataSource id="SqlDSTipoLicencia" runat="server" SelectCommand="SELECT tipo_licencia_id, descripcion_tipo_licencia FROM TIPO_LICENCIA" ConnectionString="<%$ ConnectionStrings:TRIBUTOConnectionString %>" ProviderName="<%$ ConnectionStrings:TRIBUTOConnectionString.ProviderName %>"></asp:SqlDataSource> <asp:SqlDataSource id="SqlDSClasificacionLicencia" runat="server" SelectCommand="SELECT clasificacion_licencia_id, descripcion_clasificacion_licencia FROM vis_clasificacion_licencia" ConnectionString="<%$ ConnectionStrings:TRIBUTOConnectionString %>" ProviderName="<%$ ConnectionStrings:TRIBUTOConnectionString.ProviderName %>"></asp:SqlDataSource> <DIV style="LEFT: 458px; WIDTH: 100px; POSITION: absolute; TOP: 778px; HEIGHT: 18px" id="DivCalendar"><asp:Calendar id="calendario" runat="server" Width="220px" Visible="False" Height="180px" BorderColor="#999999" CellPadding="4" BackColor="White" Font-Names="Verdana" Font-Size="8pt" DayNameFormat="Shortest" ForeColor="Black" OnSelectionChanged="calendario_SelectionChanged">
<SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White"></SelectedDayStyle>

<SelectorStyle BackColor="#CCCCCC"></SelectorStyle>

<WeekendDayStyle BackColor="#FFFFCC"></WeekendDayStyle>

<TodayDayStyle BackColor="#CCCCCC" ForeColor="Black"></TodayDayStyle>

<OtherMonthDayStyle ForeColor="Gray"></OtherMonthDayStyle>

<NextPrevStyle VerticalAlign="Bottom"></NextPrevStyle>

<DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt"></DayHeaderStyle>

<TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True"></TitleStyle>
</asp:Calendar></DIV><TABLE style="WIDTH: 1000px" id="Table2" class="cssTabla" onclick="return TABLE1_onclick()"><TBODY><TR><TD style="HEIGHT: 30px" align=center colSpan=5 rowSpan=2><asp:Label id="Label50" runat="server" Text="DATOS DE LA LICENCIA" CssClass="subtitulo"></asp:Label></TD></TR><TR></TR><TR><TD style="WIDTH: 202px; HEIGHT: 16px" align=left><asp:Label id="Label51" runat="server" Text="Tipo de Licencia:" CssClass="subtitulo"></asp:Label></TD><TD style="WIDTH: 203px; HEIGHT: 16px" align=left><asp:DropDownList id="ddlTipoLicencia" runat="server" Width="236px" AppendDataBoundItems="True" EnableViewState="False" AutoPostBack="True" DataValueField="tipo_licencia_id" DataTextField="descripcion_tipo_licencia" DataSourceID="SqlDSTipoLicencia"><asp:ListItem Value="0">SELECCIONE</asp:ListItem>
</asp:DropDownList></TD><TD style="WIDTH: 50px; HEIGHT: 16px"></TD><TD style="WIDTH: 200px; HEIGHT: 16px" align=left><asp:Label id="Label53" runat="server" Text="Motivo de la Autorización:" CssClass="subtitulo" Width="192px"></asp:Label></TD><TD style="WIDTH: 222px; HEIGHT: 16px" align=left><asp:DropDownList id="ddlMotivoAutorizacion" runat="server" Width="236px" AppendDataBoundItems="True" EnableViewState="False" AutoPostBack="True" DataValueField="motivo_autorizacion_id" DataTextField="descripcion_motivo_autorizacion" DataSourceID="SqlDSMotivoAutorizacion"><asp:ListItem>SELECCIONE</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="WIDTH: 202px; HEIGHT: 22px" align=left><asp:Label id="Label58" runat="server" Text="Clasificación de la Licencia:" CssClass="subtitulo" Width="200px"></asp:Label></TD><TD style="WIDTH: 203px; HEIGHT: 22px" align=left><asp:DropDownList id="ddlClasificacionLicencia" runat="server" Width="236px" AppendDataBoundItems="True" EnableViewState="False" AutoPostBack="True" DataValueField="clasificacion_licencia_id" DataTextField="descripcion_clasificacion_licencia" DataSourceID="SqlDSClasificacionLicencia"><asp:ListItem Value="0">SELECCIONE</asp:ListItem>
</asp:DropDownList></TD><TD style="WIDTH: 50px; HEIGHT: 22px"></TD><TD style="WIDTH: 200px; HEIGHT: 22px" align=left><asp:Label id="Label59" runat="server" Text="Estado de la Licencia:" CssClass="subtitulo"></asp:Label></TD><TD style="WIDTH: 222px; HEIGHT: 22px" align=left><asp:DropDownList id="ddlEstadoLicencia" runat="server" Width="236px" AppendDataBoundItems="True" EnableViewState="False" AutoPostBack="True" DataValueField="estado_licencia_id" DataTextField="descripcion_estado_licencia" DataSourceID="SqlDSEstadoLicencia"><asp:ListItem>SELECCIONE</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="WIDTH: 202px; HEIGHT: 30px" align=left><asp:Label id="Label61" runat="server" Text="Horario:" CssClass="subtitulo"></asp:Label></TD><TD style="HEIGHT: 30px" align=left colSpan=4><asp:DropDownList id="ddlHorario" runat="server" Width="758px" AppendDataBoundItems="True" EnableViewState="False" AutoPostBack="True" DataValueField="horario_licencia_id" DataTextField="descripcion_horario_licencia" DataSourceID="SqlDSHorario"><asp:ListItem>SELECCIONE</asp:ListItem>
</asp:DropDownList></TD></TR><TR><TD style="WIDTH: 202px; HEIGHT: 30px" align=left><asp:Label id="Label65" runat="server" Text="Número de Control:" CssClass="subtitulo"></asp:Label></TD><TD style="WIDTH: 203px; HEIGHT: 30px" align=left><asp:TextBox id="txtNumeroControl" runat="server" Width="230px"></asp:TextBox></TD><TD style="WIDTH: 50px; HEIGHT: 30px"></TD><TD style="WIDTH: 200px; HEIGHT: 30px" align=left><asp:Label id="Label67" runat="server" Text="Número de Renovaciones:" CssClass="subtitulo"></asp:Label></TD><TD style="WIDTH: 222px; HEIGHT: 30px" align=left><asp:Label id="Label68" runat="server" Text="2" CssClass="normal" Width="8px"></asp:Label></TD></TR><TR><TD style="WIDTH: 202px; HEIGHT: 30px" align=left><asp:Label id="Label49" runat="server" Text="Número de Autorización:" CssClass="subtitulo"></asp:Label></TD><TD style="WIDTH: 203px; HEIGHT: 30px" align=left><asp:TextBox id="txtNumeroAutorizacion" runat="server" Width="230px"></asp:TextBox></TD><TD style="WIDTH: 50px; HEIGHT: 30px"></TD><TD style="WIDTH: 200px; HEIGHT: 30px" align=left><asp:Label id="Label52" runat="server" Text="Número de Solicitud:" CssClass="subtitulo"></asp:Label></TD><TD style="WIDTH: 222px; HEIGHT: 30px" align=left><asp:TextBox id="txtNumeroSolicitud" runat="server" Width="230px"></asp:TextBox></TD></TR><TR><TD style="WIDTH: 202px; HEIGHT: 30px" align=left><asp:Label id="Label71" runat="server" Text="Fecha de Solicitud:" CssClass="subtitulo"></asp:Label></TD><TD style="WIDTH: 203px; HEIGHT: 30px" align=left><asp:TextBox id="txtFechaSolicitud" runat="server" Width="168px" Enabled="False"></asp:TextBox> <asp:ImageButton id="btnFechaSolicitud" onclick="btnFechaSolicitud_Click" runat="server" ImageUrl="~/Images/imagen/icono/icono-calendar.gif"></asp:ImageButton> </TD><TD style="WIDTH: 50px; HEIGHT: 30px"></TD><TD style="WIDTH: 200px; HEIGHT: 30px" align=left><asp:Label id="Label74" runat="server" Text="Fecha de Emisión:" CssClass="subtitulo"></asp:Label></TD><TD style="WIDTH: 222px; HEIGHT: 30px" align=left><asp:TextBox id="txtFechaEmision" runat="server" Width="180px" Enabled="False"></asp:TextBox>&nbsp; <asp:ImageButton id="btnFechaEmision" onclick="btnFechaEmision_Click" runat="server" ImageUrl="~/Images/imagen/icono/icono-calendar.gif"></asp:ImageButton></TD></TR><TR><TD style="WIDTH: 202px; HEIGHT: 30px" align=left><asp:Label id="Label77" runat="server" Text="Fecha de Vencimiento:" CssClass="subtitulo"></asp:Label></TD><TD style="WIDTH: 203px; HEIGHT: 30px" align=left><asp:TextBox id="txtFechaVencimiento" runat="server" Width="168px" Enabled="False"></asp:TextBox>&nbsp;<asp:ImageButton id="FechaVencimiento" onclick="FechaVencimiento_Click" runat="server" ImageUrl="~/Images/imagen/icono/icono-calendar.gif"></asp:ImageButton></TD><TD style="WIDTH: 50px; HEIGHT: 30px"></TD><TD style="WIDTH: 200px; HEIGHT: 30px" align=left><asp:Label id="Label80" runat="server" Text="Número de Licencia:" CssClass="subtitulo"></asp:Label></TD><TD style="WIDTH: 222px; HEIGHT: 30px" align=left><asp:TextBox id="txtNumeroLicencia" runat="server" Width="230px" Enabled="False">GENERADO AUTOMATICAMENTE</asp:TextBox></TD></TR><TR><TD style="WIDTH: 202px" align=left><asp:Label id="Label83" runat="server" Text="Año:" CssClass="subtitulo"></asp:Label></TD><TD style="WIDTH: 203px" align=left><asp:TextBox id="txtAnn" runat="server" Width="72px"></asp:TextBox></TD><TD style="WIDTH: 50px"></TD><TD style="WIDTH: 200px" align=left></TD><TD style="WIDTH: 222px" align=left></TD></TR><TR><TD style="HEIGHT: 30px" align=left colSpan=5><asp:Button id="BtnInsertar" onclick="BtnInsertar_Click" runat="server" Text="Actualizar" CssClass="cssBoton" Width="68px"></asp:Button>&nbsp; <asp:Button id="BtnCancelar" runat="server" Text="Cancelar" CssClass="cssBoton" Width="64px" OnClick="BtnCancelar_Click"></asp:Button></TD></TR></TBODY></TABLE>
</contenttemplate>
     </asp:UpdatePanel>
    </form> 
</asp:Content>

