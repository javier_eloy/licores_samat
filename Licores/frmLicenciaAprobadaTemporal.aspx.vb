﻿Imports System.Data.SqlClient
Partial Class pagina_operacion_Licores_frmLicenciaAprobadaTemporal
    Inherits System.Web.UI.Page
    Protected Sub BtnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancelar.Click
        Response.Redirect(ResolveUrl("~/pagina/operacion/Licores/frmConsultaLicencia.aspx"))
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Call CargaDatosContribuyente()
    End Sub

    Protected Sub BtnInsertar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Call GuardaLicencia()
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub btnFechaSolicitud_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        calendario.Visible = True
        txtindice.Text = "1"
    End Sub

    Protected Sub calendario_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Select Case txtindice.Text
            Case "1"
                InsertaFecha(txtFechaSolicitud)
            Case "2"
                InsertaFecha(txtFechaVencimiento)
            Case "3"
                InsertaFecha(txtFechaEmision)
        End Select

    End Sub

    Private Sub InsertaFecha(ByVal txt As TextBox)
        Dim ann As Integer
        txt.Text = calendario.SelectedDate
        If txtindice.Text = "3" Then
            ann = CInt(calendario.SelectedDate.Year)
            ann = ann + 1
            txtFechaVencimiento.Text = calendario.SelectedDate.Day & "/" & Format(calendario.SelectedDate.Month, "00") & "/" & ann.ToString
        End If
        calendario.Visible = False
    End Sub

    Protected Sub FechaVencimiento_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        calendario.Visible = True
        txtindice.Text = "2"
    End Sub

    Protected Sub btnFechaEmision_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        calendario.Visible = True
        txtindice.Text = "3"
    End Sub

    Private Sub GuardaLicencia()
        Dim sql As String = vbNullString
        Dim cnn As SqlConnection = Nothing
        Dim cmd As SqlCommand = Nothing
        Dim dr As System.Data.IDataReader
        Dim id As Integer

        cnn = modBaseDatos.obtenerConexion

        Try
            cnn.Open()

            sql = "Select Max(licencia_id) + 1 from LICENCIA"
            cmd = New SqlCommand(sql, cnn)
            dr = cmd.ExecuteReader()
            If dr.Read() = True Then
                id = CInt(dr.GetValue(0).ToString())
            End If
            dr.Close()


            sql = "Insert into LICENCIA "
            sql += "(licencia_id, "
            sql += "contribuyente_id, "
            sql += "usuario_id, "
            sql += "tipo_licencia_id, "
            sql += "motivo_autorizacion_id, "
            sql += "estado_licencia_id, "
            sql += "horario_licencia_id, "
            sql += "num_control, "
            sql += "num_autorizacion, "
            sql += "num_solicitud, "
            sql += "fecha_solicitud, "
            sql += "fecha_inicio_licencia, "
            sql += "fecha_vencimiento_licencia, "
            sql += "año) values "
            sql += "(" & id & ", "
            sql += "" & Session("contribuyente_id") & ", "
            sql += "" & CType(Session("Usuario"), Sesion).UsuarioID.ToString & ", "
            sql += "" & ddlTipoLicencia.SelectedValue & ", "
            sql += "" & ddlMotivoAutorizacion.SelectedValue & ", "    
            sql += "" & ddlEstadoLicencia.SelectedValue & ", "
            sql += "" & ddlHorario.SelectedValue & ", "
            sql += "'" & txtNumeroControl.Text & "', "
            sql += "'" & txtNumeroAutorizacion.Text & "', "
            sql += "'" & txtNumeroSolicitud.Text & "', "
            sql += "'" & Format(CDate(txtFechaSolicitud.Text), "yyyyMMdd") & "', "
            sql += "'" & Format(CDate(txtFechaEmision.Text), "yyyyMMdd") & "', "
            sql += "'" & Format(CDate(txtFechaVencimiento.Text), "yyyyMMdd") & "', "
            sql += "'" & txtAnn.Text & "')"
            cmd = New SqlCommand(sql, cnn)
            cmd.ExecuteNonQuery()
            System.Windows.Forms.MessageBox.Show("Datos registrados con exito.")
            Call Nuevo()
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try

    End Sub

    Private Sub CargaDatosContribuyente()
        lblUsoContribuyente.Text = Session("UsoContribuyente")
        lblNombreContribuyente.Text = Session("NombreContribuyente")
        lblLemaContribuyente.Text = Session("LemaContribuyente")
        lblRIFContribuyente.Text = Session("RIFContribuyente")
        lblCedulaContribuyente.Text = Session("CedulaContribuyente")
        lblRIMContribuyente.Text = Session("RIMContribuyente")
        lblParroquiaContribuyente.Text = Session("ParroquiaContribuyente")
        lblDireccionContribuyente.Text = Session("DireccionContribuyente")
        lblTelefonoContribuyente.Text = Session("TelefonoContribuyente")
        lblCelularContribuyente.Text = Session("CelularContribuyente")
        lblEmailContribuyente.Text = Session("EmailContribuyente")
        lblEstadoContribuyente.Text = Session("EstadoContribuyente")
        lblEstClasificacionContribuyente.Text = Session("EstClasificacionContribuyente")
        lblSectorContribuyente.Text = Session("SectorContribuyente")
        lblTlfAdicionalContribuyente.Text = Session("TlfAdicionalContribuyente")
        lblCelAdicionalContribuyente.Text = Session("CelAdicionalContribuyente")
        lblPaginaContribuyente.Text = Session("PaginaContribuyente")
        lblParroquiaEstablecimiento.Text = Session("ParroquiaEstablecimiento")
        lblSectorEstablecimiento.Text = Session("SectorEstablecimiento")
        lblDireccionEstablecimiento.Text = Session("DireccionEstablecimiento")
        'lblNumLicencia.Text = Session("NumLicencia")
        lblSeniat.Text = Session("Seniat")
    End Sub

    Private Sub Nuevo()
        ddlTipoLicencia.ClearSelection()
        ddlMotivoAutorizacion.ClearSelection()       
        ddlEstadoLicencia.ClearSelection()
        ddlHorario.ClearSelection()
        txtNumeroControl.Text = ""
        txtNumeroAutorizacion.Text = ""
        txtNumeroSolicitud.Text = ""
        txtFechaSolicitud.Text = ""
        txtFechaEmision.Text = ""
        txtFechaVencimiento.Text = ""
        txtAnn.Text = ""
    End Sub

End Class
