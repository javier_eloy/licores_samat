﻿<%@ Page Language="VB" MasterPageFile="~/pagina/plantilla/plantillaSegura.master"
    AutoEventWireup="false" CodeFile="frmConsultaLicencia.aspx.vb" Inherits="pagina_operacion_Licores_frmConsultaLicencia"
    Title="Untitled Page" %>
<%@ Register Src="wucDatosContribuyente.ascx" TagName="frmDatosContribuyente" TagPrefix="uc1" %>
<%@ Register Assembly="CrystalDecisions.Web, Version=10.2.3600.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>
<%@ Register Assembly="System.Web.Extensions, Version=1.0.61025.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35"
    Namespace="System.Web.UI" TagPrefix="asp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <form id="frm1" runat="server">
        <uc1:frmDatosContribuyente ID="ucDatosContribuyentes" runat="server" />
        <br />
        <asp:ScriptManager ID="ScriptMan" runat="server">
        </asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePnl" runat="server">
            <ContentTemplate>
<asp:SqlDataSource id="SqlDSLicencia" runat="server" SelectCommand="SELECT licencia_id, num_licencia FROM LICENCIA WHERE ([contribuyente_id] = @contribuyente_id)" ConnectionString="<%$ ConnectionStrings:TRIBUTOConnectionString %>" ProviderName="<%$ ConnectionStrings:TRIBUTOConnectionString.ProviderName %>">
                    <SelectParameters>
                        <asp:SessionParameter SessionField="contribuyente_id" DefaultValue="" Name="contribuyente_id">
                        </asp:SessionParameter>
                    </SelectParameters>
                </asp:SqlDataSource> <asp:SqlDataSource id="SqlDSTipoLicencia" runat="server" SelectCommand="SELECT tipo_licencia_id, descripcion_tipo_licencia FROM vis_tipo_licencia" ConnectionString="<%$ ConnectionStrings:TRIBUTOConnectionString %>" ProviderName="<%$ ConnectionStrings:TRIBUTOConnectionString.ProviderName %>">
                </asp:SqlDataSource> <asp:SqlDataSource id="SqlDSClasificacionLicencia" runat="server" SelectCommand="SELECT clasificacion_licencia_id, descripcion_clasificacion_licencia FROM vis_clasificacion_licencia" ConnectionString="<%$ ConnectionStrings:TRIBUTOConnectionString %>" ProviderName="<%$ ConnectionStrings:TRIBUTOConnectionString.ProviderName %>">
                </asp:SqlDataSource> <asp:SqlDataSource id="SqlDSLicencias" runat="server" ConnectionString="<%$ ConnectionStrings:TRIBUTOConnectionString %>" ProviderName="<%$ ConnectionStrings:TRIBUTOConnectionString.ProviderName %>"></asp:SqlDataSource> <TABLE style="WIDTH: 998px" class="cssTabla"><TBODY><TR><TD style="HEIGHT: 30px" colSpan=8><asp:Label id="Label49" runat="server" CssClass="subtitulo" Text="DATOS DE LA LICENCIA"></asp:Label></TD></TR><TR><TD style="WIDTH: 163px; HEIGHT: 30px" align=left><asp:Label id="Label50" runat="server" CssClass="subtitulo" Text="Nº de Licencia:"></asp:Label></TD><TD style="WIDTH: 120px; HEIGHT: 30px"><asp:DropDownList id="ddlNumLicencia" runat="server" AppendDataBoundItems="True" EnableViewState="False" AutoPostBack="True" DataValueField="licencia_id" DataTextField="num_licencia" DataSourceID="SqlDSLicencia" Width="130px">
                                    <asp:ListItem Value="0">SELECCIONE</asp:ListItem>
                                </asp:DropDownList></TD><TD style="WIDTH: 6px; HEIGHT: 30px"></TD><TD style="WIDTH: 109px; HEIGHT: 30px"><asp:Label id="Label51" runat="server" CssClass="subtitulo" Text="Tipo de Licencia:" Width="104px"></asp:Label></TD><TD style="WIDTH: 177px; HEIGHT: 30px"><asp:DropDownList id="ddlTipoLicencia" runat="server" AppendDataBoundItems="True" EnableViewState="False" AutoPostBack="True" DataValueField="tipo_licencia_id" DataTextField="descripcion_tipo_licencia" DataSourceID="SqlDSTipoLicencia" Width="170px">
                                    <asp:ListItem Value="0">SELECCIONE</asp:ListItem>
                                </asp:DropDownList></TD><TD style="WIDTH: 7px; HEIGHT: 30px"></TD><TD style="WIDTH: 242px; HEIGHT: 30px"><asp:Label id="Label52" runat="server" CssClass="subtitulo" Text="Clasif. de la Licencia:"></asp:Label></TD><TD style="HEIGHT: 30px"><asp:DropDownList id="ddlClasificacionLicencia" runat="server" AppendDataBoundItems="True" EnableViewState="False" AutoPostBack="True" DataValueField="clasificacion_licencia_id" DataTextField="descripcion_clasificacion_licencia" DataSourceID="SqlDSClasificacionLicencia" Width="220px">
                                    <asp:ListItem Value="0">SELECCIONE</asp:ListItem>
                                </asp:DropDownList></TD></TR><TR><TD style="HEIGHT: 43px" align=left colSpan=8><asp:Button id="BtnBuscar" onclick="BtnBuscar_Click" runat="server" CssClass="cssBoton" Text="Buscar" Width="56px"></asp:Button>&nbsp; <asp:Button id="BtnNuevo" onclick="BtnNuevo_Click" runat="server" CssClass="cssBoton" Text="Nuevo" Width="56px"></asp:Button></TD></TR><TR><TD style="HEIGHT: 50px" colSpan=8><asp:GridView id="Grid_Licencias" runat="server" CssClass="cssGrid" DataSourceID="SqlDSLicencias" Width="950px" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" EmptyDataText="No se encontraron registros." Font-Bold="False" Font-Italic="False" Font-Names="Tahoma" Font-Size="10pt" PageSize="25">
<RowStyle CssClass="cssFila"></RowStyle>

<EmptyDataRowStyle CssClass="grid_vacio"></EmptyDataRowStyle>
<Columns>
<asp:ButtonField CommandName="Edit" ImageUrl="~/Images/Botones/btnEdit.gif" ButtonType="Image"></asp:ButtonField>
<asp:ButtonField CommandName="Print" ImageUrl="~/imagen/icono/icono-imprimir-pdf.gif" ButtonType="Image"></asp:ButtonField>
<asp:BoundField DataField="licencia_id" HeaderText="Licencia_Id"></asp:BoundField>
<asp:BoundField DataField="num_licencia" HeaderText="N&#186; de Licencia"></asp:BoundField>
<asp:BoundField DataField="num_control" HeaderText="N&#250;mero de Control"></asp:BoundField>
<asp:BoundField DataField="fecha_inicio_licencia" HeaderText="Fecha de Emisi&#243;n"></asp:BoundField>
<asp:BoundField DataField="descripcion_tipo_licencia" HeaderText="Tipo de Licencia"></asp:BoundField>
<asp:BoundField DataField="descripcion_clasificacion_licencia" HeaderText="Clasificaci&#243;n de Licencia"></asp:BoundField>
<asp:BoundField DataField="esdistribuidor" HeaderText="esdistribuidor"></asp:BoundField>
<asp:BoundField DataField="estemporal" HeaderText="estemporal"></asp:BoundField>
</Columns>

<HeaderStyle CssClass="cssCabecera"></HeaderStyle>

<AlternatingRowStyle CssClass="grid_consulta_alt" Font-Names="Tahoma" Font-Size="10pt"></AlternatingRowStyle>
</asp:GridView> <BR /></TD></TR><TR><TD style="HEIGHT: 30px" align=left colSpan=8><asp:Button id="BtnVolver" onclick="BtnVolver_Click" runat="server" CssClass="cssBoton" Text="Volver" Width="56px"></asp:Button></TD></TR></TBODY></TABLE>
</ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="Grid_Licencias" EventName="RowCommand" />
            </Triggers>
        </asp:UpdatePanel>
    </form>
</asp:Content>
