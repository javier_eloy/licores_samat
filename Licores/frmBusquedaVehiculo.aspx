﻿<%@ Page Language="VB" MasterPageFile="~/pagina/plantilla/plantillaSegura.master" AutoEventWireup="false" CodeFile="frmBusquedaVehiculo.aspx.vb" Inherits="pagina_operacion_Licores_emrBusquedaVehiculo" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<form id= "frm1" runat ="server"> 
    <br />
    <table class="cssTabla" style="width: 816px">
        <tr>
            <td colspan="3" style="height: 30px">
                <asp:Label ID="Label1" runat="server" CssClass="subtitulo" Text="BÚSQUEDA DE VEHÍCULOS"></asp:Label></td>
        </tr>
        <tr>
            <td align="left" colspan="3" style="height: 30px">
                </td>
        </tr>
        <tr>
            <td class="box" colspan="3" style="height: 38px">
                <asp:Label ID="Label2" runat="server" CssClass="subtitulo" Text="Campo:" Width="64px"></asp:Label>
                <asp:DropDownList ID="ddlCampo" runat="server" Width="184px">
                    <asp:ListItem>MARCA</asp:ListItem>
                    <asp:ListItem>MODELO</asp:ListItem>
                    <asp:ListItem>A&#209;O</asp:ListItem>
                    <asp:ListItem>PLACA</asp:ListItem>
                </asp:DropDownList>&nbsp;<asp:DropDownList ID="ddlCriterio" runat="server" Width="184px">
                    <asp:ListItem>IGUAL A</asp:ListItem>
                    <asp:ListItem>CONTENGA</asp:ListItem>
                </asp:DropDownList>
                <asp:TextBox ID="txtValor" runat="server" Width="264px"></asp:TextBox>&nbsp;
                <asp:Button ID="cmdBuscar" runat="server" CssClass="cssBoton" Text="Buscar" Width="80px" /></td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;<br />
                <asp:SqlDataSource ID="SqlDSVehiculos" runat="server" ConnectionString="<%$ ConnectionStrings:tributo %>"
                    ProviderName="<%$ ConnectionStrings:TRIBUTOConnectionString.ProviderName %>"
                    SelectCommand="select -1 as esta, v.vehiculo_id, V.año, v.numero_placa, v.numero_calcomania, v.activo, v.descripcion_estado_vehiculo, v.descripcion_tipo_vehiculo, v.descripcion_modelo, v.nombre_color, v.descripcion_marca, v.contribuyente_id FROM vis_vehiculo_contribuyente v WHERE v.contribuyente_id =  @contribuyente_id ORDER BY v.descripcion_marca, v.descripcion_modelo">
                    <SelectParameters>
                        <asp:QueryStringParameter Name="contribuyente_id" QueryStringField="ID" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:GridView ID="grvVehiculo" runat="server" AllowPaging="True" AlternatingRowStyle-CssClass="grvFila"
                    AutoGenerateColumns="False" CssClass="cssGrid" DataSourceID="SqlDSVehiculos"
                    EmptyDataText="No se encontraron vehículos en la búsqueda" HeaderStyle-CssClass="btnFrm"
                    Height="125px" Width="800px">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:CheckBox ID="cbxAsociar" runat="server" AutoPostBack="False" />
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="vehiculo_id" HeaderText="vehiculo_id" SortExpression="vehiculo_id"
                            Visible="False" />
                        <asp:BoundField DataField="descripcion_marca" HeaderText="Marca" />
                        <asp:BoundField DataField="descripcion_modelo" HeaderText="Modelo" />
                        <asp:BoundField DataField="a&#241;o" HeaderText="A&#241;o" />
                        <asp:BoundField DataField="nombre_color" HeaderText="Color" />
                        <asp:BoundField DataField="numero_placa" HeaderText="Placa" SortExpression="numero_placa">
                            <ItemStyle Width="120px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="vehiculo_id" HeaderText="vehiculo_id" ReadOnly="True"
                            SortExpression="vehiculo_id" />
                        <asp:TemplateField HeaderText="Solvente" SortExpression="EsSolvente">
                            <ItemTemplate>
                                <asp:Label ID="lblSolvente" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="contribuyente_id" HeaderText="contribuyente_id" SortExpression="contribuyente_id"
                            Visible="False">
                            <ItemStyle Width="120px" />
                        </asp:BoundField>
                    </Columns>
                    <EmptyDataTemplate>
                        <div class="grvDivVacio">
                            <table>
                                <tr>
                                    <td valign="middle">
                                        No hay vehículos asociados</td>
                                </tr>
                            </table>
                        </div>
                    </EmptyDataTemplate>
                    <HeaderStyle CssClass="cssCabecera" />
                    <AlternatingRowStyle CssClass="grvFila" />
                </asp:GridView>
                
                
                
                
                
                
                
                
                
                
                
                
            </td>
        </tr>
        <tr>
            <td align="left" colspan="3">
                <asp:Button ID="cmdSeleccionar" runat="server" CssClass="cssBoton" Text="Seleccionar" Width="80px" />
                <asp:Button ID="cmdCancelar" runat="server" CssClass="cssBoton" Text="Cerrar" Width="80px" /></td>
        </tr>
    </table>
    </form>
</asp:Content>

