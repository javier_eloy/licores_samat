﻿<%@ Page Language="VB" MasterPageFile="~/pagina/plantilla/plantillaSegura.master" AutoEventWireup="false" CodeFile="frmContribuyentes.aspx.vb" Inherits="pagina_operacion_Licores_frmContribuyentes" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
 <form id= "frm1" runat ="server"> 
    <table class="cssTabla" style="width: 1000px">
        <tr>
            <td colspan="3" rowspan="2" style="width: 994px; height: 34px" valign="middle">
                <asp:Label ID="Label1" runat="server" CssClass="cssTitulo" Text="Contribuyentes"></asp:Label></td>
        </tr>
        <tr>
        </tr>
        <tr>
            <td align="left" colspan="3" rowspan="3" style="width: 994px; height: 234px">
                <table class="cssTabla" style="width: 968px">
                    <tr>
                        <td align="left" style="width: 168px; height: 30px">
                            <asp:Label ID="Label13" runat="server" CssClass="subtitulo" Text="RIM:" Width="144px"></asp:Label></td>
                        <td align="left" style="width: 339px; height: 30px">
                            <asp:TextBox ID="TextBox2" runat="server" Width="256px"></asp:TextBox></td>
                        <td style="width: 230px; height: 30px">
                        </td>
                        <td align="left" style="width: 581px; height: 30px">
                            <asp:Label ID="Label38" runat="server" CssClass="subtitulo" Text="Nombre Contribuyente:"></asp:Label></td>
                        <td align="left" style="width: 280px; height: 30px">
                            <asp:TextBox ID="TextBox1" runat="server" Width="256px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 168px; height: 30px">
                            <asp:Label ID="Label17" runat="server" CssClass="subtitulo" Text="C.I. / RIF:"></asp:Label></td>
                        <td align="left" style="width: 339px; height: 30px">
                            <asp:TextBox ID="TextBox3" runat="server" Width="256px"></asp:TextBox></td>
                        <td align="left" style="width: 230px; height: 30px">
                        </td>
                        <td align="left" style="width: 581px; height: 30px">
                            <asp:Label ID="Label4" runat="server" CssClass="subtitulo" Text="Tipo Contribuyente:"
                                Width="128px"></asp:Label>
                        </td>
                        <td align="left" style="width: 280px; height: 30px">
                            <asp:DropDownList ID="DropDownList1" runat="server" Width="264px">
                                <asp:ListItem>SELECCIONE</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 168px; height: 30px">
                            <asp:Label ID="Label2" runat="server" CssClass="subtitulo" Text="Parroquia:"></asp:Label></td>
                        <td align="left" style="width: 339px; height: 30px">
                            <asp:DropDownList ID="DropDownList2" runat="server" Width="260px">
                                <asp:ListItem>SELECCIONE</asp:ListItem>
                            </asp:DropDownList></td>
                        <td align="left" style="width: 230px; height: 30px">
                        </td>
                        <td align="left" style="width: 581px; height: 30px">
                            <asp:Label ID="Label3" runat="server" CssClass="subtitulo" Text="Sector:" Width="64px"></asp:Label></td>
                        <td align="left" style="width: 280px; height: 30px">
                            <asp:DropDownList ID="DropDownList3" runat="server" Width="264px">
                                <asp:ListItem>SELECCIONE</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 168px; height: 30px">
                            <asp:Label ID="Label5" runat="server" CssClass="subtitulo" Text="Dirección:"></asp:Label></td>
                        <td align="left" colspan="4" style="height: 30px">
                            <asp:TextBox ID="TextBox4" runat="server" Width="800px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 168px; height: 30px">
                            <asp:Label ID="Label6" runat="server" CssClass="subtitulo" Text="Tipo de Org. Económica:"
                                Width="152px"></asp:Label></td>
                        <td align="left" colspan="4" style="height: 30px">
                            <asp:DropDownList ID="DropDownList4" runat="server" Width="260px">
                                <asp:ListItem>SELECCIONE</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left" colspan="5" style="height: 30px">
                            <asp:Button ID="Button5" runat="server" CssClass="cssBoton" Text="Buscar" Width="72px" />&nbsp;
                            <asp:Button ID="Button6" runat="server" CssClass="cssBoton" Text="Nuevo" Width="72px" /></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
        </tr>
        <tr>
        </tr>
    </table>
    <br />
    <table style="width: 1000px">
        <tr>
            <td class="cssTabla" colspan="3" rowspan="3">
                <table id="TABLE1" class="tablabtn" onclick="return TABLE1_onclick()" style="width: 952px;
                    height: 32px">
                    <tr>
                        <td align="left" style="width: 175px">
                            <asp:ImageButton ID="ImageButton14" runat="server" ImageUrl="~/Images/imagen/icono/icono-asociacion.gif"
                                ToolTip="Asociación de Servicios" />
                            <asp:ImageButton ID="ImageButton15" runat="server" ImageUrl="~/Images/imagen/icono/icono-tarifa.gif"
                                ToolTip="Registro de Tarifas" />
                            <asp:ImageButton ID="ImageButton16" runat="server" ImageUrl="~/Images/imagen/icono/icono-presupuestos.gif"
                                ToolTip="Presupuestos SAGAS" />
                            <asp:ImageButton ID="ImageButton17" runat="server" ImageUrl="~/Images/imagen/icono/icono-facturar.gif"
                                ToolTip="Facturar  " /></td>
                        <td>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/Images/imagen/icono/icono-ver.gif"
                                ToolTip="Seleccionar" />
                            <asp:ImageButton ID="ImageButton9" runat="server" ImageUrl="~/Images/imagen/icono/icono-inmueble.gif"
                                ToolTip="Inmueble" />&nbsp;<asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/imagen/icono/icono-cancelacion.gif"
                                    ToolTip="Cancelaciones " />
                            <asp:ImageButton ID="ImageButton3" runat="server" ImageUrl="~/imagen/icono/icono-solvencia.gif"
                                ToolTip="Solvencias" /></td>
                        <td align="right" style="width: 139px">
                            <asp:ImageButton ID="ImageButton10" runat="server" ImageUrl="~/Images/imagen/icono/icono-asociacion.gif"
                                ToolTip="Asociación de Tributos" />
                            <asp:ImageButton ID="ImageButton12" runat="server" ImageUrl="~/Images/imagen/icono/icono-car.gif"
                                ToolTip="Vehículos " />
                            <asp:ImageButton ID="ImageButton11" runat="server" ImageUrl="~/Images/imagen/icono/icono-licencia.gif"
                                ToolTip="Licencias de Licores" />
                            <asp:ImageButton ID="ImageButton4" runat="server" ImageUrl="~/Images/imagen/icono/icono-liquidado.gif"
                                ToolTip="Liquidación, Exoneración" />
                            <asp:ImageButton ID="ImageButton13" runat="server" ImageUrl="~/Images/imagen/icono/icono-exenciones.gif"
                                ToolTip="Exenciones" /></td>
                    </tr>
                </table>
                <br />
                <table style="width: 960px">
                    <tr>
                        <td style="width: 30px">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30px">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30px">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30px">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30px">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30px">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30px">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30px">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30px">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 30px">
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                        <td>
                        </td>
                    </tr>
                </table>
                <br />
                <table id="Table2" class="tablabtn" onclick="return TABLE1_onclick()" style="width: 952px;
                    height: 32px">
                    <tr>
                        <td align="left" style="width: 175px">
                            <asp:ImageButton ID="ImageButton5" runat="server" ImageUrl="~/Images/imagen/icono/icono-asociacion.gif"
                                ToolTip="Asociación de Servicios" />
                            <asp:ImageButton ID="ImageButton6" runat="server" ImageUrl="~/Images/imagen/icono/icono-tarifa.gif"
                                ToolTip="Registro de Tarifas" />
                            <asp:ImageButton ID="ImageButton7" runat="server" ImageUrl="~/Images/imagen/icono/icono-presupuestos.gif"
                                ToolTip="Presupuestos SAGAS" />
                            <asp:ImageButton ID="ImageButton8" runat="server" ImageUrl="~/Images/imagen/icono/icono-facturar.gif"
                                ToolTip="Facturar  " /></td>
                        <td>
                            <asp:ImageButton ID="ImageButton18" runat="server" ImageUrl="~/Images/imagen/icono/icono-ver.gif"
                                ToolTip="Seleccionar" />
                            <asp:ImageButton ID="ImageButton19" runat="server" ImageUrl="~/Images/imagen/icono/icono-inmueble.gif"
                                ToolTip="Inmueble" />
                            <asp:ImageButton ID="ImageButton20" runat="server" ImageUrl="~/Images/imagen/icono/icono-cancelacion.gif"
                                ToolTip="Cancelaciones " />
                            <asp:ImageButton ID="ImageButton21" runat="server" ImageUrl="~/Images/imagen/icono/icono-solvencia.gif"
                                ToolTip="Solvencias" /></td>
                        <td align="right" style="width: 139px">
                            <asp:ImageButton ID="ImageButton22" runat="server" ImageUrl="~/Images/imagen/icono/icono-asociacion.gif"
                                ToolTip="Asociación de Tributos" />
                            <asp:ImageButton ID="ImageButton23" runat="server" ImageUrl="~/Images/imagen/icono/icono-car.gif"
                                ToolTip="Vehículos " />
                            <asp:ImageButton ID="ImageButton24" runat="server" ImageUrl="~/Images/imagen/icono/icono-licencia.gif"
                                ToolTip="Licencias de Licores" />
                            <asp:ImageButton ID="ImageButton25" runat="server" ImageUrl="~/Images/imagen/icono/icono-liquidado.gif"
                                ToolTip="Liquidación, Exoneración" />
                            <asp:ImageButton ID="ImageButton26" runat="server" ImageUrl="~/Images/imagen/icono/icono-exenciones.gif"
                                ToolTip="Exenciones" /></td>
                    </tr>
                </table>
                <br />
                1 2 3 4 5 6 7 8 9 10</td>
        </tr>
        <tr>
        </tr>
        <tr>
        </tr>
    </table>
    </form> 
</asp:Content>

