﻿'----------------------------------------------------------------------------------------------
'-- ORIGINAL: (DESCONOCIDO)
'-- MODIFICADO: JAVIER HERNANDEZ 
'-- OBJETIVO DE LA MODIFICACION: Agrupar las acciones de los diferentes modulos en controles 
'--                              de usuarios.
'-- FECHA: ENERO 2011
'---------------------------------------------------------------------------------------------

Imports modModelLicencia
Imports modModelContribuyente

Partial Class pagina_operacion_Licores_frmDatosContribuyente
    Inherits System.Web.UI.UserControl

    Public Function CargaDatosContribuyenteLicencia(ByVal lngLicenciaId As Long) As Long
        Dim dr As System.Data.IDataReader
        Dim pLicenciaID As String = vbNull
        Dim lngContribuyenteId As Long

        pLicenciaID = lngLicenciaId.ToString()
        If IsDBNull(pLicenciaID) = False Then

            dr = getDatosContribuyenteByLicencia(pLicenciaID)

            If dr.Read = True Then
                lngContribuyenteId = dr.GetValue(21).ToString()
                Session("contribuyente_id") = lngContribuyenteId
                lblUsoContribuyente.Text = dr.GetValue(0).ToString()
                lblNombreContribuyente.Text = dr.GetValue(1).ToString()
                lblLemaContribuyente.Text = dr.GetValue(2).ToString()
                lblRIFContribuyente.Text = dr.GetValue(3).ToString()
                lblCedulaContribuyente.Text = dr.GetValue(4).ToString()
                lblRIMContribuyente.Text = dr.GetValue(5).ToString()
                lblParroquiaContribuyente.Text = dr.GetValue(6).ToString()
                lblDireccionContribuyente.Text = dr.GetValue(7).ToString()
                lblTelefonoContribuyente.Text = dr.GetValue(8).ToString()
                lblCelularContribuyente.Text = dr.GetValue(9).ToString()
                lblEmailContribuyente.Text = dr.GetValue(10).ToString()
                lblEstadoContribuyente.Text = dr.GetValue(11).ToString()
                lblEstClasificacionContribuyente.Text = dr.GetValue(12).ToString()
                lblSectorContribuyente.Text = dr.GetValue(13).ToString()
                lblTlfAdicionalContribuyente.Text = dr.GetValue(14).ToString()
                lblCelAdicionalContribuyente.Text = dr.GetValue(15).ToString()
                lblPaginaContribuyente.Text = dr.GetValue(16).ToString()
                lblParroquiaEstablecimiento.Text = dr.GetValue(6).ToString()
                lblSectorEstablecimiento.Text = dr.GetValue(17).ToString()
                lblDireccionEstablecimiento.Text = dr.GetValue(18).ToString()
                'lblNumLicencia.Text = dr.GetValue(19).ToString()
                lblSeniat.Text = dr.GetValue(20).ToString()
                Return lngContribuyenteId
            End If
            Return 0
        End If

    End Function

    Public Sub CargarContribuyente(ByVal lngContribuyenteId As Long)
        Dim dr As System.Data.IDataReader
        Dim sql As String = vbNullString
        Dim pContribuyenteID As String = vbNull

        pContribuyenteID = lngContribuyenteId.ToString()
        If IsDBNull(pContribuyenteID) = False Then
            dr = getDatosContribuyente(lngContribuyenteId)
            If dr.Read = True Then
                lblUsoContribuyente.Text = dr.GetValue(0).ToString()
                lblNombreContribuyente.Text = dr.GetValue(1).ToString()
                lblLemaContribuyente.Text = dr.GetValue(2).ToString()
                lblRIFContribuyente.Text = dr.GetValue(3).ToString()
                lblCedulaContribuyente.Text = dr.GetValue(4).ToString()
                lblRIMContribuyente.Text = dr.GetValue(5).ToString()
                lblParroquiaContribuyente.Text = dr.GetValue(6).ToString()
                lblDireccionContribuyente.Text = dr.GetValue(7).ToString()
                lblTelefonoContribuyente.Text = dr.GetValue(8).ToString()
                lblCelularContribuyente.Text = dr.GetValue(9).ToString()
                lblEmailContribuyente.Text = dr.GetValue(10).ToString()
                lblEstadoContribuyente.Text = dr.GetValue(11).ToString()
                lblEstClasificacionContribuyente.Text = dr.GetValue(12).ToString()
                lblSectorContribuyente.Text = dr.GetValue(13).ToString()
                lblTlfAdicionalContribuyente.Text = dr.GetValue(14).ToString()
                lblCelAdicionalContribuyente.Text = dr.GetValue(15).ToString()
                lblPaginaContribuyente.Text = dr.GetValue(16).ToString()
                lblParroquiaEstablecimiento.Text = dr.GetValue(6).ToString()
                lblSectorEstablecimiento.Text = dr.GetValue(17).ToString()
                lblDireccionEstablecimiento.Text = dr.GetValue(18).ToString()
                'lblNumLicencia.Text = dr.GetValue(19).ToString()
                lblSeniat.Text = dr.GetValue(20).ToString()
            End If
            dr.Close()
        End If
    End Sub
End Class
