﻿Imports System.Data.SqlClient
Partial Class pagina_operacion_Licores_frmLicenciaAprobadaConsumo
    Inherits System.Web.UI.Page

    Private Sub CargaDatosContribuyente()
        lblUsoContribuyente.Text = Session("UsoContribuyente")
        lblNombreContribuyente.Text = Session("NombreContribuyente")
        lblLemaContribuyente.Text = Session("LemaContribuyente")
        lblRIFContribuyente.Text = Session("RIFContribuyente")
        lblCedulaContribuyente.Text = Session("CedulaContribuyente")
        lblRIMContribuyente.Text = Session("RIMContribuyente")
        lblParroquiaContribuyente.Text = Session("ParroquiaContribuyente")
        lblDireccionContribuyente.Text = Session("DireccionContribuyente")
        lblTelefonoContribuyente.Text = Session("TelefonoContribuyente")
        lblCelularContribuyente.Text = Session("CelularContribuyente")
        lblEmailContribuyente.Text = Session("EmailContribuyente")
        lblEstadoContribuyente.Text = Session("EstadoContribuyente")
        lblEstClasificacionContribuyente.Text = Session("EstClasificacionContribuyente")
        lblSectorContribuyente.Text = Session("SectorContribuyente")
        lblTlfAdicionalContribuyente.Text = Session("TlfAdicionalContribuyente")
        lblCelAdicionalContribuyente.Text = Session("CelAdicionalContribuyente")
        lblPaginaContribuyente.Text = Session("PaginaContribuyente")
        lblParroquiaEstablecimiento.Text = Session("ParroquiaEstablecimiento")
        lblSectorEstablecimiento.Text = Session("SectorEstablecimiento")
        lblDireccionEstablecimiento.Text = Session("DireccionEstablecimiento")
        'lblNumLicencia.Text = Session("NumLicencia")
        lblSeniat.Text = Session("Seniat")
    End Sub
    
    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Licencia_id As Integer
        Call CargaDatosContribuyente()
        Try
            If txtActualizar.Text <> "Actualizando" Then
                Licencia_id = CInt(Request("parm1"))
                Call CargarLicencia(Licencia_id)
                txtActualizar.Text = "Actualizando"
            End If
        Catch ex As Exception

        End Try

    End Sub

    Protected Sub BtnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect(ResolveUrl("~/pagina/operacion/Licores/frmConsultaLicencia.aspx"))
    End Sub

    Protected Sub calendario_SelectionChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Select Case txtindice.Text
            Case "1"
                InsertaFecha(txtFechaSolicitud)
            Case "2"
                InsertaFecha(txtFechaVencimiento)
            Case "3"
                InsertaFecha(txtFechaEmision)
        End Select
    End Sub

    Protected Sub btnFechaSolicitud_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        calendario.Visible = True
        txtindice.Text = "1"
    End Sub

    Protected Sub FechaVencimiento_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        calendario.Visible = True
        txtindice.Text = "2"
    End Sub

    Protected Sub btnFechaEmision_Click(ByVal sender As Object, ByVal e As System.Web.UI.ImageClickEventArgs)
        calendario.Visible = True
        txtindice.Text = "3"
    End Sub

    Protected Sub BtnInsertar_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Licencia_id As Integer
        Try           
            Licencia_id = CInt(Request("parm1"))
            Call ActualizaLicencia(Licencia_id)
        Catch ex As Exception
        End Try
    End Sub

    Private Sub CargarLicencia(ByVal Licencia_id As Integer)
        Dim cnn As SqlConnection = Nothing
        Dim cmd As SqlCommand = Nothing
        Dim dr As System.Data.IDataReader
        Dim sql As String = vbNullString

        cnn = modBaseDatos.obtenerConexion
        Try
            cnn.Open()

            If IsDBNull(Licencia_id) = False Then

                sql = "SELECT     "
                sql += "LICENCIA.tipo_licencia_id, "
                sql += "LICENCIA.motivo_autorizacion_id,"
                sql += "LICENCIA.clasificacion_licencia_id, "
                sql += "LICENCIA.estado_licencia_id, "
                sql += "LICENCIA.horario_licencia_id, "
                sql += "LICENCIA.num_control, "
                sql += "LICENCIA.num_autorizacion,"
                sql += "LICENCIA.num_solicitud, "
                sql += "LICENCIA.fecha_solicitud,"
                sql += "LICENCIA.fecha_inicio_licencia, "
                sql += "LICENCIA.fecha_vencimiento_licencia, "
                sql += "LICENCIA.num_licencia,"
                sql += "LICENCIA.año "
                sql += "FROM "
                sql += "MOTIVO_AUTORIZACION INNER JOIN horario_licencia INNER JOIN CLASIFICACION_LICENCIA INNER JOIN LICENCIA ON CLASIFICACION_LICENCIA.clasificacion_licencia_id = LICENCIA.clasificacion_licencia_id INNER JOIN ESTADO_LICENCIA ON LICENCIA.estado_licencia_id = ESTADO_LICENCIA.estado_licencia_id ON horario_licencia.horario_licencia_id = LICENCIA.horario_licencia_id INNER JOIN TIPO_LICENCIA ON LICENCIA.tipo_licencia_id = TIPO_LICENCIA.tipo_licencia_id ON MOTIVO_AUTORIZACION.motivo_autorizacion_id = LICENCIA.motivo_autorizacion_id where LICENCIA.licencia_id =" & Licencia_id
                txtindice.Text = sql
                cmd = New SqlCommand(sql, cnn)
                cmd.CommandTimeout = 0
                dr = cmd.ExecuteReader()

                If dr.Read = True Then
                    ddlTipoLicencia.Text = dr.GetValue(0).ToString()
                    ddlMotivoAutorizacion.Text = dr.GetValue(1).ToString()
                    ddlClasificacionLicencia.Text = dr.GetValue(2).ToString()
                    ddlEstadoLicencia.Text = dr.GetValue(3).ToString()
                    ddlHorario.Text = dr.GetValue(4).ToString()
                    txtNumeroControl.Text = dr.GetValue(5).ToString()
                    txtNumeroAutorizacion.Text = dr.GetValue(6).ToString()
                    txtNumeroSolicitud.Text = dr.GetValue(7).ToString()
                    txtFechaSolicitud.Text = dr.GetValue(8).ToString()
                    txtFechaSolicitud.Text = Mid(txtFechaSolicitud.Text, 1, 10)
                    txtFechaEmision.Text = dr.GetValue(9).ToString()
                    txtFechaEmision.Text = Mid(txtFechaEmision.Text, 1, 10)
                    txtFechaVencimiento.Text = dr.GetValue(10).ToString()
                    txtFechaVencimiento.Text = Mid(txtFechaVencimiento.Text, 1, 10)
                    txtNumeroLicencia.Text = dr.GetValue(11).ToString()
                    txtNumeroLicencia.Text = Mid(txtNumeroLicencia.Text, 1, 10)
                    txtAnn.Text = dr.GetValue(12).ToString()
                End If
                dr.Close()
                cnn.Close()

            End If
        Catch

        End Try
    End Sub

    Private Sub InsertaFecha(ByVal txt As TextBox)
        Dim ann As Integer
        txt.Text = calendario.SelectedDate
        If txtindice.Text = "3" Then
            ann = CInt(calendario.SelectedDate.Year)
            ann = ann + 1
            txtFechaVencimiento.Text = calendario.SelectedDate.Day & "/" & Format(calendario.SelectedDate.Month, "00") & "/" & ann.ToString
        End If
        calendario.Visible = False
    End Sub

    Private Sub ActualizaLicencia(ByVal id As Integer)
        Dim sql As String = vbNullString
        Dim cnn As SqlConnection = Nothing
        Dim cmd As SqlCommand = Nothing

        cnn = modBaseDatos.obtenerConexion

        Try
            cnn.Open()
            sql = "Update LICENCIA set "
            sql += "usuario_id =" & CType(Session("Usuario"), Sesion).UsuarioID.ToString & ", "
            sql += "tipo_licencia_id = " & ddlTipoLicencia.SelectedValue & ", "
            sql += "motivo_autorizacion_id =" & ddlMotivoAutorizacion.SelectedValue & ", "
            sql += "clasificacion_licencia_id =" & ddlClasificacionLicencia.SelectedValue & ", "
            sql += "estado_licencia_id = " & ddlEstadoLicencia.SelectedValue & ", "
            sql += "horario_licencia_id = " & ddlHorario.SelectedValue & ", "
            sql += "num_control = '" & txtNumeroControl.Text & "', "
            sql += "num_autorizacion = '" & txtNumeroAutorizacion.Text & "', "
            sql += "num_solicitud = '" & txtNumeroSolicitud.Text & "', "
            sql += "fecha_solicitud = '" & Format(CDate(txtFechaSolicitud.Text), "yyyyMMdd") & "', "
            sql += "fecha_inicio_licencia = '" & Format(CDate(txtFechaEmision.Text), "yyyyMMdd") & "', "
            sql += "fecha_vencimiento_licencia = '" & Format(CDate(txtFechaVencimiento.Text), "yyyyMMdd") & "', "
            sql += "año = '" & txtAnn.Text & "' "
            sql += "where licencia_id = " & id
            cmd = New SqlCommand(sql, cnn)
            cmd.ExecuteNonQuery()
            System.Windows.Forms.MessageBox.Show("Datos actualizados con exito.")
        Catch ex As Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

End Class