﻿Imports System.Data.SqlClient
Imports System.Data
Imports modModelContribuyente
Imports modModelLicencia
Imports modGeneralTributo
Imports modPermiso


Partial Class pagina_operacion_Licores_frmEditDatosLicencia
    Inherits System.Web.UI.Page

    Dim lngLicenciaId As Long = 0
    Dim lngContribuyenteId As Long = 0

    '---- Definicion de objetos del contro datos del contribuyente para permitir el acceso ---
    Protected WithEvents lblNombreContribuyente As Label
    Protected WithEvents lblEstadoContribuyente As Label
    Protected WithEvents lblEstClasificacionContribuyente As New Label
    Protected WithEvents lblLemaContribuyente As New Label
    Protected WithEvents lblCedulaContribuyente As New Label
    Protected WithEvents lblRIFContribuyente As New Label
    Protected WithEvents lblRIMContribuyente As New Label
    Protected WithEvents lblUsoContribuyente As New Label
    Protected WithEvents lblParroquiaContribuyente As New Label
    Protected WithEvents lblSectorContribuyente As New Label
    Protected WithEvents lblDireccionContribuyente As New Label
    Protected WithEvents lblTelefonoContribuyente As New Label
    Protected WithEvents lblTlfAdicionalContribuyente As New Label
    Protected WithEvents lblCelularContribuyente As New Label
    Protected WithEvents lblCelAdicionalContribuyente As New Label
    Protected WithEvents lblEmailContribuyente As New Label
    Protected WithEvents lblPaginaContribuyente As New Label
    Protected WithEvents lblParroquiaEstablecimiento As New Label
    Protected WithEvents lblSectorEstablecimiento As New Label
    Protected WithEvents lblDireccionEstablecimiento As New Label
    Protected WithEvents lblNumLicencia As New Label
    Protected WithEvents lblSeniat As New Label
    '--- fin Definicion -------

    '--- Definicion de objetos del control de Datos de las licencias ---
    Protected WithEvents ddlMotivoAutorizacion As DropDownList
    Protected WithEvents ddlEstadoLicencia As DropDownList
    Protected WithEvents ddlTipoLicencia As DropDownList
    Protected WithEvents ddlHorario As DropDownList
    Protected WithEvents ddlClasificacionLicencia As DropDownList

    Protected txtNumeroControl As TextBox
    Protected txtNumeroSolicitud As TextBox
    Protected txtFechaSolicitud As TextBox
    Protected txtFechaEmision As TextBox
    Protected txtFechaInicio As TextBox
    Protected txtFechaVencimiento As TextBox
    Protected txtAnn As TextBox
    Protected txtLicenciaSENIAT As TextBox
    Protected txtNumeroExpediente As TextBox
    Protected txtNumeroLicencia As TextBox

    Protected imgFechaSolicitud As Image
    Protected imgFechaEmision As Image
    Protected imgFechaInicio As Image
    Protected imgFechaVencimiento As Image
    Protected lblRenovacion As Label
    '--- fin Definicion ---

    Protected Sub Page_PreLoad(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.PreLoad
        '----- Datos del Contribuyente ----
        lblNombreContribuyente = ucDatosContribuyentes.FindControl("lblNombreContribuyente")
        lblEstadoContribuyente = ucDatosContribuyentes.FindControl("lblEstadoContribuyente")
        lblEstClasificacionContribuyente = ucDatosContribuyentes.FindControl("lblEstClasificacionContribuyente")
        lblLemaContribuyente = ucDatosContribuyentes.FindControl("lblLemaContribuyente")
        lblCedulaContribuyente = ucDatosContribuyentes.FindControl("lblCedulaContribuyente")
        lblRIFContribuyente = ucDatosContribuyentes.FindControl("lblRIFContribuyente")
        lblRIMContribuyente = ucDatosContribuyentes.FindControl("lblRIMContribuyente")
        lblUsoContribuyente = ucDatosContribuyentes.FindControl("lblUsoContribuyente")
        lblParroquiaContribuyente = ucDatosContribuyentes.FindControl("lblParroquiaContribuyente")
        lblSectorContribuyente = ucDatosContribuyentes.FindControl("lblSectorContribuyente")
        lblDireccionContribuyente = ucDatosContribuyentes.FindControl("lblDireccionContribuyente")
        lblTelefonoContribuyente = ucDatosContribuyentes.FindControl("lblTelefonoContribuyente")
        lblTlfAdicionalContribuyente = ucDatosContribuyentes.FindControl("lblTlfAdicionalContribuyente")
        lblCelularContribuyente = ucDatosContribuyentes.FindControl("lblCelularContribuyente")
        lblCelAdicionalContribuyente = ucDatosContribuyentes.FindControl("lblCelAdicionalContribuyente")
        lblEmailContribuyente = ucDatosContribuyentes.FindControl("lblEmailContribuyente")
        lblPaginaContribuyente = ucDatosContribuyentes.FindControl("lblPaginaContribuyente")
        lblParroquiaEstablecimiento = ucDatosContribuyentes.FindControl("lblParroquiaEstablecimiento")
        lblSectorEstablecimiento = ucDatosContribuyentes.FindControl("lblSectorEstablecimiento")
        lblDireccionEstablecimiento = ucDatosContribuyentes.FindControl("lblDireccionEstablecimiento")
        lblNumLicencia = ucDatosContribuyentes.FindControl("lblNumLicencia")
        lblSeniat = ucDatosContribuyentes.FindControl("lblSeniat")
        '---- Datos de Licencia
        ddlMotivoAutorizacion = ucDatosLicencia.FindControl("ddlMotivoAutorizacion")
        ddlEstadoLicencia = ucDatosLicencia.FindControl("ddlEstadoLicencia")
        ddlTipoLicencia = ucDatosLicencia.FindControl("ddlTipoLicencia")
        ddlHorario = ucDatosLicencia.FindControl("ddlHorario")
        ddlClasificacionLicencia = ucDatosLicencia.FindControl("ddlClasificacionLicencia")
        txtNumeroControl = ucDatosLicencia.FindControl("txtNumeroControl")
        txtNumeroSolicitud = ucDatosLicencia.FindControl("txtNumeroSolicitud")
        txtFechaSolicitud = ucDatosLicencia.FindControl("txtFechaSolicitud")
        txtFechaEmision = ucDatosLicencia.FindControl("txtFechaEmision")
        txtFechaInicio = ucDatosLicencia.FindControl("txtFechaInicio")
        txtFechaVencimiento = ucDatosLicencia.FindControl("txtFechaVencimiento")
        txtAnn = ucDatosLicencia.FindControl("txtAnn")
        txtLicenciaSENIAT = ucDatosLicencia.FindControl("txtLicenciaSENIAT")
        txtNumeroExpediente = ucDatosLicencia.FindControl("txtNumeroExpediente")
        txtNumeroLicencia = ucDatosLicencia.FindControl("txtNumeroLicencia")
        imgFechaSolicitud = ucDatosLicencia.FindControl("imgFechaSolicitud")
        imgFechaEmision = ucDatosLicencia.FindControl("imgFechaEmision")
        imgFechaInicio = ucDatosLicencia.FindControl("imgFechaInicio")
        imgFechaVencimiento = ucDatosLicencia.FindControl("imgFechaVencimiento")
        lblRenovacion = ucDatosLicencia.FindControl("lblRenovacion")

    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim transaccion As Integer

        Session("Action") = "frmEditDatosLicencia"  '<---- Identifica la accion para el user control
        Try
            lngContribuyenteId = CType(Request("ID"), Integer)
            Session("contribuyente_id") = lngContribuyenteId
            lngLicenciaId = CType(Request("licenciaId"), Integer)
            Session("licencia_id") = lngLicenciaId
            transaccion = CType(Request("transac"), Integer)
        Catch
            lngContribuyenteId = 0
        End Try


        If Not Page.IsPostBack Then
            DeshabilitarCache(Me)
            Dim Usuario As Sesion = Session("Usuario")
            'Validar(Usuario, Me) OJO TENGO QUE DESCOMENTAR ESTO

            lngContribuyenteId = ucDatosContribuyentes.CargaDatosContribuyenteLicencia(lngLicenciaId)

            If transaccion = 1 Then
                '---- VIENE DE CONSULTA DIRECTAMENTE
                ucDatosLicencia.CargaDatosLicencia(lngLicenciaId)
            Else
                '----VIENE DE LA MODIFICACIÓN DE UNA LICENCIA EXISTENTE
                ucDatosLicencia.AsignaDatosLicencia(Request("motivoAutorizacionId"), _
                    Request("estadoLicenciaId"), Request("tipoLicenciaId"), Request("horarioId"), _
                    Request("clasificacionLicenciaId"), Request("numeroControl"), _
                    Request("numeroSolicitud"), _
                    Request("fechaSolicitud"), Request("fechaEmision"), _
                    Request("fechaInicio"), Request("fechaVencimiento"), _
                    Request("anno"), Request("numeroSeniat"), _
                    Request("numeroExpediente"), Request("numeroLicencia"), _
                    Request("evento"), Request("ikiosko"), Request("kiosko"), Request("UbicacionEv"))
            End If
        End If
    End Sub

    Protected Sub BtnCancelar_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancelar.Click
        Response.Redirect(ResolveUrl("~/pagina/operacion/Licores/frmConsultaLicencia.aspx?ID=" & Session("contribuyente_id")))
    End Sub

    Protected Sub BtnInsertar_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        If ucDatosLicencia.InsertaLicencia(lngLicenciaId, lngContribuyenteId, lblUsoContribuyente.Text, _
                                           lblSeniat.Text, lblRIMContribuyente.Text, _
                                           lblRIFContribuyente.Text, lblNombreContribuyente.Text, _
                                           lblParroquiaContribuyente.Text, lblSectorContribuyente.Text, _
                                           False, _
                                           UpdatePanel1) Then
            Response.Redirect(ResolveUrl("~/pagina/operacion/Licores/frmConsultaLicencia.aspx?ID=" & lngContribuyenteId.ToString))
        End If


    End Sub

End Class
